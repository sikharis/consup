package main

import (
	"fmt"

	"consup/models"

	"github.com/kataras/iris"
)

type APIResult struct {
	Data        interface{} `json:"data"`
	Message     string      `json:"message"`
	Error       error       `json:"error"`
	StatusCode  int
	RedirectURL string
}

var maxSize int64 = 5 << 20

func (res *APIResult) Render(ctx iris.Context) {

	final := make(map[string]interface{})
	final["message"] = res.Message

	ctx.StatusCode(200)
	if res.StatusCode != 0 {
		ctx.StatusCode(res.StatusCode)
	}

	if res.Error != nil {
		final["error"] = res.Error.Error()

		if res.StatusCode != 200 && res.StatusCode != 0 {
			ctx.StatusCode(iris.StatusBadRequest)
		}
	} else {
		final["data"] = res.Data
		final["error"] = ""
	}

	if res.RedirectURL != "" {
		final["redirectURL"] = res.RedirectURL
	}

	ctx.JSON(final)
}

func backEndAPI(app *iris.Application) {

	v1 := app.Party("/api/v1")
	{
		navigation := v1.Party("/navigation")
		{
			navigation.Get("/", func(ctx iris.Context) {

				var res APIResult
				res.Data, res.Error = models.GetNavigations()

				if res.Error != nil {
					res.StatusCode = iris.StatusInternalServerError
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			navigation.Get("/{id:int}", func(ctx iris.Context) {

				var res APIResult
				id, err := ctx.Params().GetInt("id")
				res.Error = err
				res.Data, res.Error = models.GetNavigationByID(id)

				if res.Error != nil {
					res.StatusCode = iris.StatusInternalServerError
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

		}

		auth := v1.Party("/auth")
		{
			auth.Post("/login", func(ctx iris.Context) {

				var res APIResult
				user := models.User{}
				// res.Error = ctx.ReadJSON(&user)
				user.Username = ctx.PostValue("username")
				user.Password = ctx.PostValue("password")

				if res.Error != nil {
					res.StatusCode = iris.StatusInternalServerError
					res.Render(ctx)
					return
				}

				user, res.Error = user.GetUserAuth()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				if user.Username != "" && user.Password != "" {
					//set session values
					s := sess.Start(ctx)
					s.Set("auth", true)
					s.Set("name", user.Name)
					s.Set("username", user.Username)

					res.RedirectURL = "/dashboard"
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			auth.Get("/logout", CheckAuth, func(ctx iris.Context) {
				sess.Start(ctx).Clear()
				sess.Start(ctx).Destroy()
				returnToBase(ctx)
			})

			auth.Get("/forceLogout", CheckAuth, func(ctx iris.Context) {
				sess.Start(ctx).Clear()
				sess.Start(ctx).Destroy()

				ctx.Redirect("/", iris.StatusTemporaryRedirect)
			})
		}

		user := v1.Party("/users")
		user.Use(CheckAuth)
		{
			// Get all users
			// GET /api/v1/users
			user.Get("/", func(ctx iris.Context) {

				var res APIResult
				res.Data, res.Error = models.GetUsers()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// Get specific user
			// GET /api/v1/users/foo
			user.Get("/{username:string}", func(ctx iris.Context) {
				username := ctx.Params().Get("username")

				var res APIResult
				user := models.User{}
				user.Username = username

				res.Data, res.Error = user.GetUserByUsername()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// Insert new user
			// PUT /api/v1/users
			user.Put("/", func(ctx iris.Context) {

				var res APIResult
				user := models.User{}
				res.Error = ctx.ReadJSON(&user)

				if res.Error != nil {
					res.Render(ctx)
					return
				}
				temp, _ := user.CheckUsernameExist()
				if temp > 0 {
					res.Message = "User already exists."
					res.Render(ctx)
					return
				}

				res.Data, res.Error = user.NewUser()
				CasbinController.AddGroupingPolicy(user.Username, user.Role)
				if res.Error != nil {
					res.Message = "User already exists."
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// Update user
			// POST /api/v1/users
			user.Post("/{username:string}", func(ctx iris.Context) {

				username := ctx.Params().Get("username")

				var res APIResult
				user := models.User{}
				user.Username = username

				res.Error = ctx.ReadJSON(&user)
				if res.Error != nil {
					res.Render(ctx)
					return
				}

				//CHECK FOR CHANGE PASSWORD
				if user.Username != "" && user.Password != "" && user.OldPassword != "" {
					tempUser := models.User{}
					tempUser.Username = user.Username
					tempUser.Password = user.OldPassword

					tempUser, res.Error = tempUser.GetUserAuth()

					if res.Error != nil {
						res.Render(ctx)
						return
					}
					if res.Error != nil || tempUser.Username == "" {
						user.OldPassword = ""
						user.Password = ""
					}
				}

				//UPDATE USER PROCESS
				res.Error = user.UpdateUser()
				CasbinController.UpdateGroupingPolicy(user.Username, user.Role)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// DELETE /api/v1/users/foo
			user.Delete("/{username:string}", func(ctx iris.Context) {

				username := ctx.Params().Get("username")

				var res APIResult
				user := models.User{}
				user.Username = username

				res.Error = user.DeleteUserByUsername()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
		}

		Environment := v1.Party("/Environments")
		Environment.Use(CheckAuth)
		{
			// Get all server types
			// GET /api/v1/servertype
			Environment.Get("/", func(ctx iris.Context) {

				var res APIResult
				res.Data, res.Error = models.GetEnvironments()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// Get specific server types By ID
			// GET /api/v1/servertype/1
			Environment.Get("/{id:uint}", CheckAuth, func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				environment := models.Environment{}
				environment.ID = id

				res.Data, res.Error = environment.GetEnvironmentByID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// Insert new server types
			// POST /api/v1/servertype
			Environment.Put("/", func(ctx iris.Context) {
				var res APIResult
				environment := models.Environment{}
				res.Error = ctx.ReadJSON(&environment)
				if res.Error != nil {
					res.Render(ctx)
					return
				}
				res.Data, res.Error = environment.NewEnvironment()
				if res.Error != nil {
					res.Render(ctx)
					return
				}
				res.Render(ctx)
			})

			// Update server types
			// POST /api/v1/servertype
			Environment.Post("/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				environment := models.Environment{}
				environment.ID = id

				res.Error = ctx.ReadJSON(&environment)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Data, res.Error = environment.UpdateEnvironment()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// Delete server type
			// DELETE /api/v1/servertype/10.0.0.1
			Environment.Delete("/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				environment := models.Environment{}
				environment.ID = id

				res.Error = environment.DeleteEnvironmentByID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
		}

		server := v1.Party("/servers")
		server.Use(CheckAuth)
		{
			// Get all servers
			// GET /api/v1/servers
			server.Get("/", func(ctx iris.Context) {

				var res APIResult
				res.Data, res.Error = models.GetServers()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// Get specific server
			// GET /api/v1/servers/10.0.0.1
			server.Get("/{ip:string}", func(ctx iris.Context) {

				ip := ctx.Params().Get("ip")

				var res APIResult
				server := models.Server{}
				server.IP = ip

				res.Data, res.Error = server.GetServerByIP()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// Get specific server By ID
			// GET /api/v1/servers/id/1
			server.Get("/{id:int}", CheckAuth, func(ctx iris.Context) {

				var res APIResult
				id, err := ctx.Params().GetInt("id")
				res.Error = err
				server := models.Server{}
				server.ID = id

				res.Data, res.Error = server.GetServerByID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// Insert new server
			// put /api/v1/servers/1
			server.Put("/", func(ctx iris.Context) {

				var res APIResult
				server := models.Server{}
				res.Error = ctx.ReadJSON(&server)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Data, res.Error = server.NewServer()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// Update server
			// POST /api/v1/servers
			server.Post("/{id:int}", func(ctx iris.Context) {
				var res APIResult
				id, err := ctx.Params().GetInt("id")
				res.Error = err
				data := models.Server{}
				data.ID = id

				res.Error = ctx.ReadJSON(&data)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Data, res.Error = data.UpdateServer()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// DELETE /api/v1/servers/10.0.0.1
			server.Delete("/{id:int}", func(ctx iris.Context) {

				var res APIResult
				id, err := ctx.Params().GetInt("id")
				res.Error = err

				server := models.Server{}
				server.ID = id

				res.Error = server.DeleteServerByID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
		}

		application := v1.Party("/apps")
		application.Use(CheckAuth)
		{
			// 	// Get all servers
			// 	// GET /api/v1/servers
			application.Get("/", func(ctx iris.Context) {

				var res APIResult
				res.Data, res.Error = models.GetApps()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// 	// Get specific server
			// 	// GET /api/v1/apps/10.0.0.1
			application.Get("/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				apps := models.App{}
				apps.ID = id

				res.Data, res.Error = apps.GetAppByID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// Insert new App
			// POST /api/v1/apps
			application.Put("/", func(ctx iris.Context) {

				var res APIResult
				apps := models.App{}
				res.Error = ctx.ReadJSON(&apps)

				if res.Error != nil {
					fmt.Println("Error :", res.Error)
					res.Render(ctx)
					return
				}
				fmt.Println("Apps :", apps)
				res.Data, res.Error = apps.NewApp()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// Update App
			// POST /api/v1/apps
			application.Post("/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				apps := models.App{}
				apps.ID = id

				res.Error = ctx.ReadJSON(&apps)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Data, res.Error = apps.UpdateApp()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// 	// DELETE /api/v1/apps/1
			application.Delete("/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				apps := models.App{}
				apps.ID = id

				res.Error = apps.DeleteAppByID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
		}

		applicationserver := v1.Party("/appservers")
		applicationserver.Use(CheckAuth)
		{
			applicationserver.Get("/app/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				apps := models.AppServer{}
				apps.AppID = id

				res.Data, res.Error = apps.GetAppServerByAppID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
			applicationserver.Get("/server/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				apps := models.AppServer{}
				apps.ServerID = id

				res.Data, res.Error = apps.GetAppServerByServerID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
			applicationserver.Get("/environment/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				apps := models.AppServer{}
				apps.EnvironmentID = id

				res.Data, res.Error = apps.GetAppServerByEnvironmentID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
			applicationserver.Post("/{serverid:uint}/{appid:uint}/{envid:uint}", func(ctx iris.Context) {
				var res APIResult
				appserver := models.AppServer{}
				res.Error = ctx.ReadJSON(&appserver)

				serverid, _ := ctx.Params().GetInt("serverid")
				appid, _ := ctx.Params().GetInt("appid")
				envid, _ := ctx.Params().GetInt("envid")

				appserverold := models.AppServer{}
				appserverold.ServerID = serverid
				appserverold.AppID = appid
				appserverold.EnvironmentID = envid

				res.Data, res.Error = appserver.UpdateAppServer(appserverold)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
			applicationserver.Delete("/{id:uint}", func(ctx iris.Context) {
				var res APIResult
				serverid, _ := ctx.Params().GetInt("id")
				appserver := models.AppServer{}
				appserver.ID = serverid
				appserver.GetAppServerByID()
				res.Error = appserver.DeleteAppServerByID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
			applicationserver.Put("/", func(ctx iris.Context) {

				var res APIResult
				appserver := models.AppServer{}
				res.Error = ctx.ReadJSON(&appserver)
				fmt.Println("APPSERVER ", appserver)
				res.Data, res.Error = appserver.NewAppServer()
				if res.Error != nil {
					fmt.Println("ERROR ", res.Error)
					res.Render(ctx)
					return
				}
				res.Render(ctx)
			})
			applicationserver.Get("/all/{serverid:uint}/{appid:uint}/{envid:uint}", func(ctx iris.Context) {
				var res APIResult
				serverid, _ := ctx.Params().GetInt("serverid")
				appid, _ := ctx.Params().GetInt("appid")
				envid, _ := ctx.Params().GetInt("envid")

				appserver := models.AppServer{}
				appserver.ServerID = serverid
				appserver.AppID = appid
				appserver.EnvironmentID = envid

				res.Data, res.Error = appserver.GetAppServerByALLID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
		}

		version := v1.Party("/version")
		version.Use(CheckAuth)
		{

			// PUT /api/v1/version
			version.Put("/", func(ctx iris.Context) {

				var res APIResult
				data := models.Version{}
				res.Error = ctx.ReadJSON(&data)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Data, res.Error = data.NewVersion()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
		}

		batch := v1.Party("/batch")
		batch.Use(CheckAuth)
		{
			// Get all batch
			// GET /api/v1/batch
			batch.Get("/", func(ctx iris.Context) {

				var res APIResult
				batch := models.Batch{}
				res.Data, res.Error = batch.GetBatch()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// Get spesific batch
			// GET /api/v1/batch/1
			batch.Get("/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				batch := models.Batch{}
				batch.ID = id
				res.Data, res.Error = batch.GetBatchByID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// PUT /api/v1/batch
			batch.Put("/", func(ctx iris.Context) {

				var res APIResult
				data := models.Batch{}
				res.Error = ctx.ReadJSON(&data)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Data, res.Error = data.NewBatch()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// POST /api/v1/batch
			batch.Post("/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				data := models.Batch{}
				data.ID = id
				res.Error = ctx.ReadJSON(&data)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Data, res.Error = data.UpdateBatch()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
		}

		stage := v1.Party("/stages")
		stage.Use(CheckAuth)
		{

			// Get all stages
			// GET /api/v1/stages/1
			stage.Get("/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				data := models.Batch{}
				data.ID = id

				res.Data, res.Error = data.GetStagesByBatchID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			//PUT /api/v1/stages
			stage.Put("/", iris.LimitRequestBodySize(maxSize), func(ctx iris.Context) {

				var res APIResult
				data := models.Version{}
				data.BatchID, _ = ctx.PostValueInt("batchid")
				data.AppID, _ = ctx.PostValueInt("appid")
				textAreaTarget := ctx.PostValue("textAreaTarget")
				app.Logger().Infof("textAreaTarget: %s", textAreaTarget)

				// Get the file from the request.
				file, info, err := ctx.FormFile("uploadfile")
				if err != nil {
					ctx.StatusCode(iris.StatusInternalServerError)
					ctx.HTML("Error while uploading 1: <b>" + err.Error() + "</b>")
					return
				}

				defer file.Close()

				// buf := bytes.NewBuffer(nil)
				// if _, err = io.Copy(buf, file); err != nil {
				// 	res.Error = err
				// 	res.Render(ctx)
				// 	return
				// }

				fileModel := models.File{}
				fileModel.Content = []byte(textAreaTarget) //buf.Bytes()
				fileUploaded, err3 := fileModel.UploadFile()
				res.Error = err3

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				data.FileID = fileUploaded.ID
				data.FileName = info.Filename
				res.Data, res.Error = data.NewVersion()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// // Update stages
			// // POST /api/v1/stages
			// stage.Post("/{id:int}", func(ctx iris.Context) {

			// 	id, _ := ctx.Params().GetInt("id")

			// 	var res APIResult
			// 	data := models.Stage{}
			// 	data.BatchID = id

			// 	res.Error = ctx.ReadJSON(&data)

			// 	if res.Error != nil {
			// 		res.Render(ctx)
			// 		return
			// 	}

			// 	res.Error = data.UpdateStage()

			// 	if res.Error != nil {
			// 		res.Render(ctx)
			// 		return
			// 	}

			// 	res.Render(ctx)
			// })

			// DELETE /api/v1/stages/1
			stage.Delete("/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				data := models.Version{}
				data.ID = id

				res.Error = data.DeleteStagesByID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

		}

		conn := v1.Party("/connection")
		conn.Use(CheckAuth)
		{
			// Get all batch
			// GET /api/v1/batch
			conn.Get("/", func(ctx iris.Context) {

				var res APIResult
				res.Data, res.Error = models.GetConnections()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			conn.Get("/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				connection := models.Connection{}
				connection.ID = id
				res.Data, res.Error = connection.GetConnectionByID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
			conn.Get("/userdb/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				connection := models.Connection{}
				connection.ID = id
				res.Data, res.Error = connection.GetUserDatabases()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			conn.Put("/", func(ctx iris.Context) {

				var res APIResult
				data := models.Connection{}
				res.Error = ctx.ReadJSON(&data)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Data, res.Error = data.NewConnection()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			conn.Post("/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")

				var res APIResult
				data := models.Connection{}
				data.ID = id
				res.Error = ctx.ReadJSON(&data)
				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Data, res.Error = data.UpdateConnection()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
			conn.Delete("/{id:int}", func(ctx iris.Context) {

				id, _ := ctx.Params().GetInt("id")
				var res APIResult
				data := models.Connection{}
				data.ID = id
				if res.Error != nil {
					res.Render(ctx)
					return
				}
				res.Error = data.DeleteConnectionByID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
		}
		authorization := v1.Party("/authorization")
		authorization.Use(CheckAuth)
		{
			// Get all batch
			// GET /api/v1/batch
			authorization.Get("/{id:int}", func(ctx iris.Context) {

				var res APIResult
				id, _ := ctx.Params().GetInt("id")
				auth := models.Authorization{}
				auth.ID = id
				res.Data, res.Error = auth.GetAuths()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
			authorization.Get("/getuserdb/{connectionid:int}/{dbname:string}/{role:string}", func(ctx iris.Context) {

				var res APIResult
				connectionid, _ := ctx.Params().GetInt("connectionid")
				dbname := ctx.Params().Get("dbname")
				role := ctx.Params().Get("role")
				auth := models.Authorization{}
				auth.ConnectionID = connectionid
				auth.Role.Name = role
				auth.DBName = dbname
				res.Data, res.Error = auth.GetRoleUserDB()
				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
			authorization.Get("/getroledb/{connectionid:int}/{dbname:string}", func(ctx iris.Context) {

				var res APIResult
				connectionid, _ := ctx.Params().GetInt("connectionid")
				dbname := ctx.Params().Get("dbname")
				auth := models.Authorization{}
				auth.ConnectionID = connectionid
				auth.DBName = dbname
				res.Data, res.Error = auth.GetRoleDB()
				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// authorization.Get("/{id:int}", func(ctx iris.Context) {

			// 	id, _ := ctx.Params().GetInt("id")

			// 	var res APIResult
			// 	auth := models.Authorization{}
			// 	auth.ID = id
			// 	res.Data, res.Error = auth.GetAuthorizationByID()

			// 	if res.Error != nil {
			// 		res.Render(ctx)
			// 		return
			// 	}

			// 	res.Render(ctx)
			// })

			// authorization.Put("/", func(ctx iris.Context) {

			// 	var res APIResult
			// 	data := models.Authorization{}
			// 	res.Error = ctx.ReadJSON(&data)

			// 	if res.Error != nil {
			// 		res.Render(ctx)
			// 		return
			// 	}

			// 	res.Data, res.Error = data.NewAuthorization()

			// 	if res.Error != nil {
			// 		res.Render(ctx)
			// 		return
			// 	}

			// 	res.Render(ctx)
			// })

			authorization.Post("/", func(ctx iris.Context) {
				var res APIResult
				data := models.Authorization{}
				res.Error = ctx.ReadJSON(&data)
				if res.Error != nil {
					res.Render(ctx)
					return
				}
				res.Data, res.Error = data.UpdateAuth()
				if res.Error != nil {
					res.Render(ctx)
					return
				}
				res.Render(ctx)
			})
			// authorization.Delete("/{id:int}", func(ctx iris.Context) {

			// 	id, _ := ctx.Params().GetInt("id")

			// 	var res APIResult
			// 	data := models.Authorization{}
			// 	data.ID = id
			// 	res.Error = ctx.ReadJSON(&data)

			// 	if res.Error != nil {
			// 		res.Render(ctx)
			// 		return
			// 	}

			// 	res.Error = data.DeleteAuthorizationByID()

			// 	if res.Error != nil {
			// 		res.Render(ctx)
			// 		return
			// 	}

			// 	res.Render(ctx)
			// })
		}

		userdb := v1.Party("/userdb")
		userdb.Use(CheckAuth)
		{

			userdb.Put("/", func(ctx iris.Context) {

				var res APIResult

				data := models.UserDB{}

				conn := models.Connection{}
				conn.ID = data.ConnectionID
				res.Error = ctx.ReadJSON(&data)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Data, res.Error = data.NewUserDatabase(conn)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			userdb.Post("/{connectionid:int}/{principalid:int}", func(ctx iris.Context) {

				connectionid, _ := ctx.Params().GetInt("connectionid")
				principalid, _ := ctx.Params().GetInt("principalid")

				var res APIResult
				data := models.UserDB{}
				data.ID = principalid
				conn := models.Connection{}
				conn.ID = connectionid

				res.Error = ctx.ReadJSON(&data)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Data, res.Error = data.UpdateUserDatabase(conn)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
			userdb.Delete("/{connectionid:int}/{userlogin:string}", func(ctx iris.Context) {

				connectionid, _ := ctx.Params().GetInt("connectionid")
				userlogin := ctx.Params().Get("userlogin")

				var res APIResult
				conn := models.Connection{}
				conn.ID = connectionid
				userdb := models.UserDB{}
				userdb.Name = userlogin
				if res.Error != nil {
					res.Render(ctx)
					return
				}
				res.Error = userdb.DeleteUserDatabase(conn)
				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
		}

		role := v1.Party("/role")
		role.Use(CheckAuth)
		{

			role.Get("/GetDBSchemaTrees/{connectionid:int}/{rolename:string}", func(ctx iris.Context) {

				connectionid, _ := ctx.Params().GetInt("connectionid")
				rolename := ctx.Params().Get("rolename")
				var res APIResult
				data := models.Role{}
				data.Name = rolename
				data.ConnectionID = connectionid
				res.Data, res.Error = data.GetDBSchemaTrees()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				ctx.JSON(res.Data)
			})
			role.Get("/GetFormTrees", func(ctx iris.Context) {

				var res APIResult
				data := models.Role{}
				res.Data, res.Error = data.GetFormTrees()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				ctx.JSON(res.Data)
			})
			role.Get("/GetFormTrees/{role:string}", func(ctx iris.Context) {
				role := ctx.Params().Get("role")
				var res APIResult
				data := models.Role{}
				res.Data, res.Error = data.GetFormTreesByRole(role)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				ctx.JSON(res.Data)
			})
			role.Get("/GetDBTableTrees/{connectionid:int}/{database:string}/{rolename:string}", func(ctx iris.Context) {

				connectionid, _ := ctx.Params().GetInt("connectionid")
				database := ctx.Params().Get("database")
				rolename := ctx.Params().Get("rolename")
				var res APIResult
				data := models.Role{}
				data.ConnectionID = connectionid
				data.DatabaseName = database
				data.Name = rolename
				res.Data, res.Error = data.GetDBTableTrees()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				ctx.JSON(res.Data)
			})

			role.Post("/", func(ctx iris.Context) {

				var res APIResult
				data := models.Role{}
				res.Error = ctx.ReadJSON(&data)
				if res.Error != nil {
					res.Render(ctx)
					return
				}
				res.Data, res.Error = data.UpdateRole()
				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
			role.Put("/", func(ctx iris.Context) {

				var res APIResult
				data := models.Role{}
				res.Error = ctx.ReadJSON(&data)
				if res.Error != nil {
					res.Render(ctx)
					return
				}
				res.Data, res.Error = data.UpdateRole()
				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
			role.Delete("/{connectionid:int}/{role:string}", func(ctx iris.Context) {

				connectionid, _ := ctx.Params().GetInt("connectionid")
				role := ctx.Params().Get("role")
				var res APIResult
				data := models.Role{}
				data.ConnectionID = connectionid
				data.Name = role
				res.Data, res.Error = data.DeleteRole()
				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

		}

		systemroles := v1.Party("/systemroles")
		systemroles.Use(CheckAuth)
		{
			systemroles.Put("/", func(ctx iris.Context) {

				var res APIResult
				res.Error = ctx.ReadJSON(&CasbinController)
				if res.Error != nil {
					res.Render(ctx)
					return
				}
				CasbinController.AddRole()
				if res.Error != nil {
					res.Render(ctx)
					return
				}
				res.Render(ctx)
			})
			systemroles.Post("/{role:string}", func(ctx iris.Context) {

				var res APIResult
				role := ctx.Params().Get("role")
				res.Error = ctx.ReadJSON(&CasbinController)
				if res.Error != nil {
					res.Render(ctx)
					return
				}
				CasbinController.UpdateRole(role)
				if res.Error != nil {
					res.Render(ctx)
					return
				}
				res.Render(ctx)
			})
			systemroles.Delete("/{role:string}", func(ctx iris.Context) {

				var res APIResult
				role := ctx.Params().Get("role")
				if res.Error != nil {
					fmt.Println(res.Error)
					res.Render(ctx)
					return
				}
				res.Message, res.Error = CasbinController.DeleteRole(role)
				if res.Error != nil {
					fmt.Println(res.Error)
					res.Render(ctx)
					return
				}
				res.Render(ctx)
			})

		}

		system := v1.Party("/system")
		system.Use(CheckAuth)
		{
			system.Delete("/{id:int}", func(ctx iris.Context) {

				var res APIResult
				var id int
				id, res.Error = ctx.Params().GetInt("id")
				if res.Error != nil {
					fmt.Println(res.Error)
					res.Render(ctx)
					return
				}
				var gs = models.GeneralSetting{}
				gs.ID = id
				res.Error = gs.DeleteGeneralSettingByID()
				if res.Error != nil {
					fmt.Println(res.Error)
					res.Render(ctx)
					return
				}
				res.Render(ctx)
			})
			system.Put("/", func(ctx iris.Context) {

				var res APIResult
				gs := models.GeneralSetting{}
				res.Error = ctx.ReadJSON(&gs)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Data, res.Error = gs.NewGeneralSetting()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			// Update server
			// POST /api/v1/servers
			system.Post("/{id:int}", func(ctx iris.Context) {
				var res APIResult
				id, err := ctx.Params().GetInt("id")
				res.Error = err
				data := models.GeneralSetting{}
				data.ID = id

				res.Error = ctx.ReadJSON(&data)

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Data, res.Error = data.UpdateGeneralSettingByID()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

		}

		generatekey := v1.Party("/generatekey")
		generatekey.Use(CheckAuth)
		{
			generatekey.Put("/", func(ctx iris.Context) {

				var res APIResult
				gk := models.GenerateKey{}

				res.Data, res.Error = gk.NewGenerateKey()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			generatekey.Post("/{id:int}", func(ctx iris.Context) {

				var res APIResult
				var id int
				id, res.Error = ctx.Params().GetInt("id")
				gk := models.GenerateKey{}
				gk.ID = id
				gk.DeployToAllServer()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})

			generatekey.Get("/", func(ctx iris.Context) {

				var res APIResult
				res.Data, res.Error = models.GetGeneralSettings()

				if res.Error != nil {
					res.Render(ctx)
					return
				}

				res.Render(ctx)
			})
		}

	}
}
