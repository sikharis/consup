function AddUpdateBatch(obj){
    var servertype = parseInt($("#data-servertypes").val());
    var name = $("#data-name").val();
    var user = $("#current-user").val();


    if (user == '') {
        alert("Please Login Again")
        return false;
    }{
        let servertypes = {
            "id": servertype
        }
        let info = {
            "UsrCrt": user,
            "UsrUpd": user
        }
        let batch = {
            "servertype": servertypes,
            "name": name,
            "info": info
        }
        $.ajax({
            type: 'PUT',
            url: 'api/v1/batch',
            contentType: 'application/json',
            data: JSON.stringify(batch), // access in body
        }).success(function (data) {
            console.log("sukses");
            renderURI(obj);
        }).error(function (data) {
            alert(data.error);
        });
    }
    
}