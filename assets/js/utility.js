function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

        var reader = new FileReader();
        reader.onload = function (event) {
            // NOTE: event.target point to FileReader
            var contents = event.target.result;
            var lines = contents.split('\n');
            let filebody = document.getElementById('textAreaTarget');
            filebody.value = contents;
            AddconfigProtectedData(filebody);

        };

        reader.readAsText(f);
    }
}

function AddconfigProtectedData(obj) {
    let rows = `<configProtectedData><providers><add keyContainerName="AdInsKeys" useMachineContainer="true" description="Uses RsaCryptoServiceProvider to encrypt and decrypt" name="CustomProvider" type="System.Configuration.RsaProtectedConfigurationProvider,System.Configuration, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" /></providers></configProtectedData>`;
    obj.value = insertBeforeLastOccurrence(obj.value, "</configuration>", rows);
}

function insertBeforeLastOccurrence(strToSearch, strToFind, strToInsert) {
    var n = strToSearch.lastIndexOf(strToFind);
    if (n < 0) return strToSearch;
    return strToSearch.substring(0, n) + strToInsert + strToSearch.substring(n);
}

function clean(obj) {
    for (var propName in obj) {
        if (obj[propName] === null || obj[propName] === undefined || obj[propName] === "") {
            delete obj[propName];
        }
    }
}