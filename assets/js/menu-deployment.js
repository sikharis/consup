function LoadDeployment(obj) {
	
	let content = '.tabs .content .content-body';
	
    $.get(obj.page, function (data) {
						
        $(content).html(data);       				
		//$(content).append('<input type="hidden" value="'++'"/>');
		
        $("#btn-link").bind("click", function (event) {
            let next = $(this).attr("data-next");
			let previous = $(this).attr("data-back");
            LoadDeployment(next);
        });		
		
        $("#btn-data-save").bind("click", function (event) {
            console.log("success");
            let next = $(this).attr("previouspage");
            LoadDeployment(next) 
        });
		
        $("#data-table>tr").each(function (index) {
            $(this).find("#btn-deploy-edit").bind("click", function (event) {
                let next = $(this).attr("nextpage");
                LoadDeployment(next) 
            });
        });
		
        $("#data-table>tr").each(function (index) {
            $(this).find("#btn-deploy-delete").bind("click", function (event) {
                if (confirm("Are you sure Wanna delete this?")) {
                };
            });
        });
		
        if ($("#data-file").length != 0) {
            document.getElementById('data-file').addEventListener('change', handleFileSelect, false);
        }
		
        $(".clickable-row").click(function () {
            let next = $(this).attr("nextpage");
            LoadDeployment(next) 
        });
       
        
    });
}