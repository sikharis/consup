function LoadApplication(obj, data) {
    let counter = 0;
    if (data.data != null) {
        $.each(data.data, function (i, item) {
            counter++;
            var rows =
                "<tr>" +
                "<td id='id'>" + counter + " </td>" +
                "<td id='servername'>" + item.server.name + "</td>" +
                "<td id='name'>" + item.name + "</td>" +
                "<td id='pathapp'>" + item.pathapp + "</td>" +
                "<td id='pathconf'>" + item.pathconf + "</td>" +
                "<td id='usrupd'>" + item.info.usrupd + "</td>" +
                "<td class='text-center'> <button type='button' id='btn-edit' class='btn btn-default' data-key='" + item.id + "' data-toggle='modal' data-target='#data-target-modal'>Edit</button></td>" +
                "<td class='text-center'> <button type='button' id='btn-delete' class='btn btn-default' data-key='" + item.id + "'>Delete</button> </td>" +
                "</tr>";

            $('#data-table').append(rows);
        }); //End of foreach Loop   
    }

}


function EmptyApplication() {
    $("#data-name").val();
    $("#data-pathapp").val();
    $("#data-pathconf").val();
}

function AddUpdateApplication() {
    var id = $("#data-id").val();
    var serverid = parseInt($("#data-servers").val());
    var name = $("#data-name").val();
    var pathapp = $("#data-pathapp").val();
    var pathconf = $("#data-pathconf").val();
    var user = $("#current-user").val();


    if (user == '') {
        alert("Please Login Again")
    } else {
        let server = {
            "id": serverid
        }
        let app = {
            "server": server,
            "name": name,
            "pathapp": pathapp,
            "pathconf": pathconf,
            "info": info
        }
        if (id.length <= 0) { //New  Insert 
            $.ajax({
                type: 'PUT',
                url: 'api/v1/apps',
                contentType: 'application/json',
                data: JSON.stringify(app), // access in body
            }).success(function (data) {
                alert("Success");
            }).error(function (data) {
                alert(data.error);
            });
        } else {
            $.ajax({
                type: 'POST',
                url: 'api/v1/apps/' + id,
                contentType: 'application/json',
                data: JSON.stringify(app), // access in body
            }).success(function (data) {
                alert("Success");
            }).error(function (data) {
                alert(data.error);
            });
        }
    }
    show_content($("#application-list"));
    ModalHide("#data-target-modal");
}