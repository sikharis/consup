$(function () {
    var uri = location.origin + "/dashboard";
    $("body").append('<input type="hidden" id="back-link" value="' + uri + '" />');
    $('.ui.celled.selection.list > .item').on('click', function (e) {
        e.preventDefault();
        $('.ui.celled.selection.list .item').removeClass('active');
        $(this).closest(".item").addClass("active");
        renderURI($(this));
    });

    //call default page in dashboard
    renderURI($("#"+$("#firstpage").val()));
});

let loadingFlag = 0;

function renderURI(obj) {

    loading('.content-body', 'stop');

    let index = 1;
    let uri = "";
    let result = [];
    let out = false;

    do {
        uri = $(obj).data("uri-" + index);
        index++;

        if (uri === undefined) {
            out = true;
            continue;
        }

        if (uri == "") {
            continue;
        }

        var splitArr = uri.split(" ");
        var ajaxType = "GET";

        splitArr.forEach(val => {
            if (["POST", "GET", "DELETE", "PUT"].includes(val)) {
                ajaxType = val.trim();
            } else {
                if (val.trim() != "") {
                    uri = val;
                }
            }
        });

        result.push({
            type: ajaxType,
            uri: uri
        });
    }
    while (!out);

    Promise.resolve(result).then(RecurseURI);
}

function renderButton() {
    $(".table button").each(function (index) {
        $(this).on("click", function (e) {
            e.preventDefault();
            renderURI($(this));
        });
        $(this).removeClass("mini").addClass("mini");
    });

    $(".content-body button").not(".table button").each(function (index) {
        $(this).on("click", function (e) {
            e.preventDefault();
            renderURI($(this));
        });
        $(this).removeClass("tiny").addClass("tiny");
    });

    $("select.dropdown").each(function (index) {
        var data = $(this).data("json");
        if (data !== undefined) {
            $(this).dropdown(data);
        } else {
            $(this).dropdown();
        }
    });

    $(".ui.form").each(function (index) {
        $(this).addClass("small");
    });

    $("table").each(function (index) {
        $(this).removeClass("selectable celled very compact teal small action").addClass("selectable celled very compact teal small action");
    });
}

//Get value from an input field
function getFieldValue(fieldId) {
    // 'get field' is part of Semantics form behavior API
    return $('.ui.form').form('get field', fieldId).val();
}

function loading(type, action) {
    if (action == undefined) {
        loadingFlag = 1;
        $(type).loading({
            onStart: function (loading) {
                loading.overlay.fadeIn(0);
            },
            onStop: function (loading) {
                loading.overlay.fadeOut(100);
            }
        });
        return;
    } else if (action == 'stop') {
        loadingFlag = 0;
        $(type).loading('stop');
    }
}

// Recurse on a given promise chain
function RecurseURI(arr) {
    return new Promise(function (resolve, reject) {

        if (loadingFlag == 0) {
            loading('.content-body');
        }
        let current = arr.shift();
        var param = {
            url: current.uri,
            type: current.type
        };

        if (["POST", "PUT"].includes(current.type)) {
            if ($("input:file").length) {
                param.processData = false;
                param.contentType = false;

                paramData = new FormData();
                json = $('.ui.form').serializeJSON({
                    parseNumbers: true
                })
                
                $.each(json, function (key, val) {
                    paramData.append(key, val);
                });
                $("input:file").each(function (index, field) {
                    paramData.append(field.name, field.files[0]);
                });

                param.data = paramData;
            } else {
                param.data = JSON.stringify($('.ui.form').serializeJSON({
                    parseNumbers: true
                }));
            }
        }

        if ("DELETE" == current.type) {

            $('.ui.content.modal > .header').html("Delete Confirmation");
            $('.ui.content.modal > .content').html("Are you sure you want to delete this data");

            $('.ui.content.modal').modal({
                closable: true,
                context: 'body',
                duration: 250,
                onDeny: function () {
                    sessionStorage.SessionName = "NET_SECURE";
                    if(sessionStorage.getItem("LAST_URL") !== null )
                    {
                        let result = []
                        result.push({
                            type: "GET",
                            uri: sessionStorage.getItem("LAST_URL")
                        });
                        Promise.resolve(result).then(RecurseURI);
                    }
                },
                onApprove: function () {

                    ajax = $.ajax(param);
                    ajax.done(function (data, status, jqXHR) {
                        if(data.message != undefined && data.message != ""){
                            LoggingAdd("Info", new Date().toLocaleString(), data.message);
                        }
                        if (arr.length > 0) {
                            RecurseURI(arr);
                        } else {
                            loading('.content-body', 'stop');
                        }
                        resolve(true);
                    });

                    ajax.fail(function (data, status, jqXHR) {
                        if(data.error != undefined && data.error != ""){
                            LoggingAdd("Error", new Date().toLocaleString(), data.error);
                        }
                        reject(jqXHR);
                        if(data.status == 403)
                        {
                            $('.ui.content.modal > .header').html("Forbidden");
                            $('.ui.content.modal > .content').html("You are not authorized to access the requested resource");
                
                            $('.ui.content.modal').modal({
                                closable: true,
                                context: 'body',
                                duration: 250,
                                onDeny: function () {
                                    sessionStorage.SessionName = "NET_SECURE";
                                    if(sessionStorage.getItem("LAST_URL") !== null )
                                    {
                                        let result = []
                                        result.push({
                                            type: "GET",
                                            uri: sessionStorage.getItem("LAST_URL")
                                        });
                                        Promise.resolve(result).then(RecurseURI);
                                    }
                                },
                                onApprove: function () {
                                    sessionStorage.SessionName = "NET_SECURE";
                                    if(sessionStorage.getItem("LAST_URL") !== null )
                                    {
                                        let result = []
                                        result.push({
                                            type: "GET",
                                            uri: sessionStorage.getItem("LAST_URL")
                                        });
                                        Promise.resolve(result).then(RecurseURI);
                                    }
                                }
                            }).modal('show');
                        }
                    });

                    ajax.always(function () {

                    });
                }
            }).modal('show');

        } else {

            var ajax = $.ajax(param);
            ajax.done(function (data, status, jqXHR) {
                if(data.message != undefined && data.message != ""){
                    LoggingAdd("Info", new Date().toLocaleString(), data.message);
                }
                var ct = jqXHR.getResponseHeader("content-type") || "";

                if (data.redirectURL !== undefined) {
                    window.location.replace(data.redirectURL);
                } else {

                    if (ct.indexOf('html') > -1) {
                        sessionStorage.SessionName = "NET_SECURE";
                        sessionStorage.setItem("LAST_URL",param.url);
                        $('.content-body').html(data);
                    }
                }

                if (arr.length > 0) {
                    RecurseURI(arr);
                } else {
                    renderButton();
                    loading('.content-body', 'stop');
                }

                resolve(true);
            });

            ajax.fail(function (data, status, jqXHR) {
                
                if(data.error != undefined && data.error != ""){
                    LoggingAdd("Error", new Date().toLocaleString(), data.error);
                }
                reject(false);
                if(data.status == 403)
                {
                    $('.ui.content.modal > .header').html("Forbidden");
                    $('.ui.content.modal > .content').html("You are not authorized to access the requested resource");
        
                    $('.ui.content.modal').modal({
                        closable: true,
                        context: 'body',
                        duration: 250,
                        onDeny: function () {
                            sessionStorage.SessionName = "NET_SECURE";
                            if(sessionStorage.getItem("LAST_URL") !== null )
                            {
                                let result = []
                                result.push({
                                    type: "GET",
                                    uri: sessionStorage.getItem("LAST_URL")
                                });
                                Promise.resolve(result).then(RecurseURI);
                            }
                        },
                        onApprove: function () {
                            sessionStorage.SessionName = "NET_SECURE";
                            if(sessionStorage.getItem("LAST_URL") !== null )
                            {
                                let result = []
                                result.push({
                                    type: "GET",
                                    uri: sessionStorage.getItem("LAST_URL")
                                });
                                Promise.resolve(result).then(RecurseURI);
                            }
                        }
                    }).modal('show');
                }
                
                // LoggingAdd("ERROR", new Date().toLocaleString(), xhr.responseText);
            });

            ajax.always(function () {

            });
        }
    });
}

function LoggingAdd(type, time, message) {
    $("#LoggingPanel tbody").append(`<tr>
    <td>` + type + `</td>
    <td>` + time + `</td>
    <td>` + message + `</td>
    </tr>`)
}

function RenderPage(obj, data) {

    if (data.data !== null) {

        if (data.data.uri !== undefined) {

            if (data.data.prev.id != 0) {
                $("div.content-body button[data-prev]").each(function () {
                    let counter = 1;
                    if (data.data.prev.page != "") {
                        $(this).attr("data-uri-" + counter, data.data.prev.page);
                        counter++;
                    }
                    if (data.data.prev.link != "") {
                        $(this).attr("data-uri-" + counter, data.data.prev.link);
                        counter++;
                    }
                    if (data.data.prev.id != 0) {
                        $(this).attr("data-uri-" + counter, "api/v1/navigation/" + data.data.prev.id);
                        counter++;
                    }


                });
            }

            if (data.data.next.id != 0) {
                $("div.content-body button[data-next]").each(function () {
                    let counter = 1;
                    if (data.data.next.page != "") {
                        $(this).attr("data-uri-" + counter, data.data.next.page);
                        counter++;
                    }
                    if (data.data.next.link != "") {
                        $(this).attr("data-uri-" + counter, data.data.next.link);
                        counter++;
                    }
                    if (data.data.next.id != 0) {
                        $(this).attr("data-uri-" + counter, "api/v1/navigation/" + data.data.next.id);
                        counter++;
                    }

                });
            }
        }
    }
}

function toggleFullScreen(elem) {
    // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
        if (elem.requestFullScreen) {
            elem.requestFullScreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
}