$(function () {

     messageTxt = $("#messageTxt");
     messages = $("#messages");

     /* secure wss because we ListenTLS */
     let scheme = document.location.protocol == "https:" ? "wss" : "ws";
     let port = document.location.port ? (":" + document.location.port) : "";
     let webURL = document.location.protocol + "//" + document.location.hostname + port + "";
     let wsURL = scheme + "://" + document.location.hostname + port + "/socket";
     let logoutURL = webURL + "/api/v1/auth/forceLogout"
     socket = new Ws(wsURL);
     let socketStatus = $("#socketStatus");

     socket.OnConnect(function () {
          socket.Emit("visit");
     });

     socket.OnDisconnect(function () {
          SetStatus(0, "Disonnected");

          $('.ui.content.modal > .header').html("Reconnect Confirmation");
          $('.ui.content.modal > .content').html("Connection lost, reconnect to server..");

          $('.ui.content.modal').modal({
               closable: false,
               context: 'body',
               duration: 250,
               onDeny: function () {
                    return true;
               },
               onApprove: function () {
                    window.location.replace(logoutURL);
               }
          }).modal('show');
     });

     socket.On("visit", function (data) {
          SetStatus(1, data);
     });

     socket.On("logout", function (msg) {
          if ($("#loggedAs").val() == msg) {
               window.location.replace(logoutURL);
          }
     });

     socket.On("deploy", function (msg) {
          console.log(msg)
     });

     function SetStatus(status, msg) {
          socketStatus.html('');
          if (status == 0) {
               msg = msg + '&nbsp;&nbsp;<i class="sync alternate icon"></i>';
               socketStatus.html(msg);
               socketStatus.css({
                    "background-color": "#f7fff7",
                    "color": "red",
                    "font-weight": "bold"
               });
          } else {
               let jsonObj = JSON.parse(msg);
               let countUser = 1;
               let strHtml = "";
               if (jsonObj.length > 0) {
                    Object.keys(jsonObj).forEach(function (k) {
                         // console.log(jsonObj[k]);
                         strHtml += '<a class="item">' + jsonObj[k].Username + '</a>';
                    });
                    countUser = jsonObj.length;
               } else {
                    strHtml += '<a class="item">' + jsonObj.Username + '</a>';
               }
               strHtml = countUser + ' Connected <div class="menu">' + strHtml;
               strHtml = strHtml + '</div>';
               socketStatus.html(strHtml);
               socketStatus.css({
                    "background-color": "#f7fff7",
                    "color": "green",
                    "font-weight": "bold"
               });
          }
          $("#socketStatus").dropdown();
     }

     $("#btnSignout").click(function () {
          // w.Emit("chat", messageTxt.val().toString());
          // messageTxt.val("");
          socket.Emit("logout", "signout");
     });
})