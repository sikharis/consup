$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return JSON.stringify(o);
};

function send() {
    event.preventDefault();

    var test = JSON.stringify($('.ui.form').form('get values'));

    if ($('.ui.form').form('is valid')) {
        let ajax = $.ajax({
            type: "POST",
            url: "/api/v1/auth/login",
            data: $('.ui.form').form('get values'),
            beforeSend: function () {
                $("#btn-submit").addClass("loading");
            }
        });

        ajax.done(function (data, status, jqXHR) {
            if (data !== undefined) {
                if (data.error != "") {
                    $(".ui.warning.message").html(`<ul class="list"><li>Wrong password or username</li></ul>`);
                    $(".ui.form").addClass("warning");
                } else {
                    if (data.redirectURL != "") {
                        window.location.replace(data.redirectURL);
                    }
                }
            }
            $("#btn-submit").removeClass("loading");
        });

        ajax.fail(function (xhr) {
            console.log(xhr.status);
            console.log(xhr.responseText);
        });

        ajax.always(function () {
            // $("#btn-submit").removeClass("loading");
        });
    }
}

function addListenerMulti(el, s, fn) {
    s.split(' ').forEach(e => el.addEventListener(e, fn, false));
}

$(function () {
    $('.ui.form').form({
        fields: {
            email: {
                identifier: 'username',
                rules: [{
                    type: 'empty',
                    prompt: 'Please enter your username'
                }]
            },
            password: {
                identifier: 'password',
                rules: [{
                        type: 'empty',
                        prompt: 'Please enter your password'
                    },
                    {
                        type: 'length[6]',
                        prompt: 'Your password must be at least 6 characters'
                    }
                ]
            }
        }
    });

    // var btnSubmit = document.getElementById("#btn-submit");

    // btnSubmit.addEventListener('click', send);
    // btnSubmit.addEventListener("keyup", function (event) {
    //     // Cancel the default action, if needed
    //     event.preventDefault();
    //     // Number 13 is the "Enter" key on the keyboard
    //     if (event.keyCode === 13) {
    //         // Trigger the button element with a click
    //         document.getElementById("btnSubmit").click();
    //     }
    // });


    document.getElementById("btn-submit").addEventListener('click', send);
    $(document).on('keypress', function (e) {
        if (e.which == 13) {
            send();
        }
    });
});