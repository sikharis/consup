function LoadServerType(obj, data) {
    let counter = 0;
    if (data.data != null) {
        $.each(data.data, function (i, item) {
            counter++;
            var rows =
                "<tr>" +
                "<td >" + counter + "</td>" +
                "<td >" + item.name + "</td>" +
                "<td >" + item.info.usrupd + "</td>" +
                "<td class='text-center'> <button type='button' id='btn-edit' class='btn btn-default' data-key='" + item.id + "' data-toggle='modal' data-target='#data-target-modal'>Edit</button></td>" +
                "<td class='text-center'> <button type='button' id='btn-delete' class='btn btn-default' data-key='" + item.id + "'>Delete</button> </td>" +
                "</tr>";

            $('#data-table').append(rows);
        }); //End of foreach Loop   
    }
}

function EmptyServerType() {
    $("#data-id").val("");
    $("#data-name").val("");
}

function GetServerNameByID(id) {

    $.ajax({
        type: "GET",
        url: '/api/v1/servertypes/id/' + id,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            return data.data.name;
        },
        error: function (data) {
            alert(data.error);
        }
    });

}

function AddUpdateServerType() {
    var Id = $("#data-id").val();
    var ServerName = $("#data-name").val();
    var user = $("#current-user").val();
    let data = {
        "name": ServerName
    }
    if (Id.length <= 0) { //New  Insert 
        $.ajax({
            type: 'PUT',
            url: '/api/v1/servertypes',
            contentType: 'application/json',
            data: JSON.stringify(data), // access in body
        }).success(function () {
            alert("Success");
        }).error(function (data) {
            alert(data.error);
        });
    } else {
        $.ajax({
            type: 'POST',
            url: '/api/v1/servertypes/' + Id,
            contentType: 'application/json',
            data: JSON.stringify(data), // access in body
        }).success(function () {
            alert("Success");
        }).error(function (msg) {
            alert(data.error);
        });

    }

}