function LoadlinkTab(obj, data) {
    let counter = 0;
    $.each(data.data.linktab, function (i, item) {
        counter++;
        var rows = `
        <li >
            <a id="tag-link" 
                data-toggle="tab"  
                page="` + item.link.page + `"  
                tab-id="` + item.link.id + `"  
                tab-previous="` + item.previous.id + `" 
                tab-next="` + item.next.id + `"  
                href="#tab-content-body"> ` +
            item.link.title + ` 
        </a></li>`;


        $('#tab-header').append(rows);
        if (counter === 1) {
            add_tab_active(item.link.id);
            show_tab($("#tag-link"));
            fill_tab_info($("#tag-link"));

        }


    }); //End of foreach Loop  

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        let current = $("#hdnlinkcurrent").val();
        let after = $("#hdnlinkafter").val();
        let back = $("#hdnlinkbefore").val();
        var target = $(e.target).attr("tab-id"); // activated 

        if (after == target) {
            let nextlink = $(e.target);
            fill_tab_info(nextlink);
            show_tab(nextlink);
            tab_active(current, after);
        } else if (back == target) {
            let nextlink = $(e.target);
            fill_tab_info(nextlink);
            show_tab(nextlink);
            tab_active(current, back);
        } else {
            alert("Please continue Step By Step")
            let nextlink = $('#tag-link[tab-id="' + current + '"]');
            fill_tab_info(nextlink);
            show_tab(nextlink)
            tab_active(target, current);
        }
    });
}


function show_tab(obj) {
    $.get(obj.attr("page"), function (data) {
        $('#tab-content-body').html(data);
        generate_button(obj);
        if ($("#data-file").length != 0) {
            document.getElementById('data-file').addEventListener('change', handleFileSelect, false);
        }



        $("#btn-link-back").bind("click", function (event) {
            let current = $("#hdnlinkcurrent").val();
            let back = $("#hdnlinkbefore").val();
            let backlink = $('#tag-link[tab-id="' + back + '"]')
            fill_tab_info(backlink)
            show_tab(backlink);
            tab_active(current, back);
        });
        $("#btn-link-continue").bind("click", function (event) {
            let current = $("#hdnlinkcurrent").val();
            let after = $("#hdnlinkafter").val();
            let nextlink = $('#tag-link[tab-id="' + after + '"]')
            fill_tab_info(nextlink)
            show_tab(nextlink);
            tab_active(current, after);
        });
        // $(".clickable-row").click(function () {
        //     alert("Click");
        // });
    });
}

function generate_button(obj) {
    let previous = obj.attr("tab-previous");
    let next = obj.attr("tab-next");


    let rows = `<div class="row"> <div class="col-md-8">`;
    if (previous != 0) {
        rows += `<button id="btn-link-back" type="submit" class="btn default">Previous</button>`;
    }
    if (next != 0) {
        rows += `<button id="btn-link-continue" type="submit" class="btn default">Save & Continue</button>`;
    } else {
        rows += `<button id="btn-link-finish" type="submit" class="btn default">Finish</button>`;
    }
    rows += `</div></div>`;
    $("#btn-container").append(rows);

}

function fill_tab_info(obj) {
    $('#hdnlinkbefore').val(obj.attr("tab-previous"))
    $('#hdnlinkafter').val(obj.attr("tab-next"))
    $('#hdnlinkcurrent').val(obj.attr("tab-id"))
}

function tab_active(before, after) {
    remove_tab_active(before);
    add_tab_active(after);
}

function remove_tab_active(id) {
    $('#tag-link[tab-id="' + id + '"]').parent().closest('li').removeClass("active");
}

function add_tab_active(id) {
    $('#tag-link[tab-id="' + id + '"]').parent().closest('li').addClass("active");
}