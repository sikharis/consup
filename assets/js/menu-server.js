function AddUpdateServer(obj) {
    var Id = $("#data-id").val();
    var ServerName = $("#data-name").val();
    var IpAddress = $("#data-ip").val();
    var Username = $("#data-authusername").val();
    var Passowrd = $("#data-authpassword").val();
    var servertypeid = parseInt($("#ServerType").val());
    var user = $("#current-user").val();


    let serverType = {
        "id": servertypeid
    }
    let data = {
        "servertype": serverType,
        "name": ServerName,
        "ip": IpAddress,
        "authusername": Username,
        "authpassword": Passowrd,
    }
    if (Id.length <= 0) { //New  Insert 
        $.ajax({
            type: 'PUT',
            url: '/api/v1/servers',
            contentType: 'application/json',
            data: JSON.stringify(data), // access in body
        }).success(function () {
            renderURI(obj);
        }).error(function (data) {
            alert(data.error);
        });
    } else {
        $.ajax({
            type: 'POST',
            url: '/api/v1/servers/' + Id,
            contentType: 'application/json',
            data: JSON.stringify(data), // access in body
        }).success(function () {
            renderURI(obj);
        }).error(function (msg) {
            alert(data.error);
        });
    }

}