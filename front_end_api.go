package main

import (
	"fmt"
	"strings"

	"consup/models"

	"github.com/kataras/iris"
)

type APIFrontend struct {
	Data       interface{} `json:"data"`
	PathPage   string      `json:"pathpage"`
	PageFile   string      `json:"pagefile"`
	Message    string      `json:"message"`
	Error      error       `json:"error"`
	StatusCode int
}

func (page *APIFrontend) Render(ctx iris.Context) {

	if page.PathPage == "" {
		page.PathPage = "pages/"
	}
	ctx.ViewData("PageData", page)
	ctx.View(page.PathPage + page.PageFile)
}

func frontEndAPI(app *iris.Application) {

	app.OnErrorCode(iris.StatusNotFound, func(ctx iris.Context) {
		ctx.HTML("<b>Resource Not found</b>")
	})
	index := app.Party("/").Layout(DefaultLayout)
	{
		//handle root url
		index.Get("/", func(ctx iris.Context) {

			if isAuth(ctx) {
				ctx.Redirect("/dashboard", iris.StatusTemporaryRedirect)
				return
			}

			ctx.View("pages/login.html")
		})

		//handle dashboard url
		app.Get("/dashboard", CheckAuth, func(ctx iris.Context) {

			var res APIResult
			var name = sess.Start(ctx).GetString("name")
			var username = sess.Start(ctx).GetString("username")
			linklists, err := models.GetNavigations()
			if err != nil {
				res.Error = err
			}
			policyes := CasbinController.GetPolicyByUsername(username)
			fmt.Println(policyes)
			var arraylinklist []models.LinkList
			for i := range linklists {
				for j := range policyes {
					url := policyes[j][1]
					min := 0
					if string(url[min]) == "/" {
						url = removeStringAtIndex(url, min)
					}
					max := len(url)
					if string(url[max-1]) == "*" && max > min {
						url = removeStringAtIndex(url, max-1)
					}
					if strings.Contains(linklists[i].URI.Page, url) {
						arraylinklist = append(arraylinklist, linklists[i])
					}
				}
			}
			ctx.ViewData("firstPage", arraylinklist[0].URI.Name)
			ctx.ViewData("Name", name)
			ctx.ViewData("Username", username)
			ctx.ViewData("navigation", arraylinklist)

			ctx.View("layouts/dashboard.html")
		})

	}

	//handle all pages
	pages := app.Party("/pages")
	pages.Use(CheckAuth)
	{
		user := pages.Party("/user")
		{
			user.Get("/edit/{username:string}", func(ctx iris.Context) {
				var view APIFrontend
				username := ctx.Params().Get("username")
				user := models.User{}
				user.Username = username
				view.Data, view.Error = user.GetUserByUsername()
				if username == "Admin" {
					ctx.ViewData("isAdmin", "disabled")
				} else {
					ctx.ViewData("isAdmin", "")
				}
				ctx.ViewData("id", user.ID)
				ctx.ViewData("user", view.Data)
				ctx.ViewData("username", username)

				view.PageFile = "user/modify.html"
				view.Render(ctx)
			})
			user.Get("/add", func(ctx iris.Context) {
				var view APIFrontend
				view.PageFile = "user/modify.html"
				ctx.ViewData("id", 0)
				ctx.ViewData("isAdmin", "")
				ctx.ViewData("username", "")
				view.Render(ctx)
			})
			user.Get("", func(ctx iris.Context) {
				var view APIFrontend
				view.Data, view.Error = models.GetUsers()
				ctx.ViewData("user", view.Data)
				view.PageFile = "user/index.html"
				view.Render(ctx)
			})

		}
		systemroles := pages.Party("/systemroles")
		{
			systemroles.Get("", func(ctx iris.Context) {
				var view APIFrontend
				view.Data = remove(CasbinController.GetGroupingPolicy(), "admin")

				ctx.ViewData("Roles", view.Data)
				view.PageFile = "systemrole/index.html"
				view.Render(ctx)
			})
			systemroles.Get("/edit/{role:string}", func(ctx iris.Context) {
				var view APIFrontend
				role := ctx.Params().Get("role")
				ctx.ViewData("id", 1)
				ctx.ViewData("role", role)
				view.PageFile = "systemrole/modify.html"
				view.Render(ctx)

			})
			systemroles.Get("/add", func(ctx iris.Context) {
				var view APIFrontend
				ctx.ViewData("id", 0)
				ctx.ViewData("role", "")
				view.PageFile = "systemrole/modify.html"
				view.Render(ctx)

			})
		}
		environment := pages.Party("/environment")
		{
			environment.Get("/edit/{id:uint}", func(ctx iris.Context) {
				var view APIFrontend
				var id int
				id, view.Error = ctx.Params().GetInt("id")
				environment := models.Environment{}
				environment.ID = id
				view.Data, view.Error = environment.GetEnvironmentByID()
				ctx.ViewData("environment", view.Data)
				ctx.ViewData("id", id)
				view.PageFile = "environment/modify.html"
				view.Render(ctx)

			})
			environment.Get("/add", func(ctx iris.Context) {
				var view APIFrontend
				view.PageFile = "environment/modify.html"
				ctx.ViewData("id", 0)
				view.Render(ctx)
			})
			environment.Get("", func(ctx iris.Context) {
				var view APIFrontend
				view.Data, view.Error = models.GetEnvironments()
				ctx.ViewData("environment", view.Data)
				view.PageFile = "environment/index.html"
				view.Render(ctx)
			})

		}
		server := pages.Party("/server")
		{
			server.Get("/edit/{id:uint}", func(ctx iris.Context) {
				var view APIFrontend
				var id int
				id, view.Error = ctx.Params().GetInt("id")

				//Get Server Objects
				server := models.Server{}
				server.ID = id
				view.Data, view.Error = server.GetServerByID()
				ctx.ViewData("server", view.Data)
				ctx.ViewData("id", id)
				view.PageFile = "server/modify.html"
				view.Render(ctx)
			})
			server.Get("/add", func(ctx iris.Context) {
				var view APIFrontend
				ctx.ViewData("id", 0)
				view.PageFile = "server/modify.html"
				view.Render(ctx)
			})
			server.Get("/generatekey", func(ctx iris.Context) {
				var view APIFrontend
				view.Data, view.Error = models.GetGenerateKeys()
				fmt.Println("data ", view.Data)
				ctx.ViewData("generatekey", view.Data)
				view.PageFile = "generatekey/index.html"
				view.Render(ctx)
			})

			server.Get("", func(ctx iris.Context) {
				var view APIFrontend
				view.Data, view.Error = models.GetServers()
				ctx.ViewData("server", view.Data)
				view.PageFile = "server/index.html"
				view.Render(ctx)
			})
			server.Get("/appserver/{id:uint}", func(ctx iris.Context) {
				var view APIFrontend
				var id int
				id, view.Error = ctx.Params().GetInt("id")
				appserver := models.AppServer{}
				appserver.ServerID = id
				view.Data, view.Error = appserver.GetAppServerByServerID()
				ctx.ViewData("appserver", view.Data)
				ctx.ViewData("id", id)
				view.PageFile = "appserver/index.html"
				view.Render(ctx)
			})
			server.Get("/appserver/edit/{id:uint}", func(ctx iris.Context) {
				var view APIFrontend
				appserverid, err := ctx.Params().GetInt("id")
				if err != nil {
					view.Error = err
				}
				appserver := models.AppServer{}
				appserver.ID = appserverid
				appserver.GetAppServerByID()

				ctx.ViewData("appserver", appserver)
				ctx.ViewData("id", appserverid)
				ctx.ViewData("serverid", appserver.ServerID)
				ctx.ViewData("appid", appserver.AppID)
				ctx.ViewData("environmentid", appserver.EnvironmentID)
				view.PageFile = "appserver/modify.html"
				view.Render(ctx)
			})
			server.Get("/appserver/add/{serverid:uint}", func(ctx iris.Context) {
				var view APIFrontend
				var serverid int
				serverid, view.Error = ctx.Params().GetInt("serverid")

				ctx.ViewData("serverid", serverid)
				ctx.ViewData("appid", 0)
				ctx.ViewData("environmentid", 0)
				view.PageFile = "appserver/modify.html"
				view.Render(ctx)
			})

		}

		application := pages.Party("/apps")
		{
			application.Get("/edit/{id:uint}", func(ctx iris.Context) {
				var view APIFrontend
				var id int
				id, view.Error = ctx.Params().GetInt("id")
				apps := models.App{}
				apps.ID = id
				view.Data, view.Error = apps.GetAppByID()
				view.PageFile = "application/modify.html"
				ctx.ViewData("apps", view.Data)
				ctx.ViewData("id", id)
				view.Render(ctx)
			})

			application.Get("/add", func(ctx iris.Context) {
				var view APIFrontend
				view.PageFile = "application/modify.html"
				ctx.ViewData("id", 0)
				view.Render(ctx)
			})

			application.Get("", func(ctx iris.Context) {
				var view APIFrontend
				view.Data, view.Error = models.GetApps()
				ctx.ViewData("apps", view.Data)
				view.PageFile = "application/index.html"
				view.Render(ctx)
			})
		}

		deployment := pages.Party("/deployment")
		{
			batch := deployment.Party("/batch")
			{
				batch.Get("/edit/{id:int}", func(ctx iris.Context) {
					var view APIFrontend
					var id int
					id, view.Error = ctx.Params().GetInt("id")
					view.PageFile = "deployment/batch/modify.html"

					batch := models.Batch{}
					batch.ID = id
					batch, view.Error = batch.GetBatchByID()
					ctx.ViewData("id", id)
					ctx.ViewData("batch", batch)

					view.Render(ctx)
				})

				batch.Get("/add", func(ctx iris.Context) {
					var view APIFrontend
					view.Data, view.Error = models.GetEnvironments()
					ctx.ViewData("ServerType", view.Data)
					view.PageFile = "deployment/batch/modify.html"

					batch := models.Batch{}
					ctx.ViewData("id", 0)
					ctx.ViewData("batch", batch)

					view.Render(ctx)
				})

				batch.Get("/", func(ctx iris.Context) {
					var view APIFrontend
					batch := models.Batch{}

					view.Data, view.Error = batch.GetBatch()
					ctx.ViewData("batch", view.Data)
					view.PageFile = "deployment/batch/index.html"
					view.Render(ctx)
				})

			}
			stages := deployment.Party("/stages")
			{
				stages.Get("/edit/{id:uint}", func(ctx iris.Context) {
					var view APIFrontend
					var id int
					id, view.Error = ctx.Params().GetInt("id")
					view.PageFile = "deployment/stages/modify.html"
					ctx.ViewData("id", id)
					view.Render(ctx)
				})
				stages.Get("/add/{id:uint}", func(ctx iris.Context) {
					var view APIFrontend
					var id int
					id, view.Error = ctx.Params().GetInt("id")
					batch := models.Batch{}
					batch.ID = id
					batch, view.Error = batch.GetBatchByID()
					view.PageFile = "deployment/stages/modify.html"
					ctx.ViewData("batch", batch)
					view.Render(ctx)
				})
				stages.Get("/{id:int}", func(ctx iris.Context) {
					var view APIFrontend
					var id int
					id, view.Error = ctx.Params().GetInt("id")
					batch := models.Batch{}
					batch.ID = id
					view.Data, view.Error = batch.GetStagesByBatchID()
					ctx.ViewData("batchid", id)
					ctx.ViewData("versions", view.Data)
					view.PageFile = "deployment/stages/index.html"
					view.Render(ctx)
				})
			}
			apps := deployment.Party("/apps")
			{
				apps.Get("/edit/{id:uint}", func(ctx iris.Context) {
					var view APIFrontend
					var id int
					id, view.Error = ctx.Params().GetInt("id")
					view.PageFile = "deployment/apps/edit.html"
					ctx.ViewData("data-id", id)
					view.Render(ctx)
				})
				apps.Get("/add", func(ctx iris.Context) {
					var view APIFrontend
					view.PageFile = "deployment/apps/add.html"
					view.Render(ctx)
				})
				apps.Get("", func(ctx iris.Context) {
					var view APIFrontend
					view.PageFile = "deployment/apps/index.html"
					view.Render(ctx)
				})

			}
			version := deployment.Party("/version")
			{
				version.Get("/edit/{id:uint}", func(ctx iris.Context) {
					var view APIFrontend
					var id int
					id, view.Error = ctx.Params().GetInt("id")
					version := models.Version{}
					version.ID = id
					view.Data, view.Error = version.GetVersion()
					view.PageFile = "deployment/version/modify.html"
					ctx.ViewData("versionid", id)
					view.Render(ctx)
				})
				version.Get("/add/{id:uint}", func(ctx iris.Context) {
					var view APIFrontend
					var id int
					id, view.Error = ctx.Params().GetInt("id")
					version := models.Version{}
					version.ID = id
					view.PageFile = "deployment/version/modify.html"
					ctx.ViewData("batchid", id)
					view.Render(ctx)
				})
				version.Get("", func(ctx iris.Context) {
					var view APIFrontend
					view.PageFile = "deployment/version/index.html"
					view.Render(ctx)
				})
			}
		}

		database := pages.Party("/database")
		{
			auth := database.Party("/authorization")
			{
				auth.Get("/{connectionid:uint}", func(ctx iris.Context) {
					var view APIFrontend
					var connectionid int
					connectionid, view.Error = ctx.Params().GetInt("connectionid")
					auth := models.Authorization{}
					auth.ConnectionID = connectionid
					view.Data, view.Error = auth.GetAuths()
					ctx.ViewData("auth", view.Data)
					ctx.ViewData("connectionid", connectionid)
					view.PageFile = "database/authorization/index.html"
					view.Render(ctx)
				})
				auth.Get("/edit/{connectionid:uint}/{dbname:string}", func(ctx iris.Context) {
					var view APIFrontend
					var connectionid int
					connectionid, view.Error = ctx.Params().GetInt("connectionid")
					dbname := ctx.Params().Get("dbname")
					auth := models.Authorization{}
					auth.ConnectionID = connectionid
					auth.DBName = dbname
					//view.Data, view.Error = auth.GetAuthorizationByID()
					view.PageFile = "database/authorization/modify.html"
					ctx.ViewData("connectionid", connectionid)
					ctx.ViewData("dbname", dbname)
					ctx.ViewData("isEdit", 1)
					//ctx.ViewData("auth", view.Data)
					view.Render(ctx)
				})
				auth.Get("/add/{connectionid:uint}", func(ctx iris.Context) {
					var view APIFrontend
					var connectionid int
					connectionid, view.Error = ctx.Params().GetInt("connectionid")

					view.PageFile = "database/authorization/modify.html"
					ctx.ViewData("connectionid", connectionid)
					ctx.ViewData("isEdit", 0)
					ctx.ViewData("dbname", "")
					view.Render(ctx)
				})

			}
			conn := database.Party("/connection")
			{
				conn.Get("/", func(ctx iris.Context) {
					var view APIFrontend

					view.Data, view.Error = models.GetConnections()
					ctx.ViewData("conn", view.Data)
					view.PageFile = "database/connection/index.html"
					view.Render(ctx)
				})
				conn.Get("/edit/{id:uint}", func(ctx iris.Context) {
					var view APIFrontend
					var id int
					id, view.Error = ctx.Params().GetInt("id")
					connection := models.Connection{}
					connection.ID = id
					view.Data, view.Error = connection.GetConnectionByID()
					view.PageFile = "database/connection/modify.html"
					ctx.ViewData("id", id)
					ctx.ViewData("connection", view.Data)
					view.Render(ctx)
				})
				conn.Get("/add", func(ctx iris.Context) {
					var view APIFrontend
					view.PageFile = "database/connection/modify.html"
					ctx.ViewData("id", 0)
					view.Render(ctx)
				})

			}
			role := database.Party("/role")
			{
				role.Get("/{id:uint}", func(ctx iris.Context) {
					var view APIFrontend
					var id int
					id, view.Error = ctx.Params().GetInt("id")
					conn := models.Connection{}
					conn.ID = id
					view.Data, view.Error = conn.GetRoles()
					fmt.Println(view.Data)
					ctx.ViewData("roles", view.Data)
					ctx.ViewData("connectionid", id)
					view.PageFile = "database/role/index.html"
					view.Render(ctx)
				})
				role.Get("/edit/{connectionid:uint}/{rolename:string}", func(ctx iris.Context) {
					var view APIFrontend
					var connectionid int
					connectionid, view.Error = ctx.Params().GetInt("connectionid")
					rolename := ctx.Params().Get("rolename")
					role := models.Role{}
					role.ConnectionID = connectionid
					role.Name = rolename
					view.Data, view.Error = role.GetRole()
					view.PageFile = "database/role/modify.html"
					ctx.ViewData("connectionid", connectionid)
					ctx.ViewData("role", view.Data)
					ctx.ViewData("readonly", "readonly")
					ctx.ViewData("id", 1)
					view.Render(ctx)
				})
				role.Get("/add/{connectionid:uint}", func(ctx iris.Context) {
					var view APIFrontend
					var connectionid int
					connectionid, view.Error = ctx.Params().GetInt("connectionid")
					role := models.Role{}
					view.PageFile = "database/role/modify.html"
					ctx.ViewData("connectionid", connectionid)
					ctx.ViewData("role", role)
					ctx.ViewData("readonly", "")
					ctx.ViewData("id", 0)
					view.Render(ctx)
				})

			}

			user := database.Party("/user")
			{
				user.Get("/{id:uint}", func(ctx iris.Context) {
					var view APIFrontend
					var id int
					id, view.Error = ctx.Params().GetInt("id")
					conn := models.Connection{}
					conn.ID = id
					view.Data, view.Error = conn.GetUserDatabases()
					ctx.ViewData("userdb", view.Data)
					ctx.ViewData("connectionid", id)
					view.PageFile = "database/user/index.html"
					view.Render(ctx)
				})
				user.Get("/edit/{connectionid:uint}/{id:uint}", func(ctx iris.Context) {
					var view APIFrontend
					var pricipalid, connectionid int
					pricipalid, view.Error = ctx.Params().GetInt("id")
					connectionid, view.Error = ctx.Params().GetInt("connectionid")

					userdb := models.UserDB{}
					userdb.ID = pricipalid
					conn := models.Connection{}
					conn.ID = connectionid
					view.Data, view.Error = userdb.GetUserDatabase(conn)

					view.PageFile = "database/user/modify.html"
					ctx.ViewData("connectionid", connectionid)
					ctx.ViewData("pricipalid", pricipalid)
					ctx.ViewData("userdb", view.Data)
					view.Render(ctx)
				})
				user.Get("/add/{connectionid:uint}", func(ctx iris.Context) {
					var view APIFrontend
					var connectionid int
					connectionid, view.Error = ctx.Params().GetInt("connectionid")
					view.PageFile = "database/user/modify.html"
					ctx.ViewData("connectionid", connectionid)
					ctx.ViewData("pricipalid", 0)
					view.Render(ctx)
				})
			}

		}

		system := pages.Party("/system")
		{
			system.Get("", func(ctx iris.Context) {
				var view APIFrontend
				view.Data, view.Error = models.GetGeneralSettings()
				ctx.ViewData("gs", view.Data)
				view.PageFile = "system/index.html"
				view.Render(ctx)
			})

			system.Get("/edit/{id:uint}", func(ctx iris.Context) {
				var view APIFrontend
				var id int
				id, view.Error = ctx.Params().GetInt("id")
				gs := models.GeneralSetting{}
				gs.ID = id
				view.Data, view.Error = gs.GetGeneralSettingByID()
				fmt.Println("Data ", view.Data)
				view.PageFile = "system/modify.html"
				ctx.ViewData("gs", view.Data)
				ctx.ViewData("id", id)
				view.Render(ctx)
			})

			system.Get("/add", func(ctx iris.Context) {
				var view APIFrontend
				view.PageFile = "system/modify.html"
				ctx.ViewData("id", 0)
				view.Render(ctx)
			})
		}
	}
}
