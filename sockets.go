package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"sync"
	"sync/atomic"

	"consup/models"

	"github.com/kataras/iris"
	"github.com/kataras/iris/websocket"
	log "github.com/sirupsen/logrus"
	"github.com/tomasen/realip"
)

//UserInfo is struct of user
type UserInfo struct {
	Username    string
	HostName    string
	IPAddress   string
	IsMobile    bool
	IsDeploying bool
}

//SocketResult is struct for result of socket process
type SocketResult struct {
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
	Error   error       `json:"error"`
}

//ConnList is list of user logged
var ConnList = make(map[string]*UserInfo)

const (
	stateUnlocked uint32 = iota
	stateLocked
)

var (
	locker = stateUnlocked
)

//Render is method to render the socket result struct
func (res *SocketResult) Render() string {
	out, err := json.Marshal(res.Data)
	if err != nil {
		return string(out)
	}

	return string(out)
}

func setupWebsocket(app *iris.Application) {

	// create websocket with buffer
	ws := websocket.New(websocket.Config{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	})

	ws.OnConnection(handleConnection)

	// register the server on an endpoint.
	// see the inline javascript code in the websockets.html,
	// this endpoint is used to connect to the server.
	app.Get("/socket", ws.Handler())

	// serve the javascript built'n client-side library,
	app.Any("/iris-ws.js", func(ctx iris.Context) {
		ctx.Write(websocket.ClientSource)
	})
}

var mutex = new(sync.Mutex)
var cmdResult string = ""
var appRemoteSource = `\\02-05-0137-0317\deploy2`

func handleConnection(c websocket.Connection) {

	mutex.Lock()
	username := sess.Start(c.Context()).GetString("username")
	clientIP := realip.RealIP(c.Context().Request())
	hostName := c.Context().Request().Host

	ConnList[username] = &UserInfo{
		Username:    username,
		HostName:    hostName,
		IPAddress:   clientIP,
		IsMobile:    c.Context().IsMobile(),
		IsDeploying: false,
	}
	mutex.Unlock()

	c.To(websocket.All).Emit("visit", socketUpdateInfo())

	c.OnDisconnect(func() {
		mutex.Lock()
		delete(ConnList, sess.Start(c.Context()).GetString("username"))
		mutex.Unlock()

		c.To(websocket.All).Emit("visit", socketUpdateInfo())
	})

	c.On("deploy", func(msg string) {
		var result SocketResult
		log.WithFields(log.Fields{
			"WorkingFolder": WorkingFolder,
			"Message":       msg,
		}).Infoln("Deploy Request")

		go func() {
			if !atomic.CompareAndSwapUint32(&locker, stateUnlocked, stateLocked) {
				c.Emit("deploy", "There is already a batch of processes running")
				return
			}
			defer atomic.StoreUint32(&locker, stateUnlocked)

			result.Message = "InProgress"
			c.Emit("deploy", result.Render())

			//GET DEPLOY RELATED INFO FROM DATABASE
			batchId, err := strconv.Atoi(msg)
			if err != nil {
				log.WithFields(log.Fields{
					"WorkingFolder": WorkingFolder,
					"Message":       msg,
				}).Errorln("Deploy Request")
			}
			deploy := models.Deploy{}
			deploy.BatchID = batchId
			deploys, err := deploy.GetDeployInfo()
			if err != nil {
				result.Error = err
				result.Data = "[]"
				c.Emit("deploy", result.Render())
				log.WithFields(log.Fields{
					"WorkingFolder": WorkingFolder,
					"Message":       err,
				}).Errorln("Get Deploy Info error")
				return
			}

			batch := models.Batch{}
			batch.ID = batchId
			batch, err = batch.GetBatchByID()
			batch.Status = "PROGRESS"
			batch, err = batch.UpdateBatch()

			if err != nil {
				result.Error = err
				result.Data = "[]"
				c.Emit("deploy", result.Render())
				log.WithFields(log.Fields{
					"WorkingFolder": WorkingFolder,
					"Message":       err,
				}).Errorln("Update Status error")
				return
			}

			// time.Sleep(2 * time.Minute)

			//GENERATE DEPLOY COMMAND
			fmt.Println("===========================")
			fmt.Println(" INITIALIZE ENCRYPTION KEY")
			fmt.Println("===========================")
			query := `%windir%\Microsoft.NET\Framework\v2.0.50727\aspnet_regiis -pc AdInsKeys -exp`
			execCommand(query)
			query = `%windir%\Microsoft.NET\Framework\v2.0.50727\aspnet_regiis -pa AdInsKeys Everyone`
			execCommand(query)

			dirSource := ""
			dirDestination := ""
			// dirDestinationBase := ""
			dirFile := ""

			for _, res := range deploys {
				dirSource = fmt.Sprintf(`%s\deploy\%s\%s`, AppDeployPath, res.EnvironmentName, res.AppName)
				dirFile = dirSource
				os.MkdirAll(dirSource, os.ModePerm)
				dirSource = fmt.Sprintf(`%s\%s`, dirSource, res.FileName)
				writeFile(dirSource, res.Content)

				//ENCRYPT IN LOCAL PATH
				query = fmt.Sprintf(`%%windir%%\Microsoft.NET\Framework\v2.0.50727\aspnet_regiis -pef connectionStrings %s -prov CustomProvider`, dirFile)
				execCommand(query)
			}

			for _, res := range deploys {

				cmdResult = ""

				fmt.Println("===========================")
				fmt.Println(fmt.Sprintf(` DEPLOY %s, %s `, res.BatchName, res.AppName))
				fmt.Println("===========================")

				// // FILE SOURCE
				dirSource = fmt.Sprintf(`%s\deploy\%s\%s`, AppDeployPath, res.EnvironmentName, res.AppName)
				dirFile = dirSource
				dirSource = fmt.Sprintf(`%s\%s`, dirSource, res.FileName)

				// //DESTINATION PATH
				// dirDestinationBase = strings.Replace(strings.Replace(res.PathConfig, `:`, `$`, 1), `\\`, `\`, 1)
				dirDestination = fmt.Sprintf(`x:\%s`, res.PathConfig[3:len(res.PathConfig)])

				// ESTABLISH CONNECTION PATH TO SERVER
				// query = fmt.Sprintf(`NET USE \\%s\%s %s /user:%s /persistent:No`, res.ServerName, dirDestinationBase, res.AuthPassword, res.AuthUsername)
				// execCommand(query)
				query = fmt.Sprintf(`NET USE x: \\%s\c$ %s /user:%s /p:yes`, res.ServerName, res.AuthPassword, res.AuthUsername)

				//MOVE ENCRYPTED CONFIG TO DESTINATION PATH SERVER
				query += fmt.Sprintf(` && xcopy /s/Y  %s %s`, dirSource, dirDestination)

				//DELETE ESTABLISHED PATH TO SERVER
				query += ` && net use x: /delete`
				execCommand(query)

				fmt.Print(cmdResult)

				dirSource = ""
				dirDestination = ""
				// dirDestinationBase = ""

				//SAVE ALL LATEST SCRIPT TO DATABASE FOR ROLLBACK AND DEBUGING PURPOSE
			}

			fmt.Println("===========================")
			fmt.Println(" DEPLOY FINISHED ")
			fmt.Println("===========================")

			// time.Sleep(1 * time.Millisecond)
		}()
	})

	c.On("logout", func(msg string) {
		mutex.Lock()
		username := sess.Start(c.Context()).GetString("username")
		delete(ConnList, username)
		c.To(websocket.All).Emit("logout", username)
		mutex.Unlock()

		c.To(websocket.All).Emit("visit", socketUpdateInfo())
	})
}

func execCommand(query string) {
	cmdResult += "\n[CMD] " + query
	out, err := exec.Command(`cmd`, `/C`, query).Output()
	if err != nil {
		cmdResult += fmt.Sprintf("\n[RESULT] %s %s", out, err.Error())
	} else {
		cmdResult += fmt.Sprintf("\n[RESULT] %s", out)
	}
}

func writeFile(path string, content []byte) error {
	fmt.Println(fmt.Sprintf("\n[WRITEFILE] %s", path))
	f, err := os.Create(path)
	if err != nil {
		fmt.Println(err)
		return err
	}

	n2, err := f.Write(content)
	if err != nil {
		fmt.Println(err)
		f.Close()
		return err
	}
	fmt.Println(n2, "bytes written successfully\n")
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}

func socketUpdateInfo() string {

	result := []UserInfo{}
	for _, val := range ConnList {
		result = append(result, *val)
	}

	b, err := json.Marshal(result)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	return string(b)
}
