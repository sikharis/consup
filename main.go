package main

import (
	"consup/models"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"github.com/CrowdSurge/banner"
	"github.com/gorilla/securecookie"
	"github.com/spf13/cobra"

	// "github.com/iris-contrib/middleware/secure"

	"github.com/kataras/iris"
	"github.com/kataras/iris/middleware/logger"
	"github.com/kataras/iris/middleware/recover"
	"github.com/kataras/iris/sessions"
	"github.com/mattn/go-colorable"
	log "github.com/sirupsen/logrus"
	"github.com/snowzach/rotatefilehook"
)

var (
	cookieName = "net_secure_cookie"
	sess       *sessions.Sessions

	// WorkingFolder is contain working folder of application
	WorkingFolder = ""
	HostName      = ""
	AppMode       = ""
	AppPort       = ""
	AppDeployPath = ""
	DefaultLayout = "layouts/index.html"
	DefaultTitle  = "AdIns Net Secure"

	// CasbinController is Enforcer maps the model and the policy for the casbin service, we use this variable on the main_test too.
	CasbinController *Casbin
)

const (
	ConstDBPath      = "./database/netsecure.db?journal_mode=PERSIST"
	ConstUserTimeout = 3600
)

var rootCmd = &cobra.Command{
	Use:   `netsecure`,
	Short: `Netsecure is application to manage CONFINS system`,
	Long: `
Netsecure is application to manage CONFINS database account, web deployment, and security related issue
Report bug or information related to Infra DB Technician (InfraDB@ad-ins.com)
Find related infos of AdIns and CONFINS at http://www.ad-ins.com/`,
	Run: func(cmd *cobra.Command, args []string) {
		mode, err := cmd.Flags().GetString("mode")
		if err != nil {
			fmt.Println(err)
			fmt.Println("Please specify application mode!")
			os.Exit(1)
		}

		initLogger(mode)
		initIris()
	},
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func initLogger(mode string) {
	var logLevel = log.InfoLevel
	if AppMode == "debug" {
		logLevel = log.DebugLevel
	}

	rotateFileHook, err := rotatefilehook.NewRotateFileHook(rotatefilehook.RotateFileConfig{
		Filename:   "logs/console.log",
		MaxSize:    50, // megabytes
		MaxBackups: 3,
		MaxAge:     28, //days
		Level:      logLevel,
		Formatter: &log.JSONFormatter{
			TimestampFormat: time.RFC822,
		},
	})

	if err != nil {
		log.Fatalf("Failed to initialize file rotate hook: %v", err)
	}

	log.SetLevel(logLevel)
	log.SetOutput(colorable.NewColorableStdout())
	log.SetFormatter(&log.TextFormatter{
		ForceColors:     true,
		FullTimestamp:   true,
		TimestampFormat: time.RFC822,
	})
	log.AddHook(rotateFileHook)
}

func init() {
	//GET FULL WORKING FOLDER
	initializeWorkingFolder()

	//Flag Setting
	rootCmd.PersistentFlags().StringVar(&AppPort, "port", "3001", "Application port (default is 3001)")
	rootCmd.PersistentFlags().StringVar(&AppMode, "mode", "prod", "Running mode [debug|prod] (default is debug)")
	rootCmd.PersistentFlags().StringVar(&AppDeployPath, "deployPath", WorkingFolder, "Application working folder")
}

func initIris() {
	//Banner
	banner.Print("adins")
	fmt.Println()

	app := iris.New()
	app.Use(recover.New())

	var err error
	HostName, err = os.Hostname()
	if err != nil {
		log.WithFields(log.Fields{
			"Method":   "initIris",
			"HostName": HostName,
		}).Panicln(err.Error())
		panic(err)
	}

	liveUrl := fmt.Sprintf(":%s", AppPort)

	log.WithFields(log.Fields{
		"Method":   "initIris",
		"mode":     AppMode,
		"hostname": HostName,
		"liveUrl":  liveUrl,
	}).Infoln("Application Started")

	if AppMode == "debug" {
		app.Logger().SetLevel("debug")
		requestLogger := logger.New(logger.Config{
			// Status displays status code
			Status: true,
			// IP displays request's remote address
			IP: true,
			// Method displays the http method
			Method: true,
			// Path displays the request path
			Path: true,
			// Query appends the url query to the Path.
			Query: true,

			// if !empty then its contents derives from `ctx.Values().Get("logger_message")
			// will be added to the logs.
			MessageContextKeys: []string{"logger_message"},

			// if !empty then its contents derives from `ctx.GetHeader("User-Agent")
			//MessageHeaderKeys: []string{"User-Agent"},
		})
		app.Use(requestLogger)
	} else {
		log.SetFormatter(&log.JSONFormatter{})
	}

	// liveURL := GLOBAL_PROTOCOL + "://" + GLOBAL_URL + GLOBAL_PORT

	// protect := secure.New(secure.Options{
	// 	AllowedHosts: []string{"*"}, // AllowedHosts is a list of fully qualified domain names that are allowed. Default is empty list, which allows any and all host names.
	// 	//SSLRedirect:  true,              // If SSLRedirect is set to true, then only allow HTTPS requests. Default is false.
	// 	//SSLTemporaryRedirect:    false,                                                                                                                                               // If SSLTemporaryRedirect is true, the a 302 will be used while redirecting. Default is false (301).
	// 	//SSLHost:                 "ssl.example.com",                                                                                                                                   // SSLHost is the host name that is used to redirect HTTP requests to HTTPS. Default is "", which indicates to use the same host.
	// 	//SSLProxyHeaders:         map[string]string{"X-Forwarded-Proto": "https"},                                                                                                     // SSLProxyHeaders is set of header keys with associated values that would indicate a valid HTTPS request. Useful when using Nginx: `map[string]string{"X-Forwarded-Proto": "https"}`. Default is blank map.
	// 	//STSSeconds:              315360000,                                                                                                                                           // STSSeconds is the max-age of the Strict-Transport-Security header. Default is 0, which would NOT include the header.
	// 	//STSIncludeSubdomains:    true,                                                                                                                                                // If STSIncludeSubdomains is set to true, the `includeSubdomains` will be appended to the Strict-Transport-Security header. Default is false.
	// 	//STSPreload:              true,                                                                                                                                                // If STSPreload is set to true, the `preload` flag will be appended to the Strict-Transport-Security header. Default is false.
	// 	ForceSTSHeader:          false,                                                                                                                                               // STS header is only included when the connection is HTTPS. If you want to force it to always be added, set to true. `IsDevelopment` still overrides this. Default is false.
	// 	FrameDeny:               true,                                                                                                                                                // If FrameDeny is set to true, adds the X-Frame-Options header with the value of `DENY`. Default is false.
	// 	CustomFrameOptionsValue: "SAMEORIGIN",                                                                                                                                        // CustomFrameOptionsValue allows the X-Frame-Options header value to be set with a custom value. This overrides the FrameDeny option.
	// 	ContentTypeNosniff:      true,                                                                                                                                                // If ContentTypeNosniff is true, adds the X-Content-Type-Options header with the value `nosniff`. Default is false.
	// 	BrowserXSSFilter:        true,                                                                                                                                                // If BrowserXssFilter is true, adds the X-XSS-Protection header with the value `1; mode=block`. Default is false.
	// 	ContentSecurityPolicy:   "script-src 'self' " + liveURL + "/static/js/",                                                                                                      // ContentSecurityPolicy allows the Content-Security-Policy header value to be set with a custom value. Default is "".
	// 	PublicKey:               `pin-sha256="base64+primary=="; pin-sha256="base64+backup=="; max-age=5184000; includeSubdomains; report-uri="https://www.example.com/hpkp-report"`, // PublicKey implements HPKP to prevent MITM attacks with forged certificates. Default is "".

	// 	IsDevelopment: true, // This will cause the AllowedHosts, SSLRedirect, and STSSeconds/STSIncludeSubdomains options to be ignored during development. When deploying to production, be sure to set this to false.
	// })

	// app.Use(protect.Serve)

	models.InitDB(ConstDBPath, AppMode)
	casbin := Casbin{}
	CasbinController = casbin.InitCasbinAuth(ConstDBPath, "./config/casbinmodel.conf")
	app.Use(CasbinController.ServeHTTP)
	app.StaticServe("./assets", "./static")

	//app.StaticServe("./htmlx", "./x")
	//app.StaticEmbedded("/static", "./assets", Asset, AssetNames)
	//tmpl := iris.HTML("./html", ".html")
	//default layout (index.html)
	//tmpl.Layout(DefaultLayout)

	// $ go-bindata ./html/...
	//tmpl.Binary(Asset, AssetNames) // <-- IMPORTANT
	//app.RegisterView(tmpl)

	// app.StaticServe("./html", "./html")
	// AES only supports key sizes of 16, 24 or 32 bytes.
	// You either need to provide exactly that amount or you derive the key from what you type in.
	hashKey := []byte("the-big-and-secret-fash-key-here")
	blockKey := []byte("lot-secret-of-characters-big-too")
	secureCookie := securecookie.New(hashKey, blockKey)

	sess = sessions.New(sessions.Config{
		Cookie:       cookieName,
		Encode:       secureCookie.Encode,
		Decode:       secureCookie.Decode,
		AllowReclaim: true,
		Expires:      time.Minute * time.Duration(ConstUserTimeout),
	})

	//	// Per route middleware, you can add as many as you desire.
	//	app.Get("/benchmark", MyBenchLogger(), benchEndpoint)

	//TEMPLATE FUNCTION
	templateFunction(app)

	//BACK END (LOGIC / API)
	backEndAPI(app)

	//FRONT END (HTML, JS, CSS)
	frontEndAPI(app)

	//SOCKET HANDLER
	setupWebsocket(app)

	//open URL in browser
	OpenDefault("http://localhost:3001")

	app.Run(iris.Addr(liveUrl), iris.WithoutServerError(iris.ErrServerClosed), iris.WithoutPathCorrection)
}
func getUsername(ctx iris.Context) string {
	username := ""
	username = sess.Start(ctx).GetString("username")
	return username
}
func isAuth(ctx iris.Context) bool {
	// Check if user is authenticated

	username := ""
	if username = sess.Start(ctx).GetString("username"); username != "" {
		log.WithFields(log.Fields{
			"Method":   "isAuth",
			"isAuth":   true,
			"username": username,
		}).Infoln("Auth Success")
		return true
	}

	log.WithFields(log.Fields{
		"Method":   "isAuth",
		"isAuth":   true,
		"username": username,
	}).Infoln("Auth Failed")
	return false
}

//CheckAuth is method for check user authentication return in bool type
func CheckAuth(ctx iris.Context) {
	// ctx.Next()
	if !isAuth(ctx) {
		ctx.Redirect("/", iris.StatusTemporaryRedirect)
		return
	}

	ctx.Next()
}

func returnToBase(ctx iris.Context) {
	var res APIResult
	res.RedirectURL = "/"
	res.Render(ctx)
}

func initializeWorkingFolder() {
	ex, err := os.Executable()
	if err != nil {
		log.WithFields(log.Fields{
			"Method":        "initializeWorkingFolder",
			"WorkingFolder": WorkingFolder,
		}).Debugln(err.Error())
	}
	WorkingFolder = filepath.Dir(ex)
}
func removeStringAtIndex(object string, i int) string {
	out := []rune(object)
	out = append(out[:i], out[i+1:]...)
	return string(out)
}

func remove(s []string, r string) []string {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}
