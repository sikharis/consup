# Build And Development
.DEFAULT_GOAL := run

build:	
	@go build -o AdInsEncryptor.exe

setup:
	@cls	
	@go build -o AdInsEncryptor.exe
	@del database\netsecure.db
	@AdInsEncryptor.exe --mode=setup

run:
	@cls	
	@go build -o AdInsEncryptor.exe
	@AdInsEncryptor.exe --mode=debug

refresh:
	@cls	
	@go build -o AdInsEncryptor.exe
	@AdInsEncryptor.exe --mode=setup
	@AdInsEncryptor.exe --mode=debug


#go run main.go template.go sockets.go casbin.go back_end_api.go front_end_api.go --mode=debug	