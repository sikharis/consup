package main

import (
	"html/template"
	"strconv"

	"consup/models"

	"github.com/kataras/iris"
	log "github.com/sirupsen/logrus"
	// "encoding/json"
)

func templateFunction(app *iris.Application) {
	tmpl := iris.HTML("./html", ".html")

	tmpl.AddFunc("dataURI", func(item models.LinkList) template.HTMLAttr {
		index := 1
		rows := ""

		if item.URI.Name != "" {
			rows += `id="` + item.URI.Name + `" `
		}

		if item.URI.Page != "" {
			rows += `data-uri-` + strconv.Itoa(index) + `="` + item.URI.Page + `" `
			index++
		}

		if item.URI.Link != "" {
			rows += `data-uri-` + strconv.Itoa(index) + `="` + item.URI.Link + `" `
			index++
		}

		// if (item.Next.ID != 0 || item.Previous.ID != 0) {

		//     rows += `data-uri-` + strconv.Itoa(index) + `="api/v1/navigation/` + strconv.Itoa(item.URI.ID) + `" `;
		//     index++;
		// }

		return template.HTMLAttr(rows)
	})

	tmpl.AddFunc("Sum", func(nums ...int) int {
		total := 0
		for _, v := range nums {
			total += v
		}
		return total
	})

	tmpl.AddFunc("Environment", func(item models.LinkList) interface{} {

		cbo, err := models.GetEnvironments()

		if err != nil {
			panic(err)
		}

		return cbo
	})
	tmpl.AddFunc("CboAppServerEnvironments", func(types string, ID int) interface{} {
		st := models.AppServer{}
		st.ID = ID
		dropdown := models.Dropdown{}
		dropdown, err := st.CboAppServerEnvironments()

		if err != nil {
			panic(err)
		}

		json := ""
		selected := ""
		switch types {
		case "select":
			{
				json = `<option value="">Environments</option>`
				for _, val := range dropdown.Values {
					if val.Selected == true {
						selected = `selected`
					} else {
						selected = ``
					}
					json += `<option value="` + val.Value + `" ` + selected + `>` + val.Name + `</option>`
				}
			}
		case "label":
			{
				for _, val := range dropdown.Values {
					if val.Selected {
						json += `<a class="ui teal tag label">` + val.Name + `</a>`
					}
				}
			}
		}

		return template.HTML(json)
	})
	tmpl.AddFunc("CboAppServerApps", func(types string, AppID int, ServerID int, EnvironmentID int) interface{} {
		st := models.AppServer{}
		st.AppID = AppID
		st.ServerID = ServerID
		st.EnvironmentID = EnvironmentID
		dropdown := models.Dropdown{}
		dropdown, err := st.CboAppServerApps()
		if err != nil {
			panic(err)
		}

		json := ""
		selected := ""
		switch types {
		case "select":
			{
				json = `<option value="">Application</option>`
				for _, val := range dropdown.Values {
					if val.Selected == true {
						selected = `selected`
					} else {
						selected = ``
					}
					json += `<option value="` + val.Value + `" ` + selected + `>` + val.Name + `</option>`
				}
			}
		case "label":
			{
				for _, val := range dropdown.Values {
					if val.Selected {
						json += `<a class="ui teal tag label">` + val.Name + `</a>`
					}
				}
			}
		}

		return template.HTML(json)
	})

	tmpl.AddFunc("CboServers", func(ID int) interface{} {

		st := models.Server{}
		st.ID = ID
		dropdown := models.Dropdown{}
		dropdown, err := st.CboServers()

		if err != nil {
			panic(err)
		}

		json := ""
		selected := ""
		for _, val := range dropdown.Values {
			if val.Selected == true {
				selected = `selected`
			} else {
				selected = ``
			}
			json += `<option value="` + val.Value + `" ` + selected + `>` + val.Name + `</option>`
		}

		return template.HTML(json)
	})

	tmpl.AddFunc("COM_Environments", func(types string, id int) interface{} {

		st := models.Environment{}
		st.ID = id
		dropdown := models.Dropdown{}
		dropdown, err := st.CboBatchEnvironments()

		if err != nil {
			panic(err)
		}

		json := ""
		selected := ""
		switch types {
		case "select":
			{
				json = `<option value="">Environments</option>`
				for _, val := range dropdown.Values {
					if val.Selected == true {
						selected = `selected`
					} else {
						selected = ``
					}
					json += `<option value="` + val.Value + `" ` + selected + `>` + val.Name + `</option>`
				}
			}
		case "label":
			{
				for _, val := range dropdown.Values {
					if val.Selected {
						json += `<a class="ui teal small label">` + val.Name + `</a>`
					}
				}
			}
		}

		return template.HTML(json)
	})

	tmpl.AddFunc("COM_CBOApplication", func(types string, id int) interface{} {

		app := models.App{}
		app.ID = id
		dropdown := models.Dropdown{}
		dropdown, err := app.CboApplications()

		if err != nil {
			panic(err)
		}

		json := ""
		selected := ""
		switch types {
		case "select":
			{
				json = `<option value="">Applications</option>`
				for _, val := range dropdown.Values {
					if val.Selected == false {
						json += `<option value="` + val.Value + `" ` + selected + `>` + val.Name + `</option>`
					}
				}
			}
		case "multiple":
			{
				json = `<option value="">Applications</option>`
				for _, val := range dropdown.Values {
					if val.Selected == true {
						selected = `selected`
					} else {
						selected = ``
					}
					json += `<option value="` + val.Value + `" ` + selected + `>` + val.Name + `</option>`
				}
			}
		case "label":
			{
				for _, val := range dropdown.Values {
					if val.Selected {
						json += `<a class="ui teal small label">` + val.Name + `</a>`
					}
				}
			}
		}

		return template.HTML(json)
	})

	tmpl.AddFunc("cboUserDatabase", func(types string, id int) interface{} {

		conn := models.Connection{}
		conn.ID = id
		dropdown := models.Dropdown{}
		dropdown, err := conn.CboUserDatabase()

		if err != nil {
			panic(err)
		}

		json := ""
		selected := ""
		switch types {
		case "select":
			{
				json = `<option value="">User Database</option>`
				for _, val := range dropdown.Values {
					if val.Selected == true {
						selected = `selected`
					} else {
						selected = ``
					}
					json += `<option value="` + val.Value + `" ` + selected + `>` + val.Name + `</option>`
				}
			}
		case "label":
			{
				for _, val := range dropdown.Values {
					if val.Selected {
						json += `<a class="ui teal small label">` + val.Name + `</a>`
					}
				}
			}
		}

		return template.HTML(json)
	})

	tmpl.AddFunc("cboDatabase", func(types string, id int) interface{} {

		conn := models.Connection{}
		conn.ID = id
		dropdown := models.Dropdown{}
		dropdown, err := conn.CboDatabase()

		if err != nil {
			panic(err)
		}

		json := ""
		selected := ""
		switch types {
		case "select":
			{
				json = `<option value="">Database</option>`
				for _, val := range dropdown.Values {
					if val.Selected == true {
						selected = `selected`
					} else {
						selected = ``
					}
					json += `<option value="` + val.Value + `" ` + selected + `>` + val.Name + `</option>`
				}
			}
		case "label":
			{
				for _, val := range dropdown.Values {
					if val.Selected {
						json += `<a class="ui teal small label">` + val.Name + `</a>`
					}
				}
			}
		}

		return template.HTML(json)
	})
	tmpl.AddFunc("CboDatabaseName", func(types string, id int, name string) interface{} {

		conn := models.Connection{}
		conn.ID = id
		conn.DatabaseServer = name
		dropdown := models.Dropdown{}
		dropdown, err := conn.CboDatabaseName()

		if err != nil {
			panic(err)
		}

		json := ""
		selected := ""
		switch types {
		case "select":
			{
				json = `<option value="">Database</option>`
				for _, val := range dropdown.Values {
					if val.Selected == true {
						selected = `selected`
					} else {
						selected = ``
					}
					json += `<option value="` + val.Value + `" ` + selected + `>` + val.Name + `</option>`
				}
			}
		case "label":
			{
				for _, val := range dropdown.Values {
					if val.Selected {
						json += `<a class="ui teal small label">` + val.Name + `</a>`
					}
				}
			}
		}

		return template.HTML(json)
	})
	tmpl.AddFunc("CboGetUserDb", func(types string, id int, principalid int) interface{} {

		conn := models.Connection{}
		conn.ID = id
		userdb := models.UserDB{}
		userdb.ID = principalid
		dropdown := models.Dropdown{}
		log.WithFields(log.Fields{
			"userdb.ID": strconv.Itoa(userdb.ID),
		}).Infoln("CboGetUserDb")
		dropdown, err := userdb.CboGetUserDb(conn)

		if err != nil {
			panic(err)
		}

		json := ""
		selected := ""
		switch types {
		case "select":
			{
				json = `<option value="">Database</option>`
				for _, val := range dropdown.Values {
					if val.Selected == true {
						selected = `selected`
					} else {
						selected = ``
					}
					json += `<option value="` + val.Value + `" ` + selected + `>` + val.Name + `</option>`
				}
			}
		case "label":
			{
				for _, val := range dropdown.Values {
					if val.Selected {
						json += `<a class="ui teal small label">` + val.Name + `</a>`
					}
				}
			}
		}

		return template.HTML(json)
	})

	tmpl.AddFunc("CboPermissionType", func(types string, id int, databasename string, rolename string, name string) interface{} {

		role := models.Role{}
		role.ConnectionID = id
		role.DatabaseName = databasename
		role.Name = rolename

		dropdown := models.Dropdown{}
		dropdown, err := role.CboPermissionType()

		if err != nil {
			panic(err)
		}

		json := ""
		selected := ""
		switch types {
		case "select":
			{
				json = `<option value="">Database</option>`
				for _, val := range dropdown.Values {
					if val.Selected == true {
						selected = `selected`
					} else {
						selected = ``
					}
					json += `<option value="` + val.Value + `" ` + selected + `>` + val.Name + `</option>`
				}
			}
		case "label":
			{
				for _, val := range dropdown.Values {
					if val.Selected {
						json += `<a class="ui teal small label">` + val.Name + `</a>`
					}
				}
			}
		case "radio":
			{
				for _, val := range dropdown.Values {
					if val.Selected == true {
						selected = `checked`
					} else {
						selected = ``
					}
					json += `
					<div class="field">
						<div class="ui radio checkbox">
							<input type="radio"  name="` + name + `" ` + selected + ` > 
							<label>` + val.Value + `</label>
						</div>
					</div>
					`
				}
			}
		}

		return template.HTML(json)
	})

	tmpl.AddFunc("CboPermission", func(types string, id int, rolename string) interface{} {

		role := models.Role{}
		role.ConnectionID = id

		role.Name = rolename

		dropdown := models.Dropdown{}
		dropdown, err := role.CboPermission()

		if err != nil {
			panic(err)
		}

		json := ""
		selected := ""
		switch types {
		case "select":
			{
				json = `<option value="">Permission</option>`
				for _, val := range dropdown.Values {
					if val.Selected == true {
						selected = `selected`
					} else {
						selected = ``
					}
					json += `<option value="` + val.Value + `" ` + selected + `>` + val.Name + `</option>`
				}
			}
		case "label":
			{
				for _, val := range dropdown.Values {
					if val.Selected {
						json += `<a class="ui teal small label">` + val.Name + `</a>`
					}
				}
			}

		}

		return template.HTML(json)
	})

	tmpl.AddFunc("CboUserRoles", func(types string, username string) interface{} {

		user := models.User{}
		user.Username = username

		dropdown := models.Dropdown{}
		dropdown, err := user.CboUserRoles()

		if err != nil {
			panic(err)
		}

		json := ""
		selected := ""
		switch types {
		case "select":
			{
				json = `<option value="">Roles</option>`
				for _, val := range dropdown.Values {
					if val.Selected == true {
						selected = `selected`
					} else {
						selected = ``
					}
					json += `<option value="` + val.Value + `" ` + selected + `>` + val.Name + `</option>`
				}
			}
		case "label":
			{
				for _, val := range dropdown.Values {
					if val.Selected {
						json += `<a class="ui teal small label">` + val.Name + `</a>`
					}
				}
			}

		}

		return template.HTML(json)
	})

	app.RegisterView(tmpl)
}
