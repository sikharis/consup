package main

import (
	"consup/models"
	"fmt"
	"net/http"
	"strings"

	"github.com/casbin/casbin"
	gormadapter "github.com/casbin/gorm-adapter"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/kataras/iris"
	_ "github.com/mattn/go-sqlite3"
)

// Casbin is the auth services which contains the casbin enforcer.
type Casbin struct {
	enforcer *casbin.Enforcer
	model    string
	database string
	isdebug  bool
	Role     string       `json:"role"`
	Tables   models.Table `json:"table"`
}

func (c *Casbin) InitCasbinDB() {
	a := gormadapter.NewAdapter("sqlite3", c.database) // Your driver and data source.
	c.enforcer = casbin.NewEnforcer(c.model, a, c.isdebug)

}
func (c *Casbin) InitCasbinAuth(filePath string, model string) *Casbin {
	c.model = model
	if AppMode == "debug" {
		c.enforcer.EnableLog(true)
		c.isdebug = true
	} else {
		c.enforcer.EnableLog(false)
		c.isdebug = false
	}
	c.database = filePath
	c.InitCasbinDB()
	c.enforcer.LoadPolicy()
	//Anonymous Policy
	c.enforcer.AddPolicy("anonymous", "/static/*", "GET")
	c.enforcer.AddPolicy("anonymous", "/iris-ws.js", "GET")
	c.enforcer.AddPolicy("anonymous", "/", "GET")
	c.enforcer.AddPolicy("anonymous", "/api/v1/auth/login", "POST")
	c.enforcer.AddPolicy("anonymous", "/api/v1/auth/forceLogout", "GET")
	c.enforcer.AddPolicy("anonymous", "/dashboard", "GET")
	c.enforcer.AddPolicy("anonymous", "/socket", "GET")

	// //member data readonly
	// c.enforcer.AddPolicy("member_data_readonly", "/pages/user*", "GET")
	// c.enforcer.AddPolicy("member_data_readonly", "/pages/environment*", "GET")
	// c.enforcer.AddPolicy("member_data_readonly", "/pages/server*", "GET")
	// c.enforcer.AddPolicy("member_data_readonly", "/pages/apps*", "GET")

	// //member data read/write
	// //Page
	// c.enforcer.AddPolicy("member_data", "/pages/user*", "*")
	// c.enforcer.AddPolicy("member_data", "/pages/environment*", "*")
	// c.enforcer.AddPolicy("member_data", "/pages/server*", "*")
	// c.enforcer.AddPolicy("member_data", "/pages/apps*", "*")
	// //API
	// c.enforcer.AddPolicy("member_data", "/api/v1/user*", "*")
	// c.enforcer.AddPolicy("member_data", "/api/v1/Environment*", "*")
	// c.enforcer.AddPolicy("member_data", "/api/v1/server*", "*")
	// c.enforcer.AddPolicy("member_data", "/api/v1/appserver*", "*")
	// c.enforcer.AddPolicy("member_data", "/api/v1/apps*", "*")

	// //member deployment read
	// //Page
	// c.enforcer.AddPolicy("member_deployment_readonly", "/pages/deployment*", "GET")

	// //member deployment read/write
	// //Page
	// c.enforcer.AddPolicy("member_deployment", "/pages/deployment*", "*")
	// //API
	// c.enforcer.AddPolicy("member_deployment", "/api/v1/batch*", "*")
	// c.enforcer.AddPolicy("member_deployment", "/api/v1/stages*", "*")

	// //member database read
	// //Page
	// c.enforcer.AddPolicy("member_database_readonly", "/pages/database*", "GET")

	// //member database read/write
	// //Page
	// c.enforcer.AddPolicy("member_database", "/pages/database*", "*")

	// c.enforcer.AddPolicy("admin_readonly", "/*", "GET") //can access anything
	c.enforcer.AddPolicy("admin", "/*", "*") //can access anything

	c.enforcer.AddGroupingPolicy("Admin", "admin") //username, role

	c.enforcer.SavePolicy()
	return c
}
func (c *Casbin) GetGroupingPolicy() []string {
	var result []string
	dbl, err := gorm.Open("sqlite3", c.database)
	if err != nil {
		return nil
	}
	rows, err := dbl.Raw(`SELECT  v0 FROM casbin_rule where p_type = 'p' and v0 != 'anonymous' group by v0`).Rows()
	if err != nil {
		return nil
	}
	defer rows.Close()
	for rows.Next() {
		var v0 string
		rows.Scan(&v0)
		result = append(result, v0)
	}
	return result

}

func (c *Casbin) UpdateRoleName(role string) {
	dbl, err := gorm.Open("sqlite3", c.database)
	if err != nil {
		println("Error : ", err)
	}
	dbl.Exec("UPDATE casbin_rule SET v1=? WHERE v1 =?", c.Role, role)
}
func (c *Casbin) GetPolicyByUsername(username string) [][]string {
	roles, err := c.enforcer.GetRolesForUser(username)
	if err != nil {
		println("Error : ", err)
	}
	Policy := c.enforcer.GetFilteredPolicy(0, roles[0])
	return Policy
}

func (c *Casbin) GetRolesByUsername(username string) []string {
	roles, err := c.enforcer.GetRolesForUser(username)
	if err != nil {
		println("Error : ", err)
	}
	return roles
}

func (c *Casbin) DeleteRolesForUser(username string) {
	c.InitCasbinDB()
	c.enforcer.DeleteRolesForUser(username)
	c.enforcer.SavePolicy()
	c.enforcer.LoadPolicy()
}
func (c *Casbin) AddGroupingPolicy(username, role string) {
	c.InitCasbinDB()
	c.enforcer.AddGroupingPolicy(username, role)
	c.enforcer.SavePolicy()
	c.enforcer.LoadPolicy()
}

func (c *Casbin) UpdateGroupingPolicy(username, role string) {
	c.InitCasbinDB()
	c.enforcer.DeleteRolesForUser(username)
	c.enforcer.AddGroupingPolicy(username, role)
	c.enforcer.SavePolicy()
	c.enforcer.LoadPolicy()
}

func (c *Casbin) UpdateRole(role string) {
	fmt.Println("role", role)
	fmt.Println("casbin role", c.Role)
	c.enforcer.RemoveFilteredPolicy(0, role)
	c.enforcer.LoadPolicy()
	Tables := strings.Split(c.Tables.Name, ",")
	for i := range Tables {
		var expression = strings.Split(Tables[i], "#")
		switch expression[0] {
		case "ROOT":
			if expression[1] == "1" {
				//Jalanin Semua
				fmt.Println("ROOT", expression[1])
			}
		case "Form1":
			fmt.Println("ROOT", expression[1])
			c.enforcer.AddPolicy(c.Role, `/`+expression[2], expression[1])
		case "Form2":
			fmt.Println("ROOT", expression[1])
			c.enforcer.AddPolicy(c.Role, `/`+expression[2], expression[1])
		case "Form3":
			fmt.Println("ROOT", expression[1])
			c.enforcer.AddPolicy(c.Role, `/`+expression[2], expression[1])
		}
	}

	c.UpdateRoleName(role)
	c.enforcer.SavePolicy()
	c.enforcer.LoadPolicy()

}

//DeleteRole Method yang digunakan untuk mendelete system role pada casbin
//Input yang diperlukan adalah role yaitu system role pada casbin
//Output yang diperlukan adalah message string dan error
func (c *Casbin) DeleteRole(role string) (string, error) {
	dbl, err := gorm.Open("sqlite3", c.database)
	var isDelete bool = true
	if err != nil {
		return "Error", err
	}
	rows, err := dbl.Raw(`SELECT  count(v1) as count FROM casbin_rule where v1='` + role + `'  group by v1`).Rows()
	if err != nil {
		return "Error", err
	}
	defer rows.Close()
	for rows.Next() {
		var count uint
		rows.Scan(&count)
		if count > 0 {
			isDelete = false
		}
	}
	if isDelete {
		c.enforcer.DeleteRole(role)
		c.enforcer.SavePolicy()
		c.enforcer.LoadPolicy()
	} else {
		fmt.Println(`Can't delete ` + role + ` role because policies are attached`)
		return `Can't delete ` + role + ` role because policies are attached`, nil
	}
	return `Record has been successfully deleted`, nil

}

func (c *Casbin) AddRole() {
	Tables := strings.Split(c.Tables.Name, ",")
	for i := range Tables {
		var expression = strings.Split(Tables[i], "#")
		switch expression[0] {
		case "ROOT":
			if expression[1] == "1" {
				//Jalanin Semua
				fmt.Println("ROOT", expression[1])
			}
		case "Form1":
			fmt.Println("ROOT", expression[1])
			c.enforcer.AddPolicy(c.Role, `/`+expression[2], expression[1])
		case "Form2":
			fmt.Println("ROOT", expression[1])
			c.enforcer.AddPolicy(c.Role, `/`+expression[2], expression[1])
		case "Form3":
			fmt.Println("ROOT", expression[1])
			c.enforcer.AddPolicy(c.Role, `/`+expression[2], expression[1])
		}
	}
	c.enforcer.SavePolicy()
	c.enforcer.LoadPolicy()
}
func (c *Casbin) ServeHTTP(ctx iris.Context) {
	if !CasbinController.Check(ctx.Request(), ctx) {
		ctx.StatusCode(http.StatusForbidden) // Status Forbiden
		ctx.StopExecution()
		return
	}
	ctx.Next()
}

// Check checks the username, request's method and path and
// returns true if permission grandted otherwise false.
func (c *Casbin) Check(r *http.Request, ctx iris.Context) bool {
	username := getUsername(ctx)
	method := r.Method
	path := r.URL.Path
	c.enforcer.EnableLog(c.isdebug)
	return c.enforcer.Enforce(username, path, method)
}
