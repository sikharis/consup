package models

import (
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"time"
)

var cmdResult string = ""

const constbackupPath = "./backup/key/"

//GenerateKey object yang digunakan untuk menyimpa settingan system
type GenerateKey struct {
	ID              int    `gorm:"primary_key"  gorm:"AUTO_INCREMENT" json:"id"`
	KeyName         string `json:"keyname"`
	LastGenerateKey string `json:"date"`
}

func GetGenerateKeys() ([]GenerateKey, error) {
	var GenerateKeys []GenerateKey
	if err := dbl.Find(&GenerateKeys).Error; err != nil {
		fmt.Println("error,", err)
		return GenerateKeys, err
	}
	return GenerateKeys, nil
}

//DeployToAllServer untuk deploy ke semua server
func (gk *GenerateKey) DeployToAllServer() {
	servers, err := GetServers()
	if err != nil {
		fmt.Println("error,", err)
	}
	dbl.Where("id = ?", &gk.ID).First(&gk)
	for _, server := range servers {
		fmt.Println("server : ", server.Name)
		query := fmt.Sprintf(`NET USE x: \\%s\c$ %s /user:%s /p:yes`, server.Name, server.AuthPassword, server.AuthUsername)
		query += fmt.Sprintf(` && xcopy /s/Y  %s %s`, `D:\Project\Ad-Ins\GOLANG\src\infradb\confins-support\backup\key\AdInsKeys`+strconv.Itoa(gk.ID)+`.xml`, `x:\AdInsKeys`+strconv.Itoa(gk.ID)+`.xml*`)
		query += ` && net use x: /delete`
		if gk.ID > 1 {
			query += fmt.Sprintf(` && .\vendor\PsExec.exe \\%s -u %s -p %s C:\Windows\Microsoft.NET\Framework\v2.0.50727\aspnet_regiis -pz AdInsKeys`, server.Name, server.AuthUsername, server.AuthPassword)
		}
		query += fmt.Sprintf(` && .\vendor\PsExec.exe \\%s -u %s -p %s C:\Windows\Microsoft.NET\Framework\v2.0.50727\aspnet_regiis -pi AdInsKeys c:\AdInsKeys%s.xml`, server.Name, server.AuthUsername, server.AuthPassword, strconv.Itoa(gk.ID))
		query += fmt.Sprintf(` && .\vendor\PsExec.exe \\%s -u %s -p %s C:\Windows\Microsoft.NET\Framework\v2.0.50727\aspnet_regiis -pa AdInsKeys Everyone`, server.Name, server.AuthUsername, server.AuthPassword)
		execCommand(query)
		fmt.Print(cmdResult)
	}
}

//GetGenerateKeyByID Get General Setting By ID
func (gk *GenerateKey) GetGenerateKeyByID() (GenerateKey, error) {
	if err := dbl.First(&gk, "ID=?", gk.ID).Error; err != nil {
		return *gk, err
	}
	return *gk, nil
}

//NewGenerateKey Set new Key
func (gk *GenerateKey) NewGenerateKey() (GenerateKey, error) {
	initNewKey()
	fmt.Print(cmdResult)
	return *gk, nil
}

func initNewKey() {
	//GENERATE DEPLOY COMMAND
	cmdResult += "\n==========================="
	cmdResult += "\nINITIALIZE ENCRYPTION KEY"
	cmdResult += "\n==========================="
	query := `%windir%\Microsoft.NET\Framework\v2.0.50727\aspnet_regiis -pc AdInsKeys -exp`
	out, err := execCommand(query)
	if err != nil {
		if err.Error() == "exit status 1" {
			result := fmt.Sprintf("%s", out)
			fmt.Println(result)
			match, _ := regexp.MatchString("exists", result)
			if match {
				deleteKey()
				initNewKey()
			}
		}

	} else {
		cmdResult += "\n==========================="
		cmdResult += "\nADDING ACCESS ENCRYPTION KEY"
		cmdResult += "\n==========================="
		query := `%windir%\Microsoft.NET\Framework\v2.0.50727\aspnet_regiis -pa AdInsKeys Everyone`
		_, _ = execCommand(query)
		fmt.Println("Export")
		exportKey()
	}
}

func deleteKey() {
	cmdResult += "\n==========================="
	cmdResult += "\nDELETE OLD ENCRYPTION KEY"
	cmdResult += "\n==========================="
	query := `%windir%\Microsoft.NET\Framework\v2.0.50727\aspnet_regiis -pz AdInsKeys`
	_, _ = execCommand(query)

}

func exportKey() {
	var count int
	var gks []GenerateKey
	dbl.Find(&gks).Count(&count)
	count++

	var gk GenerateKey
	currentTime := time.Now()
	gk.KeyName = "AdInsKeys" + strconv.Itoa(count) + ".xml"
	gk.LastGenerateKey = currentTime.Format("2006-01-02 15:04:05")
	dbl.Create(&gk)

	os.MkdirAll(constbackupPath, os.ModePerm)
	filepath := constbackupPath + "AdInsKeys" + strconv.Itoa(count) + ".xml"
	cmdResult += "\n==========================="
	cmdResult += "\nEXPORT ENCRYPTION KEY"
	cmdResult += "\n==========================="
	query := fmt.Sprintf(`%%windir%%\Microsoft.NET\Framework\v2.0.50727\aspnet_regiis -px AdInsKeys %s -pri`, filepath)
	_, _ = execCommand(query)
}

func execCommand(query string) ([]byte, error) {
	cmdResult += "\n[CMD] " + query
	out, err := exec.Command(`cmd`, `/C`, query).Output()

	if err != nil {
		cmdResult += fmt.Sprintf("\n[RESULT] %s %s", out, err.Error())
	} else {
		cmdResult += fmt.Sprintf("\n[RESULT] %s", out)
	}
	return out, err
}
