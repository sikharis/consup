package models

//GeneralSetting object yang digunakan untuk menyimpa settingan system
type GeneralSetting struct {
	//column id digunkan sebagai primary key dari tabel general setting
	ID int `gorm:"primary_key" json:"id"`
	//column key digunkan untuk menyimpan nama value general setting
	Key string `json:"key"`
	//column key digunkan untuk menyimpan value general setting
	Value string `json:"value"`
}

//GetGeneralSettingByID Get General Setting By ID
func (gs *GeneralSetting) GetGeneralSettingByID() (GeneralSetting, error) {
	if err := dbl.First(&gs, "ID=?", gs.ID).Error; err != nil {
		return *gs, err
	}
	return *gs, nil
}

//DeleteGeneralSettingByID Delete General Setting By ID
func (gs *GeneralSetting) DeleteGeneralSettingByID() error {
	if err := dbl.Delete(&gs, gs.ID).Error; err != nil {
		return err
	}
	return nil
}

//UpdateGeneralSettingByID
//Parameter ID
func (gs *GeneralSetting) UpdateGeneralSettingByID() (GeneralSetting, error) {

	//Check if Exists
	var generalSetting GeneralSetting
	if err := dbl.First(&generalSetting, gs.ID).Error; err != nil {
		return generalSetting, err
	}
	//Update Data
	if gs.Key != "" {
		generalSetting.Key = gs.Key
	}
	if gs.Value != "" {
		generalSetting.Value = gs.Value
	}

	//Save
	if err := dbl.Save(&generalSetting).Error; err != nil {
		return generalSetting, err
	}
	return generalSetting, nil
}

//NewGeneralSetting
func (gs *GeneralSetting) NewGeneralSetting() (GeneralSetting, error) {
	dbl.Create(&gs)
	if err := dbl.First(&gs, gs.ID).Error; err != nil {
		return *gs, err
	}
	return *gs, nil
}

//GetGSValue GetGSValue Digunakan untuk mendapatkan GsValue, Parameter yang digunakan adalah key
func GetGSValue(key string) (GeneralSetting, error) {
	var gs GeneralSetting
	if err := dbl.Where(`Key = ?`, key).First(&gs).Error; err != nil {
		return gs, err
	}
	return gs, nil
}

//GetGeneralSettings Get General Settings
func GetGeneralSettings() ([]GeneralSetting, error) {
	var gs []GeneralSetting
	if err := dbl.Find(&gs).Error; err != nil {
		return gs, err
	}
	return gs, nil
}
