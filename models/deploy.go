package models

//Deploy is struct of deploy information
type Deploy struct {
	BatchID         int    `gorm:"column:batchid" json:"batchid"`
	BatchName       string `gorm:"column:batchname" json:"batchname"`
	AppName         string `gorm:"column:appname" json:"appname"`
	EnvironmentName string `gorm:"column:environmentname" json:"environmentname"`
	FileName        string `gorm:"column:filename" json:"filename"`
	ServerName      string `gorm:"column:servername" json:"servername"`
	IP              string `gorm:"column:ip" json:"ip"`
	PathConfig      string `gorm:"column:pathconfig" json:"pathconfig"`
	AuthPassword    string `gorm:"column:authpassword" json:"authpassword"`
	AuthUsername    string `gorm:"column:authusername" json:"authusername"`
	Content         []byte `gorm:"column:content" json:"content"`
	Status          string `gorm:"column:status" json:"status"`
}

func (deploy *Deploy) GetDeployInfo() ([]Deploy, error) {
	var deployInfo []Deploy
	rows, err := dbl.Raw(`SELECT 
		b.id as batchid,
		b.name as batchname,
		a.name as appname,    
		e.name as environmentname,    
		v.file_name as filename,
		s.name as servername,
		s.ip,
		aps.path_config as pathconfig,
		s.auth_password as authpassword,
		s.auth_username as authusername,
		f.content,
		b.status
	FROM batches b
	INNER JOIN versions v on v.batch_id = b.id 
	INNER JOIN batch_environment be on be.batch_id = b.id
	INNER JOIN app_servers aps on aps.environment_id = be.environment_id and aps.app_id = v.app_id
	LEFT JOIN apps a on a.id = v.app_id
	LEFT JOIN environments e on e.id = be.environment_id
	LEFT JOIN servers s on s.id = aps.server_id
	LEFT JOIN files f on f.id = v.file_id
	WHERE v.batch_id = ?`, deploy.BatchID).Rows()
	defer rows.Close()

	if err != nil {
		return deployInfo, err
	}

	for rows.Next() {
		var temp Deploy
		dbl.ScanRows(rows, &temp)
		deployInfo = append(deployInfo, temp)
	}

	return deployInfo, nil
}
