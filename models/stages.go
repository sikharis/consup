package models

//GetStage : get all history list
func (data *Batch) GetStagesByBatchID() ([]Version, error) {
	var version []Version
	dbl.Preload("App").Where("batch_id=?", data.ID).Find(&version)
	return version, nil
}

func (version *Version) DeleteStagesByID() error {
	tx := dbl.Begin()

	if err := dbl.Delete(&version).Error; err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return nil
}
