package models

//Batch : struct untuk history
type Batch struct {
	ID           int           `gorm:"primary_key" gorm:"AUTO_INCREMENT" json:"id"`
	Name         string        `json:"name"`
	Status       string        `json:"status"`
	Environments []Environment `gorm:"many2many:batch_environment" json:"environments"`
	Versions     []Version     `gorm:"foreignkey:BatchID;association_foreignkey:ID;" json:"versions"`
}

//GetBatch : get all history list
func (data *Batch) GetBatch() ([]Batch, error) {
	var batches []Batch
	if err := dbl.Find(&batches).Error; err != nil {
		return batches, err
	}
	return batches, nil
}

func (data *Batch) GetBatchByID() (Batch, error) {
	var batch Batch
	dbl.First(&batch, data.ID)
	return batch, nil
}

func (data *Batch) NewBatch() (Batch, error) {

	tx := dbl.Begin()
	var envs []Environment
	envs = data.Environments
	data.Environments = nil
	data.Status = "OP"
	if err := dbl.Create(&data).Error; err != nil {
		tx.Rollback()
		return *data, err
	}
	// data.Environments = envs
	dbl.Model(&data).Association("Environments").Append(envs)
	tx.Commit()

	return *data, nil
}

func (data *Batch) UpdateBatch() (Batch, error) {

	tx := dbl.Begin()
	data.Environments = nil
	dbl.Model(&data).Update("status", data.Status)
	tx.Commit()

	return *data, nil
}

func (data *Batch) UpdateStatusBatch() (Batch, error) {

	tx := dbl.Begin()
	data.Environments = nil
	if err := dbl.Save(&data).Error; err != nil {
		return *data, err
	}
	tx.Commit()

	return *data, nil
}

func (data *Batch) DeleteBatchByID() error {
	tx := dbl.Begin()
	if err := dbl.Delete(&data, data.ID).Error; err != nil {
		return err
	}
	tx.Commit()
	return nil
}
