package models

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
)

type Permission struct {
	Name string `gorm:"column:name"  json:"name"`
}
type Table struct {
	Name string `gorm:"column:name"  json:"name"`
}
type Role struct {
	ConnectionID int          `gorm:"column:connectionid" json:"connectionid"`
	DatabaseName string       `gorm:"column:databasename"  json:"databasename"`
	Name         string       `gorm:"column:name"  json:"name"`
	PerType      string       `gorm:"column:pertype"  json:"pertype"`
	Permission   []Permission `gorm:"column:permission"  json:"permission"`
	Tables       Table        `gorm:"column:table"  json:"table"`
}

type State struct {
	Selected bool `gorm:"column:selected"  json:"selected"`
}
type Trees struct {
	ID       string `gorm:"column:id"  json:"id"`
	Parent   string `gorm:"column:parent"  json:"parent"`
	Text     string `gorm:"column:text"  json:"text"`
	Children bool   `gorm:"column:children"  json:"children"`
	Selected bool   `gorm:"column:selected"  json:"selected"`
	State    State  `gorm:"column:state"  json:"state"`
}

type Securable struct {
	DBName     string `gorm:"column:dbname"  json:"dbname"`
	Permission string `gorm:"column:permission"  json:"permission"`
	Object     string `gorm:"column:object"  json:"object"`
	Role       string `gorm:"column:role"  json:"role"`
	Type       string `gorm:"column:type"  json:"type"`
}

func (o *Connection) GetRoles() ([]Role, error) {
	var role []Role
	dbl.First(&o)
	if err := o.InitDBmssql(); err != nil {
		return role, err
	}

	if err := o.dbmssql.Raw(`
		CREATE TABLE #SECURABLE (
			dbname   SYSNAME NULL,
			permission   SYSNAME null,
			object SYSNAME null,
			role SYSNAME NULL,
			type sysname NULL
		)
				
		INSERT INTO #SECURABLE
		EXEC sp_MSForEachdb
		'
		DECLARE @_msparam AS VARCHAR(200)
		

		SELECT
		''?'' DBName,
		permission_name permission,
		''[''+sch.name+''].[''+obj.name+'']'' AS [object],
		grantee_principal.name COLLATE Latin1_General_CI_AS_KS_WS role,
		prmssn.class_desc type
		FROM
		[?].sys.database_permissions AS prmssn
		INNER JOIN [?].sys.all_objects AS obj ON obj.object_id = prmssn.major_id and prmssn.class = 1
		INNER JOIN [?].sys.schemas AS sch ON sch.schema_id = obj.schema_id
		INNER JOIN [?].sys.database_principals AS grantee_principal ON grantee_principal.principal_id = prmssn.grantee_principal_id
		UNION
		SELECT
		''?'' DBName,
		permission_name permission,
		obj.name AS [object],
		grantee_principal.name COLLATE Latin1_General_CI_AS_KS_WS role,
		prmssn.class_desc type
		FROM
		[?].sys.database_permissions AS prmssn
		INNER JOIN [?].sys.schemas AS obj ON obj.schema_id = prmssn.major_id and prmssn.class = 3
		INNER JOIN [?].sys.database_principals AS grantee_principal ON grantee_principal.principal_id = prmssn.grantee_principal_id
		UNION
		SELECT
		''?'' DBName,
		permission_name permission,
		''?'' AS [object],
		grantee_principal.name COLLATE Latin1_General_CI_AS_KS_WS role,
		prmssn.class_desc type
		FROM
		[?].sys.database_permissions AS prmssn
		INNER JOIN [?].sys.database_principals AS grantee_principal ON grantee_principal.principal_id = prmssn.grantee_principal_id
		WHERE prmssn.class_desc = ''DATABASE''
		'


		SELECT  DISTINCT role name,'` + strconv.Itoa(o.ID) + `' connectionid FROM #SECURABLE
		where dbname not in ('master','model','msdb','tempdb') AND permission IN ('SELECT','UPDATE','DELETE','INSERT')
		ORDER BY role
		DROP TABLE #SECURABLE	
	`).Scan(&role).Error; err != nil {
		return role, err
	}
	defer o.dbmssql.Close()
	return role, nil
}
func (o *Role) GetRole() (Role, error) {
	var conn Connection
	conn.ID = o.ConnectionID
	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		return *o, err
	}
	//Get Type
	if err := conn.dbmssql.Raw(`
		USE [` + o.DatabaseName + `]
		SELECT DISTINCT usr.name AS name,
		CASE WHEN perm.state <> 'W' THEN perm.state_desc ELSE 'GRANT' END AS pertype
		FROM sys.database_permissions AS perm
		INNER JOIN
		sys.objects AS obj
		ON perm.major_id = obj.[object_id]
		INNER JOIN
		sys.database_principals AS usr
		ON perm.grantee_principal_id = usr.principal_id
		LEFT JOIN
		sys.columns AS cl
		ON cl.column_id = perm.minor_id AND cl.[object_id] = perm.major_id
		WHERE obj.Type <> 'S' AND obj.type = 'U' and usr.name = '` + o.Name + `'	
	`).Scan(&o).Error; err != nil {
		return *o, err
	}

	defer conn.dbmssql.Close()
	return *o, nil

}
func (o *Role) UpdateRole() (Role, error) {
	var script = ""
	Tables := strings.Split(o.Tables.Name, ",")
	temp, err := o.RevokePermission()
	if err != nil {
		fmt.Println("error", err)
		return *o, err
	}
	script += temp

	for i := range o.Permission {
		for j := range Tables {

			var expression = strings.Split(Tables[j], "#")
			switch expression[0] {
			case "DB":
				script += `
					USE [` + expression[1] + `];
					iF (DATABASE_PRINCIPAL_ID('` + o.Name + `') IS NULL)
					BEGIN
						CREATE ROLE [` + o.Name + `]
					END
					GRANT ` + o.Permission[i].Name + ` TO [` + o.Name + `]
				`
			case "SC":
				script += `
				USE [` + expression[1] + `];
				iF (DATABASE_PRINCIPAL_ID('` + o.Name + `') IS NULL)
				BEGIN
					CREATE ROLE [` + o.Name + `]
				END
				GRANT ` + o.Permission[i].Name + ` ON SCHEMA::[` + expression[2] + `] TO [` + o.Name + `]
				`
			case "TA":
				script += `
				USE [` + expression[1] + `]
				iF (DATABASE_PRINCIPAL_ID('` + o.Name + `') IS NULL)
				BEGIN
					CREATE ROLE [` + o.Name + `]
				END
				GRANT ` + o.Permission[i].Name + ` ON [` + expression[1] + `].[` + expression[2] + `].[` + expression[3] + `] TO [` + o.Name + `]
				`
			}

		}
	}
	var conn Connection
	conn.ID = o.ConnectionID
	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		fmt.Println("error", err)
		return *o, err
	}

	if err := conn.dbmssql.Exec(script).Error; err != nil {
		return *o, err
	}

	return *o, nil
}

func (o *Role) DeleteRole() (Role, error) {
	var script = ""
	temp, err := o.RevokePermission()
	if err != nil {
		fmt.Println("error", err)
		return *o, err
	}
	script += temp

	var conn Connection
	conn.ID = o.ConnectionID
	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		fmt.Println("error", err)
		return *o, err
	}

	if err := conn.dbmssql.Exec(script).Error; err != nil {
		return *o, err
	}

	return *o, nil
}
func (o *Role) RevokePermission() (string, error) {
	var conn Connection
	conn.ID = o.ConnectionID
	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		return "", err
	}
	var securables []Securable

	err := conn.dbmssql.Raw(`
	CREATE TABLE #SECURABLE (
		dbname   SYSNAME NULL,
		permission   SYSNAME null,
		object SYSNAME null,
		role SYSNAME NULL,
		type sysname NULL
	)
			
	INSERT INTO #SECURABLE
	EXEC sp_MSForEachdb
	'
	DECLARE @_msparam AS VARCHAR(200)
	SET @_msparam = ''` + o.Name + `''

	SELECT
	''?'' DBName,
	permission_name permission,
	''[''+sch.name+''].[''+obj.name+'']'' AS [object],
	grantee_principal.name COLLATE Latin1_General_CI_AS_KS_WS role,
	prmssn.class_desc type
	FROM
	[?].sys.database_permissions AS prmssn
	INNER JOIN [?].sys.all_objects AS obj ON obj.object_id = prmssn.major_id and prmssn.class = 1
	INNER JOIN [?].sys.schemas AS sch ON sch.schema_id = obj.schema_id
	INNER JOIN [?].sys.database_principals AS grantee_principal ON grantee_principal.principal_id = prmssn.grantee_principal_id
	WHERE
	(grantee_principal.name=@_msparam)
	UNION
	SELECT
	''?'' DBName,
	permission_name permission,
	obj.name AS [object],
	grantee_principal.name COLLATE Latin1_General_CI_AS_KS_WS role,
	prmssn.class_desc type
	FROM
	[?].sys.database_permissions AS prmssn
	INNER JOIN [?].sys.schemas AS obj ON obj.schema_id = prmssn.major_id and prmssn.class = 3
	INNER JOIN [?].sys.database_principals AS grantee_principal ON grantee_principal.principal_id = prmssn.grantee_principal_id
	WHERE
	(grantee_principal.name=@_msparam)
	UNION
	SELECT
	''?'' DBName,
	permission_name permission,
	''?'' AS [object],
	grantee_principal.name COLLATE Latin1_General_CI_AS_KS_WS role,
	prmssn.class_desc type
	FROM
	[?].sys.database_permissions AS prmssn
	INNER JOIN [?].sys.database_principals AS grantee_principal ON grantee_principal.principal_id = prmssn.grantee_principal_id
	WHERE (grantee_principal.name=@_msparam) AND prmssn.class_desc = ''DATABASE''
	'


	SELECT  * FROM #SECURABLE
	where dbname not in ('master','model','msdb','tempdb') AND permission IN ('SELECT','UPDATE','DELETE','INSERT')
	DROP TABLE #SECURABLE
	`).Scan(&securables).Error
	fmt.Println("securables ", securables)
	if err != nil {
		return "", err
	}
	var result = ""
	for i := range securables {
		result += `
		use [` + securables[i].DBName + `]`

		if securables[i].Type == "OBJECT_OR_COLUMN" {
			result += ` REVOKE ` + securables[i].Permission + ` ON ` + securables[i].Object + ` TO ` + securables[i].Role
		} else if securables[i].Type == "SCHEMA" {
			result += ` REVOKE ` + securables[i].Permission + ` ON SCHEMA::[` + securables[i].Object + `] TO ` + securables[i].Role
		} else if securables[i].Type == "DATABASE" {
			result += ` REVOKE ` + securables[i].Permission + ` TO ` + securables[i].Role
		}

	}

	defer conn.dbmssql.Close()
	return result, nil
}
func (o *Role) GetDBSchemaTrees() ([]Trees, error) {

	var conn Connection
	var trees []Trees
	conn.ID = o.ConnectionID
	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		return trees, err
	}

	if err := conn.dbmssql.Raw(`
	CREATE TABLE #DBSCHEMATABLE (
		DBNAME   SYSNAME,
		[SCHEMA_ID]   SYSNAME,
		[SCHEMA_NAME]  SYSNAME,
		TABLE_ID  SYSNAME,
		TABLE_NAME  SYSNAME
		
	)
	CREATE TABLE #Result(
		id sysname,
		parent sysname,
		text sysname,
		selected sysname
	)
	CREATE TABLE #SECURABLE (
		dbname   SYSNAME,
		permission   SYSNAME null,
		object SYSNAME null,
		role SYSNAME NULL,
		type sysname NULL
	)
			
	INSERT INTO #SECURABLE
	EXEC sp_MSForEachdb
	'
	DECLARE @_msparam AS VARCHAR(200)
	SET @_msparam = ''` + o.Name + `''

	SELECT
	''?'' DBName,
	''?'' AS [object],
	permission_name permission,
	grantee_principal.name COLLATE Latin1_General_CI_AS_KS_WS role,
	prmssn.class_desc type
	FROM
	[?].sys.database_permissions AS prmssn
	INNER JOIN [?].sys.database_principals AS grantee_principal ON grantee_principal.principal_id = prmssn.grantee_principal_id
	WHERE (grantee_principal.name=@_msparam) AND prmssn.class_desc = ''DATABASE''
	'


	INSERT INTO	 #DBSCHEMATABLE
	EXEC sp_MSForEachdb
	'
	SELECT 
	''?'' DBNAME,
	s.schema_id SCHEMA_ID, 
	s.name SCHEMA_NAME,
	t.object_id TABLE_ID , 
	t.name TABLE_NAME
	FROM [?].sys.tables AS t
	INNER JOIN [?].sys.schemas AS s
	ON t.[schema_id] = s.[schema_id]
	'
	
	--DBNAME
	INSERT INTO #Result
	SELECT DISTINCT 'DB#'+DBNAME id, 
	'#' parent, 
	DBNAME text ,
	CASE WHEN B.id IS NOT NULL THEN 1 ELSE 0 END 
	FROM #DBSCHEMATABLE
	LEFT JOIN (
		SELECT DISTINCT  dbname id FROM #SECURABLE
	) B ON B.id = #DBSCHEMATABLE.DBNAME
	WHERE DBNAME NOT IN  ('master','tempdb','msdb','model')
	
	
	SELECT id,parent,
	text,
	selected,'1' as children 
	FROM #Result
	order by text
	
	DROP TABLE #DBSCHEMATABLE
	DROP TABLE #Result
	DROP TABLE #SECURABLE
	
	`).Scan(&trees).Error; err != nil {
		return trees, err
	}
	for i := range trees {
		trees[i].State.Selected = trees[i].Selected
	}
	defer conn.dbmssql.Close()
	return trees, nil
}
func (o *Role) GetDBTableTrees() ([]Trees, error) {

	var conn Connection
	var trees []Trees
	conn.ID = o.ConnectionID
	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		return trees, err
	}

	if err := conn.dbmssql.Raw(`
	USE ` + o.DatabaseName + `
	DECLARE @DBNAME VARCHAR(2000), @ROLENAME VARCHAR(2000)
	SET @DBNAME = '` + o.DatabaseName + `'
	SET @ROLENAME = '` + o.Name + `'
	CREATE TABLE #DBSCHEMATABLE (
		DBNAME   SYSNAME,
		[SCHEMA_ID]   SYSNAME,
		[SCHEMA_NAME]  SYSNAME,
		TABLE_ID  SYSNAME,
		TABLE_NAME  SYSNAME
	)
	CREATE TABLE #Result(
		id sysname,
		parent sysname,
		text sysname
	)
	INSERT INTO	 #DBSCHEMATABLE
	EXEC sp_MSForEachdb
	'
	SELECT 
	''?'' DBNAME,
	s.schema_id SCHEMA_ID, 
	s.name SCHEMA_NAME,
	t.object_id TABLE_ID , 
	t.name TABLE_NAME
	FROM [?].sys.tables AS t
	INNER JOIN [?].sys.schemas AS s
	ON t.[schema_id] = s.[schema_id]
	'
	
	
	--SCHEMA
	INSERT INTO #Result
	SELECT 'SC#'+DBNAME+'#'+SCHEMA_NAME id,'DB#'+DBNAME parent,SCHEMA_NAME text FROM  #DBSCHEMATABLE
	WHERE DBNAME NOT IN  ('master','tempdb','msdb','model')  AND DBNAME = @DBNAME
	GROUP BY DBNAME,SCHEMA_NAME,SCHEMA_ID
	
	--Tables
	INSERT INTO #Result
	SELECT 'TA#'+DBNAME+'#'+SCHEMA_NAME+'#'+TABLE_NAME id, 'SC#'+DBNAME+'#'+SCHEMA_NAME parent, TABLE_NAME text FROM  #DBSCHEMATABLE
	WHERE DBNAME NOT IN  ('master','tempdb','msdb','model')  AND DBNAME = @DBNAME
	GROUP BY DBNAME,SCHEMA_ID ,SCHEMA_NAME ,TABLE_ID, TABLE_NAME
	
	SELECT 
	id,
	A.parent,
	A.text,
	selected = CASE WHEN B.text IS NOT NULL THEN 1 ELSE 0 END 
	FROM #Result A
	LEFT JOIN (
		SELECT DISTINCT
			
				CASE database_permissions.class_desc
					WHEN 'SCHEMA' THEN schema_name(major_id)
					WHEN 'OBJECT_OR_COLUMN' THEN
						CASE WHEN minor_id = 0 THEN object_name(major_id) COLLATE Latin1_General_CI_AS_KS_WS
						ELSE (SELECT object_name(object_id) + ' ('+ name + ')'
							  FROM sys.columns 
							  WHERE object_id = database_permissions.major_id 
							  AND column_id = database_permissions.minor_id) end
					ELSE 'other' 
				END text
			,database_principals.name COLLATE Latin1_General_CI_AS_KS_WS Role
			FROM sys.database_permissions
			JOIN sys.database_principals
			ON database_permissions.grantee_principal_id = database_principals.principal_id
			LEFT JOIN sys.objects -- consider schemas
			ON objects.object_id = database_permissions.major_id
			WHERE database_permissions.major_id > 0
			AND permission_name in ('SELECT','INSERT','UPDATE','DELETE')
			AND database_principals.name COLLATE Latin1_General_CI_AS_KS_WS  = @ROLENAME

	) B ON  A.text = B.text
	order by A.text

	DROP TABLE #DBSCHEMATABLE
	DROP TABLE #Result
	
	`).Scan(&trees).Error; err != nil {
		return trees, err
	}
	for i := range trees {
		trees[i].State.Selected = trees[i].Selected

	}

	defer conn.dbmssql.Close()
	return trees, nil
}

func (o *Role) GetFormTrees() ([]Trees, error) {
	var trees []Trees
	s := `[ 
		{ "id" : "ROOT#1", "parent" : "#", "text" : "Form" },
		{ "id" : "Form1#GET#pages/user", "parent" : "ROOT#1", "text" : "Users" },
		{ "id" : "Form1#GET#pages/environment", "parent" : "ROOT#1", "text" : "Environments" },
		{ "id" : "Form1#GET#pages/server", "parent" : "ROOT#1", "text" : "Servers" },
		{ "id" : "Form1#GET#pages/apps", "parent" : "ROOT#1", "text" : "Application" },
		{ "id" : "Form1#GET#pages/deployment/batch", "parent" : "ROOT#1", "text" : "Batch" },
		{ "id" : "Form1#GET#pages/database/connection", "parent" : "ROOT#1", "text" : "Database" },
		{ "id" : "Form2#GET#pages/user/add#PUT#api/v1/users", "parent" : "Form1#GET#pages/user", "text" : "New User" },
		{ "id" : "Form2#GET#pages/user/edit/*#POST#api/v1/users/*", "parent" : "Form1#GET#pages/user", "text" : "Edit User" },
		{ "id" : "Form2#DELETE#api/v1/users/Admin", "parent" : "Form1#GET#pages/user", "text" : "Delete User" },
		{ "id" : "Form2#GET#pages/systemroles", "parent" : "Form1#GET#pages/user", "text" : "System Roles" },
		{ "id" : "Form2#GET#api/v1/role/GetFormTrees*", "parent" : "Form1#GET#pages/user", "text" : "Get System Roles" },
		{ "id" : "Form2#GET#pages/environment/add#PUT#api/v1/Environments", "parent" : "Form1#GET#pages/environment", "text" : "New Environment" },
		{ "id" : "Form2#GET#pages/environment/edit/*#POST#api/v1/Environments/*", "parent" : "Form1#GET#pages/environment", "text" : "Edit Environment" },
		{ "id" : "Form2#DELETE#api/v1/Environments/*", "parent" : "Form1#GET#pages/environment", "text" : "Delete Environment" },
		{ "id" : "Form2#GET#pages/server/add#PUT#api/v1/servers", "parent" : "Form1#GET#pages/server", "text" : "New Server" },
		{ "id" : "Form2#GET#pages/server/edit/*#POST#api/v1/servers/*", "parent" : "Form1#GET#pages/server", "text" : "Edit Server" },
		{ "id" : "Form2#DELETE#api/v1/servers/*", "parent" : "Form1#GET#pages/server", "text" : "Delete Server" },
		{ "id" : "Form2#GET#pages/server/appserver/*", "parent" : "Form1#GET#pages/server", "text" : "APP Server" },
		{ "id" : "Form2#GET#pages/apps/add#PUT#api/v1/apps", "parent" : "Form1#GET#pages/apps", "text" : "New Application" },
		{ "id" : "Form2#GET#pages/apps/edit/*#POST#api/v1/apps/*", "parent" : "Form1#GET#pages/apps", "text" : "Edit Application" },
		{ "id" : "Form2#DELETE#api/v1/apps/*", "parent" : "Form1#GET#pages/apps", "text" : "Delete Application" },
		{ "id" : "Form2#GET#pages/deployment/batch/add#PUT#api/v1/batch", "parent" : "Form1#GET#pages/deployment/batch", "text" : "New Batch" },
		{ "id" : "Form2#GET#pages/deployment/batch/edit/*#POST#/api/v1/batch/*", "parent" : "Form1#GET#pages/deployment/batch", "text" : "Edit Batch" },
		{ "id" : "Form2#GET#pages/deployment/stages/*", "parent" : "Form1#GET#pages/deployment/batch", "text" : "Stages" },
		{ "id" : "Form2#GET#pages/database/connection/add#PUT#api/v1/connection", "parent" : "Form1#GET#pages/database/connection", "text" : "New Database Connection" },
		{ "id" : "Form2#GET#pages/database/connection/edit/*#POST#api/v1/connection/*", "parent" : "Form1#GET#pages/database/connection", "text" : "Edit Database Connection" },
		{ "id" : "Form2#DELETE#api/v1/connection/*", "parent" : "Form1#GET#pages/database/connection", "text" : "Delete Database Connection" },
		{ "id" : "Form2#GET#pages/database/user/*", "parent" : "Form1#GET#pages/database/connection", "text" : "User" },
		{ "id" : "Form2#GET#pages/database/role/*", "parent" : "Form1#GET#pages/database/connection", "text" : "Roles" },
		{ "id" : "Form2#GET#pages/database/authorization/*", "parent" : "Form1#GET#pages/database/connection", "text" : "Authorization" },
		{ "id" : "Form3#GET#pages/systemroles/add", "parent" : "Form2#GET#pages/systemroles", "text" : "New System Roles" },
		{ "id" : "Form3#GET#pages/systemroles/edit/*", "parent" : "Form2#GET#pages/systemroles", "text" : "View System Roles" },
		{ "id" : "Form3#GET#api/v1/systemroles/*", "parent" : "Form2#GET#pages/systemroles", "text" : "Delete System Roles" },
		{ "id" : "Form3#GET#pages/server/appserver/add/*#PUT#api/v1/appservers", "parent" : "Form2#GET#pages/server/appserver/*", "text" : "New Application Server" },
		{ "id" : "Form3#GET#pages/server/appserver/edit/*#POST#api/v1/appservers/*", "parent" : "Form2#GET#pages/server/appserver/*", "text" : "Edit Application Server" },
		{ "id" : "Form3#DELETE#api/v1/appservers/*", "parent" : "Form2#GET#pages/server/appserver/*", "text" : "Delete Application Server" },
		{ "id" : "Form3#GET#pages/deployment/stages/add/*#PUT#api/v1/stages", "parent" : "Form2#GET#pages/deployment/stages/*", "text" : "New Stage File" },
		{ "id" : "Form3#DELETE#api/v1/stages/*", "parent" : "Form2#GET#pages/deployment/stages/*", "text" : "Delete Stage File" },
		{ "id" : "Form2#GET#pages/database/user/add/*#PUT#api/v1/userdb", "parent" : "Form2#GET#pages/database/user/*", "text" : "New User Database" },
		{ "id" : "Form2#GET#pages/database/user/edit/*#POST#api/v1/userdb/*", "parent" : "Form2#GET#pages/database/user/*", "text" : "Edit User Database" },
		{ "id" : "Form2#DELETE#api/v1/userdb/*", "parent" : "Form2#GET#pages/database/user/*", "text" : "Delete User Database" },
		{ "id" : "Form2#GET#pages/database/user/add/*#PUT#api/v1/userdb", "parent" : "Form2#GET#pages/database/user/*", "text" : "New User Database" },
		{ "id" : "Form2#GET#pages/database/user/edit/*#POST#api/v1/userdb/*", "parent" : "Form2#GET#pages/database/user/*", "text" : "Edit User Database" },
		{ "id" : "Form2#DELETE#api/v1/userdb/*", "parent" : "Form2#GET#pages/database/user/*", "text" : "Delete User Database" }
	]`
	err := json.Unmarshal([]byte(s), &trees)
	if err != nil {
		fmt.Println("error", err)
		return nil, err
	}
	return trees, nil
}

func (o *Role) GetFormTreesByRole(role string) ([]Trees, error) {
	var trees []Trees
	s := `[ 
		{ "id" : "ROOT#1", "parent" : "#", "text" : "Form" },
		{ "id" : "Form1#GET#pages/user", "parent" : "ROOT#1", "text" : "Users" },
		{ "id" : "Form1#GET#pages/environment", "parent" : "ROOT#1", "text" : "Environments" },
		{ "id" : "Form1#GET#pages/server", "parent" : "ROOT#1", "text" : "Servers" },
		{ "id" : "Form1#GET#pages/apps", "parent" : "ROOT#1", "text" : "Application" },
		{ "id" : "Form1#GET#pages/deployment/batch", "parent" : "ROOT#1", "text" : "Batch" },
		{ "id" : "Form1#GET#pages/database/connection", "parent" : "ROOT#1", "text" : "Database" },
		{ "id" : "Form2#GET#pages/user/add#PUT#api/v1/users", "parent" : "Form1#GET#pages/user", "text" : "New User" },
		{ "id" : "Form2#GET#pages/user/edit/*#POST#api/v1/users/*", "parent" : "Form1#GET#pages/user", "text" : "Edit User" },
		{ "id" : "Form2#DELETE#api/v1/users/Admin", "parent" : "Form1#GET#pages/user", "text" : "Delete User" },
		{ "id" : "Form2#GET#pages/systemroles", "parent" : "Form1#GET#pages/user", "text" : "System Roles" },
		{ "id" : "Form2#GET#api/v1/role/GetFormTrees*", "parent" : "Form1#GET#pages/user", "text" : "Get System Roles" },
		{ "id" : "Form2#GET#pages/environment/add#PUT#api/v1/Environments", "parent" : "Form1#GET#pages/environment", "text" : "New Environment" },
		{ "id" : "Form2#GET#pages/environment/edit/*#POST#api/v1/Environments/*", "parent" : "Form1#GET#pages/environment", "text" : "Edit Environment" },
		{ "id" : "Form2#DELETE#api/v1/Environments/*", "parent" : "Form1#GET#pages/environment", "text" : "Delete Environment" },
		{ "id" : "Form2#GET#pages/server/add#PUT#api/v1/servers", "parent" : "Form1#GET#pages/server", "text" : "New Server" },
		{ "id" : "Form2#GET#pages/server/edit/*#POST#api/v1/servers/*", "parent" : "Form1#GET#pages/server", "text" : "Edit Server" },
		{ "id" : "Form2#DELETE#api/v1/servers/*", "parent" : "Form1#GET#pages/server", "text" : "Delete Server" },
		{ "id" : "Form2#GET#pages/server/appserver/*", "parent" : "Form1#GET#pages/server", "text" : "APP Server" },
		{ "id" : "Form2#GET#pages/apps/add#PUT#api/v1/apps", "parent" : "Form1#GET#pages/apps", "text" : "New Application" },
		{ "id" : "Form2#GET#pages/apps/edit/*#POST#api/v1/apps/*", "parent" : "Form1#GET#pages/apps", "text" : "Edit Application" },
		{ "id" : "Form2#DELETE#api/v1/apps/*", "parent" : "Form1#GET#pages/apps", "text" : "Delete Application" },
		{ "id" : "Form2#GET#pages/deployment/batch/add#PUT#api/v1/batch", "parent" : "Form1#GET#pages/deployment/batch", "text" : "New Batch" },
		{ "id" : "Form2#GET#pages/deployment/batch/edit/*#POST#/api/v1/batch/*", "parent" : "Form1#GET#pages/deployment/batch", "text" : "Edit Batch" },
		{ "id" : "Form2#GET#pages/deployment/stages/*", "parent" : "Form1#GET#pages/deployment/batch", "text" : "Stages" },
		{ "id" : "Form2#GET#pages/database/connection/add#PUT#api/v1/connection", "parent" : "Form1#GET#pages/database/connection", "text" : "New Database Connection" },
		{ "id" : "Form2#GET#pages/database/connection/edit/*#POST#api/v1/connection/*", "parent" : "Form1#GET#pages/database/connection", "text" : "Edit Database Connection" },
		{ "id" : "Form2#DELETE#api/v1/connection/*", "parent" : "Form1#GET#pages/database/connection", "text" : "Delete Database Connection" },
		{ "id" : "Form2#GET#pages/database/user/*", "parent" : "Form1#GET#pages/database/connection", "text" : "User" },
		{ "id" : "Form2#GET#pages/database/role/*", "parent" : "Form1#GET#pages/database/connection", "text" : "Roles" },
		{ "id" : "Form2#GET#pages/database/authorization/*", "parent" : "Form1#GET#pages/database/connection", "text" : "Authorization" },
		{ "id" : "Form3#GET#pages/systemroles/add", "parent" : "Form2#GET#pages/systemroles", "text" : "New System Roles" },
		{ "id" : "Form3#GET#pages/systemroles/edit/*", "parent" : "Form2#GET#pages/systemroles", "text" : "View System Roles" },
		{ "id" : "Form3#GET#api/v1/systemroles/*", "parent" : "Form2#GET#pages/systemroles", "text" : "Delete System Roles" },
		{ "id" : "Form3#GET#pages/server/appserver/add/*#PUT#api/v1/appservers", "parent" : "Form2#GET#pages/server/appserver/*", "text" : "New Application Server" },
		{ "id" : "Form3#GET#pages/server/appserver/edit/*#POST#api/v1/appservers/*", "parent" : "Form2#GET#pages/server/appserver/*", "text" : "Edit Application Server" },
		{ "id" : "Form3#DELETE#api/v1/appservers/*", "parent" : "Form2#GET#pages/server/appserver/*", "text" : "Delete Application Server" },
		{ "id" : "Form3#GET#pages/deployment/stages/add/*#PUT#api/v1/stages", "parent" : "Form2#GET#pages/deployment/stages/*", "text" : "New Stage File" },
		{ "id" : "Form3#DELETE#api/v1/stages/*", "parent" : "Form2#GET#pages/deployment/stages/*", "text" : "Delete Stage File" },
		{ "id" : "Form2#GET#pages/database/user/add/*#PUT#api/v1/userdb", "parent" : "Form2#GET#pages/database/user/*", "text" : "New User Database" },
		{ "id" : "Form2#GET#pages/database/user/edit/*#POST#api/v1/userdb/*", "parent" : "Form2#GET#pages/database/user/*", "text" : "Edit User Database" },
		{ "id" : "Form2#DELETE#api/v1/userdb/*", "parent" : "Form2#GET#pages/database/user/*", "text" : "Delete User Database" },
		{ "id" : "Form2#GET#pages/database/user/add/*#PUT#api/v1/userdb", "parent" : "Form2#GET#pages/database/user/*", "text" : "New User Database" },
		{ "id" : "Form2#GET#pages/database/user/edit/*#POST#api/v1/userdb/*", "parent" : "Form2#GET#pages/database/user/*", "text" : "Edit User Database" },
		{ "id" : "Form2#DELETE#api/v1/userdb/*", "parent" : "Form2#GET#pages/database/user/*", "text" : "Delete User Database" }
	]`
	err := json.Unmarshal([]byte(s), &trees)
	if err != nil {
		fmt.Println("error", err)
		return nil, err
	}
	rows, err := dbl.Raw(`SELECT  v1 FROM casbin_rule where v0='` + role + `' and p_type = 'p'`).Rows()
	if err != nil {
		return nil, nil
	}
	defer rows.Close()
	for rows.Next() {
		var v1 string
		rows.Scan(&v1)
		for i := range trees {
			var expression = strings.Split(trees[i].ID, "#")
			switch expression[0] {
			case "ROOT":
				if expression[1] == "1" {
					fmt.Println("ROOT", expression[1])
				}
			default:
				if "/"+expression[2] == v1 {
					trees[i].State.Selected = true
					fmt.Println("v1 ", v1)
				}
			}
		}
	}

	return trees, nil
}

func (o *Role) CboPermissionType() (Dropdown, error) {

	dropdown := Dropdown{}
	items := []DropdownItem{}
	var conn Connection
	conn.ID = o.ConnectionID
	dbl.First(&conn)

	if err := conn.InitDBmssql(); err != nil {
		return dropdown, err
	}
	var script = `USE [` + o.DatabaseName + `]
	SELECT 
	A.PerType value,
	A.PerType name,
	CASE WHEN B.PerType IS NULL THEN 0 ELSE 1 END selected
	FROM 
	(
		SELECT 'GRANT' PerType
		UNION
		SELECT 'DENY' PerType
	) A 
	LEFT JOIN (
	SELECT DISTINCT
	usr.name AS RoleName,
	CASE WHEN perm.state <> 'W' THEN perm.state_desc ELSE 'GRANT' END AS PerType
	FROM sys.database_permissions AS perm
	INNER JOIN
	sys.objects AS obj
	ON perm.major_id = obj.[object_id]
	INNER JOIN
	sys.database_principals AS usr
	ON perm.grantee_principal_id = usr.principal_id
	LEFT JOIN
	sys.columns AS cl
	ON cl.column_id = perm.minor_id AND cl.[object_id] = perm.major_id
	WHERE obj.Type <> 'S' AND obj.type = 'U' AND usr.name = '` + o.Name + `'
	)
	B ON B.PerType = A.PerType 
	order by A.PerType desc
	`

	if err := conn.dbmssql.Raw(script).Find(&items).Error; err != nil {
		return dropdown, err
	}

	defer conn.dbmssql.Close()
	dropdown.Values = items
	return dropdown, nil

}

func (o *Role) CboPermission() (Dropdown, error) {

	dropdown := Dropdown{}
	items := []DropdownItem{}
	var conn Connection
	conn.ID = o.ConnectionID
	dbl.First(&conn)

	if err := conn.InitDBmssql(); err != nil {
		return dropdown, err
	}
	var script = `
	CREATE TABLE #SECURABLE (
		dbname   SYSNAME NULL,
		permission   SYSNAME null,
		object SYSNAME null,
		role SYSNAME NULL,
		type sysname NULL
	)
			
	INSERT INTO #SECURABLE
	EXEC sp_MSForEachdb
	'
	DECLARE @_msparam AS VARCHAR(200)
	

	SELECT
	''?'' DBName,
	permission_name permission,
	''[''+sch.name+''].[''+obj.name+'']'' AS [object],
	grantee_principal.name COLLATE Latin1_General_CI_AS_KS_WS role,
	prmssn.class_desc type
	FROM
	[?].sys.database_permissions AS prmssn
	INNER JOIN [?].sys.all_objects AS obj ON obj.object_id = prmssn.major_id and prmssn.class = 1
	INNER JOIN [?].sys.schemas AS sch ON sch.schema_id = obj.schema_id
	INNER JOIN [?].sys.database_principals AS grantee_principal ON grantee_principal.principal_id = prmssn.grantee_principal_id
	UNION
	SELECT
	''?'' DBName,
	permission_name permission,
	obj.name AS [object],
	grantee_principal.name COLLATE Latin1_General_CI_AS_KS_WS role,
	prmssn.class_desc type
	FROM
	[?].sys.database_permissions AS prmssn
	INNER JOIN [?].sys.schemas AS obj ON obj.schema_id = prmssn.major_id and prmssn.class = 3
	INNER JOIN [?].sys.database_principals AS grantee_principal ON grantee_principal.principal_id = prmssn.grantee_principal_id
	UNION
	SELECT
	''?'' DBName,
	permission_name permission,
	''?'' AS [object],
	grantee_principal.name COLLATE Latin1_General_CI_AS_KS_WS role,
	prmssn.class_desc type
	FROM
	[?].sys.database_permissions AS prmssn
	INNER JOIN [?].sys.database_principals AS grantee_principal ON grantee_principal.principal_id = prmssn.grantee_principal_id
	WHERE prmssn.class_desc = ''DATABASE''
	'	


		SELECT A.PermissionName name,
		A.PermissionName value,
		CASE	WHEN B.permission IS NULL THEN 0 ELSE 1 END selected
		FROM 
		(
		SELECT 'SELECT' PermissionName
		UNION
		SELECT 'INSERT' PermissionName
		UNION
		SELECT 'UPDATE' PermissionName
		UNION
		SELECT 'DELETE' PermissionName
		) A LEFT JOIN (
			SELECT DISTINCT permission FROM #SECURABLE
			WHERE dbname not in ('master','model','msdb','tempdb') AND permission IN ('SELECT','UPDATE','DELETE','INSERT') AND role = '` + o.Name + `'
		) B ON A.PermissionName = B.permission
		ORDER BY A.PermissionName
		DROP TABLE #SECURABLE
	
	`

	if err := conn.dbmssql.Raw(script).Find(&items).Error; err != nil {
		return dropdown, err
	}

	defer conn.dbmssql.Close()
	dropdown.Values = items
	return dropdown, nil

}
