package models

type Environment struct {
	ID      int     `gorm:"primary_key" json:"id"`
	Name    string  `json:"name"`
	Batches []Batch `json:"Batches"`
	// Batches []Batch `gorm:"many2many:batch_Environment" json:"Batches"`
}

func GetEnvironments() ([]Environment, error) {
	var environment []Environment
	if err := dbl.Find(&environment).Error; err != nil {
		return environment, err
	}
	return environment, nil
}

func (environment *Environment) GetEnvironmentByID() (Environment, error) {
	if err := dbl.First(&environment, environment.ID).Error; err != nil {
		return *environment, err
	}
	return *environment, nil
}

func (env *Environment) NewEnvironment() (Environment, error) {

	dbl.Create(&env)
	if err := dbl.First(&env, env.ID).Error; err != nil {
		return *env, err
	}
	return *env, nil
}

func (env *Environment) UpdateEnvironment() (Environment, error) {
	if err := dbl.Save(&env).Error; err != nil {
		return *env, err
	}
	return *env, nil
}

func (env *Environment) DeleteEnvironmentByID() error {
	if err := dbl.Delete(&env, env.ID).Error; err != nil {
		return err
	}
	return nil
}

func (env *Environment) CboBatchEnvironments() (Dropdown, error) {

	dropdown := Dropdown{}
	items := []DropdownItem{}
	dbl.Table("environments en").Select("en.ID as value, en.Name as name, CASE WHEN be.batch_id IS NULL THEN 0 ELSE 1 END as selected").
		Joins("LEFT join batch_environment be ON be.environment_id = en.id AND be.batch_id = ?", env.ID).
		Find(&items)
	dropdown.Values = items

	return dropdown, nil
}
