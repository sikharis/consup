package models

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mssql"
)

type Connection struct {
	ID             int             `gorm:"primary_key"  gorm:"AUTO_INCREMENT"  json:"id"`
	Name           string          `json:"name"`
	DatabaseServer string          `json:"databaseserver"`
	UserName       string          `json:"username"`
	Password       string          `json:"password"`
	Port           string          `json:"port"`
	Authorizations []Authorization `gorm:"foreignkey:ID;association_foreignkey:ConnectionID" json:"authorizations"`
	UserDB         []UserDB        `json:"userdb"`
	dbmssql        *gorm.DB        `json:"-"`
}

func (o *Connection) InitDBmssql() error {

	if o.dbmssql != nil {
		return nil
	}

	var err error
	fmt.Println("mssql", "sqlserver://"+o.UserName+":"+o.Password+"@"+o.DatabaseServer+":"+o.Port+"?database=master")
	o.dbmssql, err = gorm.Open("mssql", "sqlserver://"+o.UserName+":"+o.Password+"@"+o.DatabaseServer+":"+o.Port+"?database=master")
	// defer o.dbmssql.Close()

	if err != nil {
		return err
	}
	return nil
}

func (o *Connection) CboUserDatabase() (Dropdown, error) {

	dropdown := Dropdown{}
	items := []DropdownItem{}

	dbl.First(&o)
	if err := o.InitDBmssql(); err != nil {
		return dropdown, err
	}
	if err := o.dbmssql.Raw("SELECT name as name,name as value, 0 selected FROM sys.server_principals WHERE type_desc IN ('WINDOWS_LOGIN','SQL_LOGIN')").Find(&items).Error; err != nil {
		return dropdown, err
	}
	defer o.dbmssql.Close()
	dropdown.Values = items
	return dropdown, nil

}
func (o *Connection) CboDatabase() (Dropdown, error) {

	dropdown := Dropdown{}
	items := []DropdownItem{}

	dbl.First(&o)
	if err := o.InitDBmssql(); err != nil {
		return dropdown, err
	}
	if err := o.dbmssql.Raw("Select database_id value, name ,0 selected From sys.databases  Where database_id > 5 ORDER BY name ASC").Find(&items).Error; err != nil {
		return dropdown, err
	}

	defer o.dbmssql.Close()
	dropdown.Values = items
	return dropdown, nil

}

func (o *Connection) CboDatabaseName() (Dropdown, error) {

	dropdown := Dropdown{}
	items := []DropdownItem{}
	var script = `Select 
			a.name value, 
			a.name ,
			CASE WHEN b.name IS NULL THEN 0 ELSE 1 END selected 
			FROM sys.databases  a
			LEFT JOIN (
			SELECT 
			name 
			FROM sys.databases  WHERE name = '` + o.DatabaseServer + `'
			) b ON b.name = a.name
			WHERE database_id > 5 ORDER BY name ASC`
	dbl.First(&o)
	if err := o.InitDBmssql(); err != nil {
		return dropdown, err
	}
	if err := o.dbmssql.Raw(script).Find(&items).Error; err != nil {
		return dropdown, err
	}

	defer o.dbmssql.Close()
	dropdown.Values = items
	return dropdown, nil
}

func GetConnections() ([]Connection, error) {
	var conections []Connection
	dbl.Find(&conections)
	return conections, nil
}

func (o *Connection) GetConnectionByID() (Connection, error) {
	var con Connection
	dbl.Find(&con, o.ID)
	return con, nil
}

func (o *Connection) NewConnection() (Connection, error) {
	tx := dbl.Begin()
	dbl.Create(&o)
	tx.Commit()
	return *o, nil
}

func (o *Connection) UpdateConnection() (Connection, error) {
	tx := dbl.Begin()
	dbl.Save(&o)
	tx.Commit()
	return *o, nil
}

func (o *Connection) DeleteConnectionByID() error {
	dbl.Delete(&o)
	return nil
}
