package models

type Link struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Title string `json:"title"`
	Page  string `json:"page"`
	Link  string `json:"link"`
	Icon  string `json:"icon"`
	Desc  string `json:"desc"`
}

type LinkList struct {
	URI      Link `json:"uri"`
	Previous Link `json:"prev"`
	Next     Link `json:"next"`
}

func GetNavigationData(isTree bool) ([]LinkList, error) {

	//MAIN MENU
	APIUser := Link{
		ID:    1,
		Name:  "user-list",
		Title: "Users",
		Page:  "pages/user",
		Link:  "",
		Icon:  "user",
		Desc:  "Privelege, Grant, Block etc",
	}

	APIServerType := Link{
		ID:    2,
		Name:  "servertype-list",
		Title: "Environment",
		Page:  "pages/environment",
		Link:  "",
		Icon:  "sitemap",
		Desc:  "Manage environment server",
	}

	APIServer := Link{
		ID:    3,
		Name:  "server-list",
		Title: "Servers",
		Page:  "pages/server",
		Link:  "",
		Icon:  "server",
		Desc:  "Manage Servers and Apps inside",
	}

	APIApplication := Link{
		ID:    4,
		Name:  "application-list",
		Title: "Applications",
		Page:  "pages/apps",
		Link:  "",
		Icon:  "cogs",
		Desc:  "Manage Application Services",
	}

	APIDeployment := Link{
		ID:    5,
		Name:  "deployment-list",
		Title: "Batch",
		Icon:  "clone",
		Desc:  "Deployment batching",
	}

	//SUB MENU
	APIDeployBatch := Link{
		ID:    7,
		Name:  "batch",
		Title: "Batch",
		Page:  "pages/deployment/batch",
		Link:  "",
		Icon:  "clone",
		Desc:  "Deployment batching",
	}

	APIDeployStages := Link{
		ID:    8,
		Title: "Staged",
		Page:  "pages/deployment/stages",
		Link:  "",
	}

	APIDeployApps := Link{
		ID:    9,
		Title: "Application List",
		Page:  "pages/deployment/apps",
		Link:  "",
	}

	APIDeployVersion := Link{
		ID:    10,
		Title: "Config Version",
		Page:  "pages/deployment/version",
		Link:  "",
	}

	APIDatabaseConnection := Link{
		ID:    11,
		Name:  "DatabaseConnection-list",
		Title: "Database",
		Page:  "pages/database/connection",
		Link:  "",
		Icon:  "database",
		Desc:  "Manage Databases",
	}

	APISystem := Link{
		ID:    11,
		Name:  "System-list",
		Title: "System",
		Page:  "pages/system",
		Link:  "",
		Icon:  "cog",
		Desc:  "Manage System Settings",
	}

	nav := []LinkList{}

	if isTree {
		nav = []LinkList{
			LinkList{
				URI: APIUser,
			},
			LinkList{
				URI: APIServerType,
			},
			LinkList{
				URI: APIServer,
			},
			LinkList{
				URI: APIApplication,
			},
			LinkList{
				URI: APIDeployment,
			},
			LinkList{
				URI:  APIDeployBatch,
				Next: APIDeployStages,
			},
			LinkList{
				URI:      APIDeployStages,
				Next:     APIDeployApps,
				Previous: APIDeployBatch,
			},
			LinkList{
				URI:      APIDeployApps,
				Next:     APIDeployVersion,
				Previous: APIDeployStages,
			},
			LinkList{
				URI:      APIDeployVersion,
				Previous: APIDeployApps,
			},
			LinkList{
				URI: APIDatabaseConnection,
			},
			LinkList{
				URI: APISystem,
			},
		}
	} else {
		nav = []LinkList{
			LinkList{
				URI: APIUser,
			},
			LinkList{
				URI: APIServerType,
			},
			LinkList{
				URI: APIServer,
			},
			LinkList{
				URI: APIApplication,
			},
			LinkList{
				URI:  APIDeployBatch,
				Next: APIDeployStages,
			},
			LinkList{
				URI: APIDatabaseConnection,
			},
			LinkList{
				URI: APISystem,
			},
		}
	}

	return nav, nil
}

func GetNavigations() ([]LinkList, error) {
	linkLists, _ := GetNavigationData(false)

	return linkLists, nil
}

func GetNavigationByID(index int) (LinkList, error) {
	nav := LinkList{}
	linkLists, err := GetNavigationData(true)

	if err != nil {
		return nav, err
	}

	for _, item := range linkLists {
		if item.URI.ID == index {
			nav = item
			return item, nil
		}
	}

	return nav, nil
}
