package models

import (
	"strconv"
)

type UserDB struct {
	ID           int        `gorm:"column:id" json:"id"`
	Name         string     `gorm:"column:name" json:"name"`
	UserType     string     `gorm:"column:usertype" json:"usertype"`
	Password     string     `gorm:"column:password" json:"password"`
	Active       bool       `gorm:"column:active" json:"active"`
	ConnectionID int        `gorm:"column:connectionid" json:"connectionid"`
	Databases    []Database `json:"databases"`
}

type Database struct {
	Name string `gorm:"column:name" json:"name"`
}

type UserDBPermission struct {
	Name      string `gorm:"column:name" json:"name"`
	GroupName string `gorm:"column:groupname" json:"groupname"`
}

func (o *Connection) GetUserDatabases() ([]UserDB, error) {

	var userdb []UserDB
	dbl.First(&o)
	if err := o.InitDBmssql(); err != nil {
		return userdb, err
	}

	if err := o.dbmssql.Raw(`SELECT principal_id as id, name,CASE WHEN type = 'S' THEN 'SQL Login' ELSE 'Active Directory' END AS 'usertype',is_disabled as active, '` + strconv.Itoa(o.ID) + `' as connectionid  FROM sys.server_principals WHERE type_desc IN ('WINDOWS_LOGIN','SQL_LOGIN') order by name`).Scan(&userdb).Error; err != nil {
		return userdb, err
	}
	defer o.dbmssql.Close()
	return userdb, nil
}
func (userdb *UserDB) GetUserDatabase(conn Connection) (UserDB, error) {
	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		return *userdb, err
	}
	if err := conn.dbmssql.Raw(`SELECT principal_id as id, name,CASE WHEN type = 'S' THEN 'SQL Login' ELSE 'Active Directory' END AS 'usertype',is_disabled as active, '` + strconv.Itoa(conn.ID) + `' as connectionid  FROM sys.server_principals WHERE type_desc IN ('WINDOWS_LOGIN','SQL_LOGIN') and principal_id = ` + strconv.Itoa(userdb.ID)).Scan(&userdb).Error; err != nil {
		return *userdb, err
	}
	defer conn.dbmssql.Close()
	return *userdb, nil
}

func (userdb *UserDB) DeleteUserDatabase(conn Connection) error {
	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		return err
	}
	var script = `DROP LOGIN [` + userdb.Name + `]`
	if err := conn.dbmssql.Exec(script).Error; err != nil {
		return err
	}
	defer conn.dbmssql.Close()
	return nil
}

func (userdb *UserDB) NewUserDatabase(conn Connection) (UserDB, error) {
	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		return *userdb, err
	}
	var script = `
	CREATE LOGIN [` + userdb.Name + `] WITH PASSWORD=N'` + userdb.Password + `', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
	`
	for i := range userdb.Databases {
		script += ` use ` + userdb.Databases[i].Name + `
		IF NOT EXISTS(SELECT * FROM sys.database_principals  WHERE type = 'S' AND name = '` + userdb.Name + `')
		CREATE USER [` + userdb.Name + `] FOR LOGIN [` + userdb.Name + `]
		`
	}
	if err := conn.dbmssql.Exec(script).Error; err != nil {
		return *userdb, err
	}
	defer conn.dbmssql.Close()
	return *userdb, nil
}

func (userdb *UserDB) UpdateUserDatabase(conn Connection) (UserDB, error) {
	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		return *userdb, err
	}
	var script = ``

	for i := range userdb.Databases {
		script += ` use ` + userdb.Databases[i].Name + `
		IF NOT EXISTS(SELECT * FROM sys.database_principals  WHERE type = 'S' AND name = '` + userdb.Name + `')
		CREATE USER [` + userdb.Name + `] FOR LOGIN [` + userdb.Name + `]
		`
	}

	temp, err := conn.DelUserDBPermission(userdb)
	if err != nil {
		return *userdb, err
	}

	script += temp

	if len(userdb.Password) > 0 {
		script += ` use master
		ALTER LOGIN [` + userdb.Name + `] WITH PASSWORD=N'` + userdb.Password + `'
		`
	}
	if err = conn.dbmssql.Exec(script).Error; err != nil {
		return *userdb, err
	}
	defer conn.dbmssql.Close()
	return *userdb, nil
}

func (conn *Connection) DelUserDBPermission(userdb *UserDB) (string, error) {
	var userdbpermission []UserDBPermission
	conn.dbmssql.Raw(`
				BEGIN
				DECLARE @SQLVerNo INT;
				SET @SQLVerNo = cast(substring(CAST(Serverproperty('ProductVersion') AS VARCHAR(50)) ,0,charindex('.',CAST(Serverproperty('ProductVersion') AS VARCHAR(50)) ,0)) as int);

				IF @SQLVerNo >= 9 
					IF EXISTS (SELECT TOP 1 *
							FROM Tempdb.sys.objects (nolock)
							WHERE name LIKE '#TUser%')
						DROP TABLE #TUser
				ELSE
				IF @SQLVerNo = 8
				BEGIN
					IF EXISTS (SELECT TOP 1 *
							FROM Tempdb.dbo.sysobjects (nolock)
							WHERE name LIKE '#TUser%')
						DROP TABLE #TUser
				END

				CREATE TABLE #TUser (
					ServerName    varchar(256),
					DBName        SYSNAME,
					[Name]        SYSNAME,
					GroupName     SYSNAME NULL,
					LoginName     SYSNAME NULL,
					default_database_name  SYSNAME NULL,
					default_schema_name    VARCHAR(256) NULL,
					Principal_id  INT,
					[sid]         VARBINARY(85))

				IF @SQLVerNo = 8
				BEGIN
					INSERT INTO #TUser
					EXEC sp_MSForEachdb
					'
					SELECT 
					@@SERVERNAME,
					''?'' as DBName,
					u.name As UserName,
					CASE WHEN (r.uid IS NULL) THEN ''public'' ELSE r.name END AS GroupName,
					l.name AS LoginName,
					NULL AS Default_db_Name,
					NULL as default_Schema_name,
					u.uid,
					u.sid
					FROM [?].dbo.sysUsers u
					LEFT JOIN ([?].dbo.sysMembers m 
					JOIN [?].dbo.sysUsers r
					ON m.groupuid = r.uid)
					ON m.memberuid = u.uid
					LEFT JOIN dbo.sysLogins l
					ON u.sid = l.sid
					WHERE u.islogin = 1 OR u.isntname = 1 OR u.isntgroup = 1
					/*and u.name like ''tester''*/
					ORDER BY u.name
					'
				END

				ELSE 
				IF @SQLVerNo >= 9
				BEGIN
					INSERT INTO #TUser
					EXEC sp_MSForEachdb
					'
					SELECT 
					@@SERVERNAME,
					''?'',
					u.name,
					CASE WHEN (r.principal_id IS NULL) THEN ''public'' ELSE r.name END GroupName,
					l.name LoginName,
					l.default_database_name,
					u.default_schema_name,
					u.principal_id,
					u.sid
					FROM [?].sys.database_principals u
					LEFT JOIN ([?].sys.database_role_members m
					JOIN [?].sys.database_principals r 
					ON m.role_principal_id = r.principal_id)
					ON m.member_principal_id = u.principal_id
					LEFT JOIN [?].sys.server_principals l
					ON u.sid = l.sid
					WHERE u.TYPE <> ''R''
					/*and u.name like ''tester''*/
					order by u.name
					'
				END


				Select 
				a.name name,
				b.GroupName groupname
				FROM sys.databases a 
				left join (
					SELECT a.DBName name,a.GroupName
					FROM #TUser a
					INNER JOIN sys.server_principals  b
					ON  b.name = a.LoginName
					WHERE b.principal_id =` + strconv.Itoa(userdb.ID) + `
					GROUP BY a.DBName,a.GroupName
				) b ON b.name=a.name
				WHERE a.database_id > 5  and b.name IS NOT NULL
				order by a.name
				DROP TABLE #TUser
				END
	`).Scan(&userdbpermission)
	var script = ``
	for i := range userdbpermission {
		var isExsit = false
		for j := range userdb.Databases {
			if userdb.Databases[j].Name == userdbpermission[i].Name {
				isExsit = true
			}
		}
		if !isExsit {
			if userdbpermission[i].GroupName != "db_owner" {
				script += `USE [` + userdbpermission[i].Name + `]
				DROP USER [` + userdb.Name + `]
				`
			}
		}

	}
	return script, nil
}

func (userdb *UserDB) CboGetUserDb(conn Connection) (Dropdown, error) {
	dropdown := Dropdown{}
	items := []DropdownItem{}

	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		return dropdown, err
	}
	if err := conn.dbmssql.Raw(`	
				BEGIN
				DECLARE @SQLVerNo INT;
				SET @SQLVerNo = cast(substring(CAST(Serverproperty('ProductVersion') AS VARCHAR(50)) ,0,charindex('.',CAST(Serverproperty('ProductVersion') AS VARCHAR(50)) ,0)) as int);

				IF @SQLVerNo >= 9 
					IF EXISTS (SELECT TOP 1 *
							FROM Tempdb.sys.objects (nolock)
							WHERE name LIKE '#TUser%')
						DROP TABLE #TUser
				ELSE
				IF @SQLVerNo = 8
				BEGIN
					IF EXISTS (SELECT TOP 1 *
							FROM Tempdb.dbo.sysobjects (nolock)
							WHERE name LIKE '#TUser%')
						DROP TABLE #TUser
				END

				CREATE TABLE #TUser (
					ServerName    varchar(256),
					DBName        SYSNAME,
					[Name]        SYSNAME,
					GroupName     SYSNAME NULL,
					LoginName     SYSNAME NULL,
					default_database_name  SYSNAME NULL,
					default_schema_name    VARCHAR(256) NULL,
					Principal_id  INT,
					[sid]         VARBINARY(85))

				IF @SQLVerNo = 8
				BEGIN
					INSERT INTO #TUser
					EXEC sp_MSForEachdb
					'
					SELECT 
					@@SERVERNAME,
					''?'' as DBName,
					u.name As UserName,
					CASE WHEN (r.uid IS NULL) THEN ''public'' ELSE r.name END AS GroupName,
					l.name AS LoginName,
					NULL AS Default_db_Name,
					NULL as default_Schema_name,
					u.uid,
					u.sid
					FROM [?].dbo.sysUsers u
					LEFT JOIN ([?].dbo.sysMembers m 
					JOIN [?].dbo.sysUsers r
					ON m.groupuid = r.uid)
					ON m.memberuid = u.uid
					LEFT JOIN dbo.sysLogins l
					ON u.sid = l.sid
					WHERE u.islogin = 1 OR u.isntname = 1 OR u.isntgroup = 1
					/*and u.name like ''tester''*/
					ORDER BY u.name
					'
				END

				ELSE 
				IF @SQLVerNo >= 9
				BEGIN
					INSERT INTO #TUser
					EXEC sp_MSForEachdb
					'
					SELECT 
					@@SERVERNAME,
					''?'',
					u.name,
					CASE WHEN (r.principal_id IS NULL) THEN ''public'' ELSE r.name END GroupName,
					l.name LoginName,
					l.default_database_name,
					u.default_schema_name,
					u.principal_id,
					u.sid
					FROM [?].sys.database_principals u
					LEFT JOIN ([?].sys.database_role_members m
					JOIN [?].sys.database_principals r 
					ON m.role_principal_id = r.principal_id)
					ON m.member_principal_id = u.principal_id
					LEFT JOIN [?].sys.server_principals l
					ON u.sid = l.sid
					WHERE u.TYPE <> ''R''
					/*and u.name like ''tester''*/
					order by u.name
					'
				END


				Select 
				a.name value, 
				a.name ,
				CASE WHEN b.name IS NULL THEN 0 ELSE 1 END AS 'selected' 
				FROM sys.databases a 
				left join (
					SELECT a.DBName name
					FROM #TUser a
					INNER JOIN sys.server_principals  b
					ON  b.name = a.LoginName
					WHERE b.principal_id =` + strconv.Itoa(userdb.ID) + `
					GROUP BY a.DBName
				) b ON b.name=a.name
				WHERE a.database_id > 5 
				order by a.name
				DROP TABLE #TUser
				END
	`).Find(&items).Error; err != nil {
		return dropdown, err
	}

	defer conn.dbmssql.Close()
	dropdown.Values = items
	return dropdown, nil
}
