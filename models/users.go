package models

type User struct {
	ID          int    `gorm:"primary_key" json:"id"`
	Username    string `json:"username"`
	Password    string `json:"password"`
	OldPassword string `json:"oldpassword"`
	Name        string `json:"name"`
	Role        string `json:"role"`
}

func (o *User) GetUserAuth() (User, error) {
	var user User
	if err := dbl.First(&user, "username = ? And password = ?", o.Username, o.Password).Error; err != nil {
		return user, err
	}
	return user, nil
}

func GetUsers() ([]User, error) {
	var user []User
	if err := dbl.Select([]string{"id", "username", "name"}).Find(&user).Error; err != nil {
		return user, err
	}
	return user, nil
}

func (user *User) GetUserByUsername() (User, error) {
	if err := dbl.Select([]string{"id", "username", "name"}).First(&user, "username = ?", user.Username).Error; err != nil {
		return *user, err
	}
	return *user, nil
}

func (o *User) CheckUsernameExist() (int, error) {
	var result = 0
	var user User
	dbl.Model(&user).Where("username = ?", o.Username).Count(&result)

	return result, nil
}

func (user *User) NewUser() (User, error) {
	dbl.Create(&user)
	if err := dbl.Select([]string{"id", "username", "name"}).First(&user, user.ID).Error; err != nil {
		return *user, err
	}
	return *user, nil
}

func (o *User) UpdateUser() error {
	var user User
	dbl.First(&user, "username = ?", o.Username)
	if o.Name != "" {
		user.Name = o.Name
	}
	if o.Password != "" {
		user.Password = o.Password
	}
	if err := dbl.Save(&user).Error; err != nil {
		return err
	}
	return nil
}

func (user *User) DeleteUserByUsername() error {
	if err := dbl.Where("username =?", user.Username).Delete(&user).Error; err != nil {
		return err
	}
	return nil
}

func (o *User) CboUserRoles() (Dropdown, error) {

	username := o.Username

	dropdown := Dropdown{}
	items := []DropdownItem{}

	dbl.Raw(`SELECT 
				A.v0 name,
				A.v0 value,
				CASE WHEN B.v1 is not null then 1 else 0 END as selected
				FROM casbin_rule A
				left join (
					SELECT 
						v1
					FROM casbin_rule
					where p_type = "g" and v0 = "` + username + `"
				) B on A.v0 = B.V1
				where A.p_type= "p" and A.v0 != "anonymous"
				group by A.v0;`).Find(&items)
	dropdown.Values = items
	return dropdown, nil
}
