package models

//Version : struct untuk history
type Version struct {
	ID       int    `gorm:"primary_key" json:"id"`
	AppID    int    `json:"appid"`
	BatchID  int    `json:"batchid"`
	FileID   int    `json:"fileid"`
	FileName string `json:"filename"`
	App      App    `gorm:"foreignkey:ID;association_foreignkey:AppID;" json:"app"`
	Batch    Batch  `gorm:"foreignkey:ID;association_foreignkey:BatchID;" json:"batch"`
	File     File   `gorm:"foreignkey:ID;association_foreignkey:FileID;" json:"file"`
}

//GetVersion : get all history list
func (version *Version) GetVersion() (Version, error) {
	dbl.Debug().Where("batch_id=?", version.BatchID).Preload("Batch").Find(&version)
	return *version, nil
}

func (version *Version) NewVersion() (Version, error) {
	tx := dbl.Begin()

	if err := dbl.Create(&version).Error; err != nil {
		tx.Rollback()
		return *version, err
	}
	tx.Commit()

	return *version, nil
}
