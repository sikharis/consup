package models

import (
	_ "github.com/mattn/go-sqlite3"
)

type File struct {
	ID      int    `gorm:"primary_key" json:"id"`
	Content []byte `json:"content"`
}

func (file *File) UploadFile() (File, error) {
	if err := dbl.Create(&file).Error; err != nil {
		return *file, err
	}

	return *file, nil
}
