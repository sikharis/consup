package models

import (
	"fmt"
	"strconv"
)

type Authorization struct {
	ID           int      `gorm:"column:id"  json:"id"`
	ConnectionID int      `gorm:"column:connectionid" json:"connectionid"`
	DBName       string   `gorm:"column:dbname" json:"dbname"`
	Users        []UserDB `gorm:"column:users" json:"users"`
	Role         Role     `gorm:"column:role" json:"role"`
}

func (o *Authorization) GetAuths() ([]Authorization, error) {
	var conn Connection
	var auth []Authorization
	conn.ID = o.ConnectionID
	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		return auth, err
	}
	conn.dbmssql.Raw(`Select name as dbname, '` + strconv.Itoa(o.ConnectionID) + `' as connectionid  From sys.databases  Where database_id > 5 ORDER BY name ASC`).Scan(&auth)
	defer conn.dbmssql.Close()
	return auth, nil
}

func (o *Authorization) GetUserDB() ([]UserDB, error) {
	var userdb []UserDB
	var conn Connection
	conn.ID = o.ConnectionID
	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		return userdb, err
	}
	conn.dbmssql.Raw(`
	CREATE TABLE #USERINDB (
		DBName        SYSNAME,
		UserName   SYSNAME
	)
		
	INSERT INTO #USERINDB
	EXEC sp_MSForEachdb
	'
	SELECT
	''?'' AS DBName,
	u.name AS [UserName]
	FROM
	[?].sys.database_principals AS u
	LEFT OUTER JOIN [?].sys.database_permissions AS dp ON dp.grantee_principal_id = u.principal_id and dp.type = ''CO''
	WHERE
	(u.type in (''U'', ''S'', ''G'', ''C'', ''K'' ,''E'', ''X'')) AND principal_id >4
	'
	
	SELECT DBName dbname, UserName name FROM #USERINDB
	WHERE DBName NOT IN ('master','tempdb','model','msdb') AND DBName = '` + o.DBName + `'
	
	DROP TABLE #USERINDB	
	`).Scan(&userdb)
	defer conn.dbmssql.Close()
	return userdb, nil
}

func (o *Authorization) GetRoleDB() (Dropdown, error) {
	var conn Connection
	dropdown := Dropdown{}
	items := []DropdownItem{}
	conn.ID = o.ConnectionID
	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		return dropdown, err
	}

	conn.dbmssql.Raw(`
	USE [` + o.DBName + `]
	DECLARE @dbname VARCHAR(500)
	SET @dbname = '` + o.DBName + `'

	CREATE TABLE #ROLEINDB (
		DBName     SYSNAME,
		RoleName   SYSNAME
	)

	CREATE TABLE #USERDBROLE (
		RoleName   SYSNAME,
		UserName   SYSNAME
	)
			

	INSERT INTO #ROLEINDB
	EXEC sp_MSForEachdb
	'
	SELECT 
	''?'' as DBName,
	name as RoleName 
	FROM [?].sys.database_principals
	WHERE type = ''R'' AND is_fixed_role = 0
	'

	;WITH RoleMembers (member_principal_id, role_principal_id) 
	AS 
	(
	SELECT 
	rm1.member_principal_id, 
	rm1.role_principal_id
	FROM sys.database_role_members rm1 (NOLOCK)
	UNION ALL
	SELECT 
	d.member_principal_id, 
	rm.role_principal_id
	FROM sys.database_role_members rm (NOLOCK)
	INNER JOIN RoleMembers AS d 
	ON rm.member_principal_id = d.role_principal_id
	)
	INSERT INTO #USERDBROLE
	select DISTINCT  rp.name as database_role, mp.name as database_userl
	from RoleMembers drm
	join sys.database_principals rp on (drm.role_principal_id = rp.principal_id)
	join sys.database_principals mp on (drm.member_principal_id = mp.principal_id)
	order by rp.name


	SELECT 
	#ROLEINDB.RoleName name, 
	#ROLEINDB.RoleName value, 
	CASE WHEN #USERDBROLE.RoleName IS NULL THEN 0 ELSE	1 END selected
	FROM #ROLEINDB
	LEFT JOIN #USERDBROLE ON #USERDBROLE.RoleName = #ROLEINDB.RoleName
	WHERE DBName = @dbname
	GROUP BY DBName,#ROLEINDB.RoleName,#USERDBROLE.RoleName

	DROP TABLE #USERDBROLE	
	DROP TABLE #ROLEINDB

	`).Find(&items)
	defer conn.dbmssql.Close()
	dropdown.Values = items
	return dropdown, nil
}

func (o *Authorization) UpdateAuth() (Authorization, error) {
	var conn Connection
	conn.ID = o.ConnectionID
	dbl.First(&conn)
	fmt.Println("auth", o)
	if err := conn.InitDBmssql(); err != nil {
		return *o, err
	}
	var script = ""
	for j := range o.Users {
		script += ` use ` + o.DBName + `
		ALTER ROLE [` + o.Role.Name + `] ADD MEMBER [` + o.Users[j].Name + `]`
	}
	temp, err := o.DeleteRoleUserDB()
	if err != nil {
		return *o, err
	}
	script += temp
	fmt.Println("print", script)
	if err := conn.dbmssql.Exec(script).Error; err != nil {
		fmt.Println("Error", err)
		return *o, err
	}
	defer conn.dbmssql.Close()
	return *o, nil
}
func (o *Authorization) GetRoleUserDB() (Dropdown, error) {
	var conn Connection
	dropdown := Dropdown{}
	items := []DropdownItem{}
	conn.ID = o.ConnectionID
	dbl.First(&conn)
	if err := conn.InitDBmssql(); err != nil {
		return dropdown, err
	}
	var script = ``
	script = `
	USE [` + o.DBName + `]
	DECLARE @dbname VARCHAR(500)
	SET @dbname = '` + o.DBName + `'

	CREATE TABLE #USERINDB (
		DBName     SYSNAME,
		UserName   SYSNAME
	)
	CREATE TABLE #ROLEINDB (
		DBName     SYSNAME,
		RoleName   SYSNAME
	)

	CREATE TABLE #USERDBROLE (
		RoleName   SYSNAME,
		UserName   SYSNAME
	)
			
	INSERT INTO #USERINDB
	EXEC sp_MSForEachdb
	'
	SELECT
	''?'' AS DBName,
	u.name AS [UserName]
	FROM
	[?].sys.database_principals AS u
	LEFT OUTER JOIN [?].sys.database_permissions AS dp ON dp.grantee_principal_id = u.principal_id and dp.type = ''CO''
	WHERE
	(u.type in (''U'', ''S'', ''G'', ''C'', ''K'' ,''E'', ''X'')) AND principal_id >4
		'

	INSERT INTO #ROLEINDB
	EXEC sp_MSForEachdb
	'
	SELECT 
	''?'' as DBName,
	name as RoleName 
	FROM [?].sys.database_principals
	WHERE type = ''R'' AND is_fixed_role = 0
	'

	;WITH RoleMembers (member_principal_id, role_principal_id) 
	AS 
	(
	SELECT 
	rm1.member_principal_id, 
	rm1.role_principal_id
	FROM sys.database_role_members rm1 (NOLOCK)
	UNION ALL
	SELECT 
	d.member_principal_id, 
	rm.role_principal_id
	FROM sys.database_role_members rm (NOLOCK)
	INNER JOIN RoleMembers AS d 
	ON rm.member_principal_id = d.role_principal_id
	)
	INSERT INTO #USERDBROLE
	select DISTINCT  rp.name as database_role, mp.name as database_userl
	from RoleMembers drm
	join sys.database_principals rp on (drm.role_principal_id = rp.principal_id)
	join sys.database_principals mp on (drm.member_principal_id = mp.principal_id)
	--RIGHT JOIN #USERINDB UDB ON UDB.UserName = mp.name
	--WHERE UDB.DBName NOT IN ('master','tempdb','model','msdb') AND UDB.DBName = @dbname
	order by rp.name


	SELECT 
	A.UserName name,
	A.UserName value,
	CASE WHEN  B.UserName IS NULL THEN 0 ELSE 1 END selected
	FROM #USERINDB A 
	LEFT JOIN  (
		SELECT 
		#ROLEINDB.DBName ,
		#ROLEINDB.RoleName,
		#USERDBROLE.UserName,
		CASE WHEN  #USERDBROLE.RoleName IS NULL THEN 0 ELSE 1 END SELECTEDROLE
		FROM #ROLEINDB
		LEFT JOIN #USERDBROLE ON #USERDBROLE.RoleName = #ROLEINDB.RoleName
		WHERE #ROLEINDB.RoleName = '` + o.Role.Name + `'
	) B ON B.UserName = A.UserName AND B.DBName = A.DBName
	WHERE A.DBName = @dbname 
	GROUP BY A.UserName,CASE WHEN  B.UserName IS NULL THEN 0 ELSE 1 END 

	DROP TABLE #USERINDB	
	DROP TABLE #USERDBROLE	
	DROP TABLE #ROLEINDB

	`
	conn.dbmssql.Raw(script).Find(&items)
	defer conn.dbmssql.Close()
	dropdown.Values = items
	return dropdown, nil
}
func (o *Authorization) DeleteRoleUserDB() (string, error) {
	var conn Connection
	conn.ID = o.ConnectionID
	dbl.First(&conn)
	var result = ``
	if err := conn.InitDBmssql(); err != nil {
		return result, err
	}
	var script = `
	USE [` + o.DBName + `]
	DECLARE @dbname VARCHAR(500)
	SET @dbname = '` + o.DBName + `'

	CREATE TABLE #USERINDB (
		DBName     SYSNAME,
		UserName   SYSNAME
	)
	CREATE TABLE #ROLEINDB (
		DBName     SYSNAME,
		RoleName   SYSNAME
	)

	CREATE TABLE #USERDBROLE (
		RoleName   SYSNAME,
		UserName   SYSNAME
	)
			
	INSERT INTO #USERINDB
	EXEC sp_MSForEachdb
	'
	SELECT
	''?'' AS DBName,
	u.name AS [UserName]
	FROM
	[?].sys.database_principals AS u
	LEFT OUTER JOIN [?].sys.database_permissions AS dp ON dp.grantee_principal_id = u.principal_id and dp.type = ''CO''
	WHERE
	(u.type in (''U'', ''S'', ''G'', ''C'', ''K'' ,''E'', ''X'')) AND principal_id >4
		'

	INSERT INTO #ROLEINDB
	EXEC sp_MSForEachdb
	'
	SELECT 
	''?'' as DBName,
	name as RoleName 
	FROM [?].sys.database_principals
	WHERE type = ''R'' AND is_fixed_role = 0
	'

	;WITH RoleMembers (member_principal_id, role_principal_id) 
	AS 
	(
	SELECT 
	rm1.member_principal_id, 
	rm1.role_principal_id
	FROM sys.database_role_members rm1 (NOLOCK)
	UNION ALL
	SELECT 
	d.member_principal_id, 
	rm.role_principal_id
	FROM sys.database_role_members rm (NOLOCK)
	INNER JOIN RoleMembers AS d 
	ON rm.member_principal_id = d.role_principal_id
	)
	INSERT INTO #USERDBROLE
	select DISTINCT  rp.name as database_role, mp.name as database_userl
	from RoleMembers drm
	join sys.database_principals rp on (drm.role_principal_id = rp.principal_id)
	join sys.database_principals mp on (drm.member_principal_id = mp.principal_id)
	--RIGHT JOIN #USERINDB UDB ON UDB.UserName = mp.name
	--WHERE UDB.DBName NOT IN ('master','tempdb','model','msdb') AND UDB.DBName = @dbname
	order by rp.name


	SELECT DISTINCT
		#USERDBROLE.UserName name
		FROM #ROLEINDB
		LEFT JOIN #USERDBROLE ON #USERDBROLE.RoleName = #ROLEINDB.RoleName
		WHERE #ROLEINDB.DBName = @dbname AND #USERDBROLE.UserName IS NOT NULL 
		AND #ROLEINDB.RoleName = '` + o.Role.Name + `'
	DROP TABLE #USERINDB	
	DROP TABLE #USERDBROLE	
	DROP TABLE #ROLEINDB

	`
	var users []UserDB
	conn.dbmssql.Raw(script).Scan(&users)
	for i := range users {
		var isExsit = false
		for j := range o.Users {
			if o.Users[j].Name == users[i].Name {
				isExsit = true
			}
		}
		if !isExsit {
			result += `
			use ` + o.DBName + `
			ALTER ROLE [` + o.Role.Name + `] DROP MEMBER [` + users[i].Name + `]`
		}

	}

	defer conn.dbmssql.Close()

	return result, nil
}
