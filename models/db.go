package models

import (
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	_ "github.com/mattn/go-sqlite3"
)

//
// All scheme diagram saved in https://dbdiagram.io/d/5c0df09644f24b0014dd3c48
//

var dbl *gorm.DB

//InitDB is for initialization database
func InitDB(filePath string, mode string) {
	var err error

	if _, err := os.Stat(filePath); err == nil {
		// path/to/whatever exists

	} else if os.IsNotExist(err) {
		// path/to/whatever does *not* exist

	} else {
		// Schrodinger: file may or may not exist. See err for details.

		// Therefore, do *NOT* use !os.IsNotExist(err) to test for file existence

	}

	os.MkdirAll(filePath, os.ModePerm)
	dbl, err = gorm.Open("sqlite3", filePath)
	// defer dbl.Close()

	if err != nil {
		log.Fatal("Failed to open database:", err)
	}

	switch mode {
	case "debug":
		dbl.LogMode(true)
	case "setup":
		dbl.DropTableIfExists(
			&App{},
			&Batch{},
			&Server{},
			&User{},
			&Version{},
			&AppServer{},
			&File{},
			&Connection{},
			&Authorization{},
			&File{},
			&Connection{},
			&Authorization{},
			&GeneralSetting{},
			&GenerateKey{},
			&Environment{},
		)

		dbl.AutoMigrate(
			&App{},
			&Batch{},
			&Server{},
			&User{},
			&Version{},
			&AppServer{},
			&File{},
			&Connection{},
			&Authorization{},
			&File{},
			&Connection{},
			&Authorization{},
			&GeneralSetting{},
			&GenerateKey{},
			&Environment{},
		)

		//INSERT DATA DUMMY
		dbl.Create(&User{ID: 1, Username: "Admin", Password: "AdminX", Name: "Administrator"})

		dbl.Create(&Environment{ID: 1, Name: "Production"})
		dbl.Create(&Environment{ID: 2, Name: "UAT"})

		dbl.Create(&Server{ID: 1, Name: "confinstest1", IP: "172.19.11.96", AuthUsername: `confinstest1\Administrator`, AuthPassword: "AdIns2017"})
		dbl.Create(&Server{ID: 2, Name: "confinstest2", IP: "172.19.11.99", AuthUsername: `confinstest2\Administrator`, AuthPassword: "AdIns2017"})
		dbl.Create(&Server{ID: 3, Name: "confinstest3", IP: "172.19.11.100", AuthUsername: `confinstest3\Administrator`, AuthPassword: "AdIns2017"})
		dbl.Create(&Server{ID: 4, Name: "confinstest4", IP: "172.19.11.101", AuthUsername: `confinstest4\Administrator`, AuthPassword: "AdIns2017"})
		dbl.Create(&Server{ID: 5, Name: "confinstest5", IP: "172.19.11.103", AuthUsername: `confinstest5\Administrator`, AuthPassword: "AdIns2017"})

		dbl.Create(&App{ID: 1, Name: "CONFINS "})
		dbl.Create(&App{ID: 2, Name: "WORKFLOW"})
		dbl.Create(&App{ID: 3, Name: "RULE"})
		dbl.Create(&App{ID: 4, Name: "JOURNAL"})
		dbl.Create(&App{ID: 5, Name: "REPORT"})
		dbl.Create(&App{ID: 6, Name: "BATCH WORKER"})

		dbl.Create(&AppServer{AppID: 1, EnvironmentID: 2, ServerID: 1, PathApplication: `C:\Inetpub\wwwroot\BII\`, PathConfig: `C:\Inetpub\wwwroot\BII\bin\`})
		dbl.Create(&AppServer{AppID: 1, EnvironmentID: 2, ServerID: 1, PathApplication: `C:\Inetpub\wwwroot\BIIQA\`, PathConfig: `C:\Inetpub\wwwroot\BIIQA\bin\`})
		dbl.Create(&AppServer{AppID: 1, EnvironmentID: 2, ServerID: 1, PathApplication: `C:\Inetpub\wwwroot\BIISS\`, PathConfig: `C:\Inetpub\wwwroot\BIISS\bin\`})
		dbl.Create(&AppServer{AppID: 1, EnvironmentID: 2, ServerID: 1, PathApplication: `C:\Inetpub\wwwroot\BNFDEV\`, PathConfig: `C:\Inetpub\wwwroot\BNFDEV\bin\`})
		dbl.Create(&AppServer{AppID: 1, EnvironmentID: 2, ServerID: 1, PathApplication: `C:\Inetpub\wwwroot\FIALDEV\`, PathConfig: `C:\Inetpub\wwwroot\FIALDEV\bin\`})
		dbl.Create(&AppServer{AppID: 1, EnvironmentID: 2, ServerID: 1, PathApplication: `C:\Inetpub\wwwroot\FIALDEV2\`, PathConfig: `C:\Inetpub\wwwroot\FIALDEV2\bin\`})
		dbl.Create(&AppServer{AppID: 1, EnvironmentID: 2, ServerID: 1, PathApplication: `C:\Inetpub\wwwroot\SMSDEV\`, PathConfig: `C:\Inetpub\wwwroot\SMSDEV\bin\`})
		dbl.Create(&AppServer{AppID: 1, EnvironmentID: 2, ServerID: 1, PathApplication: `C:\Inetpub\wwwroot\SSFDEV\`, PathConfig: `C:\Inetpub\wwwroot\SSFDEV\bin\`})
		dbl.Create(&AppServer{AppID: 1, EnvironmentID: 2, ServerID: 2, PathApplication: `C:\Inetpub\wwwroot\BFIDEV\`, PathConfig: `C:\Inetpub\wwwroot\BFIDEV\bin\`})
		dbl.Create(&AppServer{AppID: 1, EnvironmentID: 2, ServerID: 2, PathApplication: `C:\Inetpub\wwwroot\CSULDEV\`, PathConfig: `C:\Inetpub\wwwroot\CSULDEV\bin\`})
		dbl.Create(&AppServer{AppID: 1, EnvironmentID: 2, ServerID: 2, PathApplication: `C:\Inetpub\wwwroot\MPMDEV\`, PathConfig: `C:\Inetpub\wwwroot\MPMDEV\bin\`})
		dbl.Create(&AppServer{AppID: 1, EnvironmentID: 2, ServerID: 2, PathApplication: `C:\Inetpub\wwwroot\SGFDEV\`, PathConfig: `C:\Inetpub\wwwroot\SGFDEV\bin\`})

		dbl.Create(&Connection{Name: "Localhost", DatabaseServer: "localhost", UserName: "sa", Password: "sa", Port: "1433"})
		dbl.Create(&Connection{Name: "Local Express", DatabaseServer: `.\SQLEXPRESS`, UserName: "sa", Password: "sa", Port: "1433"})

		// dbl.Create(&Connection{Name: "Localhost", DatabaseServer: "localhost", UserName: "sa", Password: "sa", Port: "1433"})

		// var batch1 Batch
		// dbl.First(&batch1, 1)
		// dbl.Model(&batch1).Association("Environments").Append(
		// 	[]Environment{
		// 		Environment{ID: 1},
		// 		Environment{ID: 2},
		// 	},
		// )
		// var batch2 Batch
		// dbl.First(&batch2, 2)
		// dbl.Model(&batch2).Association("Environments").Append(
		// 	Environment{ID: 2},
		// )
	default:
		dbl.LogMode(false)
	}

	// dbl.DropTableIfExists(
	// 	&App{},
	// 	&Batch{},
	// 	&Server{},
	// 	&Environment{},
	// 	&User{},
	// 	&Version{},
	// 	&AppServer{},
	// 	&File{},
	// 	&Connection{},
	// 	&Authorization{},
	// 	&File{},
	// 	&Connection{},
	// 	&Authorization{},
	// 	&GeneralSetting{},
	// 	&GenerateKey{},
	// )

	// dbl.AutoMigrate(
	// 	&App{},
	// 	&Batch{},
	// 	&Server{},
	// 	&Environment{},
	// 	&User{},
	// 	&Version{},
	// 	&AppServer{},
	// 	&File{},
	// 	&Connection{},
	// 	&Authorization{},
	// 	&File{},
	// 	&Connection{},
	// 	&Authorization{},
	// 	&GeneralSetting{},
	// 	&GenerateKey{},
	// )

	// dbl.Create(&Connection{Name: "Localhost", DatabaseServer: "localhost", UserName: "sa", Password: "sa", Port: "1433"})

	// var batch1 Batch
	// dbl.First(&batch1, 1)
	// dbl.Model(&batch1).Association("Environments").Append(
	// 	[]Environment{
	// 		Environment{ID: 1},
	// 		Environment{ID: 2},
	// 	},
	// )
	// var batch2 Batch
	// dbl.First(&batch2, 2)
	// dbl.Model(&batch2).Association("Environments").Append(
	// 	Environment{ID: 2},
	// )

}
