package models

import "fmt"

type App struct {
	ID   int    `gorm:"primary_key"  gorm:"AUTO_INCREMENT"  json:"id"`
	Name string `json:"name"`
}

func GetApps() ([]App, error) {
	var apps []App
	dbl.Find(&apps)
	return apps, nil
}

func (o *App) GetAppByID() (App, error) {
	var app App
	dbl.Find(&app, o.ID)
	return app, nil
}

func (app *App) NewApp() (App, error) {
	fmt.Println("APP :", app)
	tx := dbl.Begin()
	if err := dbl.Create(&app).Error; err != nil {
		fmt.Println("Error :", err)
		tx.Rollback()
	} else {
		tx.Commit()
	}

	return *app, nil
}

func (app *App) UpdateApp() (App, error) {
	tx := dbl.Begin()
	dbl.Save(&app)
	tx.Commit()
	return *app, nil
}

func (app *App) DeleteAppByID() error {
	dbl.Delete(&app)
	return nil
}

func (app *App) CboApplications() (Dropdown, error) {

	dropdown := Dropdown{}
	items := []DropdownItem{}
	dbl.Table("apps apl").Select("apl.id as value, apl.name as name, CASE WHEN ver.app_id IS NULL THEN 0 ELSE 1 END as selected").
		Joins("LEFT JOIN versions ver on ver.app_id = apl.id AND ver.batch_id = ?", app.ID).
		Find(&items)
	dropdown.Values = items

	return dropdown, nil
}
