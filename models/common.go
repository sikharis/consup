package models

import (
	"time"

	_ "github.com/mattn/go-sqlite3"
)

type Activity struct {
	ID        int    `gorm:"primary_key" json:"id"`
	TableName string `json:"tablename"`
	TableID   int    `json:"tableid"`
	Content   []byte `json:"content"`

	UsrCrt string    `json:"usrcrt"`
	DtmCrt time.Time `json:"dtmcrt"`
}

//Exported
type DropdownItem struct {
	Name     string `gorm:"column:name" json:"name"`
	Value    string `gorm:"column:value" json:"value"`
	Selected bool   `gorm:"column:selected" json:"selected"`
}

type Dropdown struct {
	Values []DropdownItem `json:"values"`
}

// func (o *Dropdown) GetDropdown(query []DropdownItem) (Dropdown, error) {

// 	rows, err := db.Query(query)
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer rows.Close()

// 	var result []DropdownItem
// 	for rows.Next() {

// 		item := DropdownItem{}
// 		err2 := rows.Scan(&item.Value, &item.Name, &item.Selected)
// 		if err2 != nil {
// 			panic(err2)
// 		}

// 		result = append(result, item)
// 	}

// 	cboResult := Dropdown{}
// 	cboResult.Values = result

// 	return cboResult, nil
// }
