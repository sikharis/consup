package models

type Server struct {
	ID           int    `gorm:"primary_key" json:"id"`
	Name         string `json:"name"`
	IP           string `json:"ip"`
	AuthUsername string `json:"authusername"`
	AuthPassword string `json:"authpassword"`
	SharedKey    bool   `json:"sharedkey"`
}

func GetServers() ([]Server, error) {
	var servers []Server
	if err := dbl.Find(&servers).Error; err != nil {
		return servers, err
	}
	return servers, nil
}

func (server *Server) GetServerByIP() (Server, error) {
	if err := dbl.First(&server, "IP=?", server.IP).Error; err != nil {
		return *server, err
	}
	return *server, nil
}

func (server *Server) GetServerByID() (Server, error) {
	if err := dbl.First(&server, server.ID).Error; err != nil {
		return *server, err
	}
	return *server, nil
}

func (server *Server) NewServer() (Server, error) {
	dbl.Create(&server)
	if err := dbl.First(&server, server.ID).Error; err != nil {
		return *server, err
	}
	return *server, nil
}

func (o *Server) UpdateServer() (Server, error) {

	//Check if Exists
	var server Server
	if err := dbl.First(&server, o.ID).Error; err != nil {
		return server, err
	}
	//Update Data
	if o.Name != "" {
		server.Name = o.Name
	}
	if o.IP != "" {
		server.IP = o.IP
	}
	if o.AuthPassword != "" {
		server.AuthPassword = o.AuthPassword
	}
	if o.AuthUsername != "" {
		server.AuthUsername = o.AuthUsername
	}

	//Save
	if err := dbl.Save(&server).Error; err != nil {
		return server, err
	}
	return server, nil
}

func (server *Server) DeleteServerByID() error {
	dbl.Delete(&server, server.ID)
	var appserver []AppServer
	dbl.Delete(&appserver, "server_id = ?", server.ID)
	return nil
}

func (o *Server) CboServers() (Dropdown, error) {
	dropdown := Dropdown{}
	items := []DropdownItem{}
	dbl.Table("Apps ST").Select("sr.ID, sr.Name, 0").Find(&items)
	dropdown.Values = items
	return dropdown, nil
}
