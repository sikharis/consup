package models

import (
	"strconv"
)

type AppServer struct {
	ID              int         `gorm:"primary_key"  gorm:"AUTO_INCREMENT"  json:"id"`
	AppID           int         `json:"appid"`
	EnvironmentID   int         `json:"environmentid"`
	ServerID        int         `json:"serverid"`
	PathApplication string      `json:"pathapp"`
	PathConfig      string      `json:"pathconfig"`
	App             App         `gorm:"foreignkey:ID;association_foreignkey:AppID" json:"app"`
	Environment     Environment `gorm:"foreignkey:ID;association_foreignkey:EnvironmentID" json:"environment"`
	Server          Server      `gorm:"foreignkey:ID;association_foreignkey:ServerID" json:"server"`
}

func (o *AppServer) GetAppServerByALLID() (AppServer, error) {
	var appserver AppServer

	dbl.Where("app_id=? AND environment_id =? AND server_id=?", o.AppID, o.EnvironmentID, o.ServerID).Preload("Server").Preload("Environment").Preload("App").First(&appserver)
	return appserver, nil
}

func (o *AppServer) GetAppServerByID() {
	dbl.Where("id=? ", o.ID).Preload("Server").Preload("Environment").Preload("App").First(&o)
}
func (appserver *AppServer) NewAppServer() (AppServer, error) {

	tx := dbl.Begin()
	if err := dbl.Create(&appserver).Error; err != nil {
		tx.Rollback()
		return *appserver, err
	}

	tx.Commit()

	return *appserver, nil
}
func (appserver *AppServer) DeleteAppServerByID() error {
	if err := dbl.Where("app_id=? AND environment_id =? AND server_id=?", appserver.AppID, appserver.EnvironmentID, appserver.ServerID).Delete(&appserver).Error; err != nil {
		return err
	}
	return nil
}
func (newappserver *AppServer) UpdateAppServer(oldAppServer AppServer) (AppServer, error) {
	tx := dbl.Begin()
	if err := dbl.Model(&AppServer{}).Where("app_id = ? AND server_id =? AND environment_id =?", oldAppServer.AppID, oldAppServer.ServerID, oldAppServer.EnvironmentID).Update(&newappserver).Error; err != nil {
		tx.Rollback()
		return *newappserver, err
	}
	tx.Commit()
	return *newappserver, nil
}

func (o *AppServer) GetAppServerByAppID() ([]AppServer, error) {
	var appserver []AppServer
	dbl.Preload("Server").Preload("Environment").Preload("App").Find(&appserver, "app_id=?", o.AppID)
	return appserver, nil
}
func (o *AppServer) GetAppServerByEnvironmentID() ([]AppServer, error) {
	var appserver []AppServer
	dbl.Preload("Server").Preload("Environment").Preload("App").Find(&appserver, "environment_id=?", o.EnvironmentID)
	return appserver, nil
}
func (o *AppServer) GetAppServerByServerID() ([]AppServer, error) {
	var appserver []AppServer
	dbl.Preload("Server").Preload("Environment").Preload("App").Find(&appserver, "server_id=?", o.ServerID)
	return appserver, nil
}

func GetAppServerByServers() ([]AppServer, error) {
	var appserver []AppServer
	dbl.Preload("Server").Preload("Environment").Preload("App").Find(&appserver)
	return appserver, nil
}

func (o *AppServer) CboAppServerEnvironments() (Dropdown, error) {

	dropdown := Dropdown{}
	items := []DropdownItem{}

	dbl.Raw(`select env2.id as value,
	env2.name, 
	CASE WHEN a.environment_id IS NULL THEN 0 ELSE 1 END AS 'selected'  
	from environments env2
	left join (
	select aps.app_id,aps.environment_id,aps.server_id from app_servers aps
	where aps.id = ` + strconv.Itoa(o.ID) + `
	) a  on env2.id = a.environment_id`).Find(&items)

	dropdown.Values = items
	return dropdown, nil
}

func (o *AppServer) CboAppServerApps() (Dropdown, error) {

	AppID := strconv.Itoa(o.AppID)
	ServerID := strconv.Itoa(o.ServerID)
	EnvironmentID := strconv.Itoa(o.EnvironmentID)

	dropdown := Dropdown{}
	items := []DropdownItem{}

	dbl.Raw(`select 
				apps2.Name,
				apps2.ID as value, 
				CASE WHEN a.envid IS NULL THEN 0 ELSE 1 END AS 'selected'  
					from apps apps2
					left join (
						select ser.id serid,aps.app_id appid,env.id envid from app_servers aps
						inner join environments   env  on aps.environment_id = env.id
						inner join servers ser on ser.id = aps.server_id
						where ser.id = ` + ServerID + ` and aps.app_id = ` + AppID + ` and aps.environment_id = ` + EnvironmentID + `
					) 
					a  on apps2.id = a.appid`).Find(&items)
	dropdown.Values = items
	return dropdown, nil
}
