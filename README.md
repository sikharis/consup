## Prerequisite
1. Install Go Language
[Download Link](https://golang.org/dl/)
2. Install gcc from TDM
[Download Link](https://nchc.dl.sourceforge.net/project/tdm-gcc/TDM-GCC%20Installer/tdm64-gcc-5.1.0-2.exe)
3. Markdown Guide (documentational)
[Go to link](https://www.markdownguide.org/cheat-sheet)


## Development & Live Reloading
1. Install rerun
Run command ` go get github.com/ivpusic/rerun `
2. Create file di root dengan nama ".rerun.json"
	.rerun.json
	{
        "ignore": ["vendor", ".git"],
        "args": ["dev", "test"],
        "suffixes": [".go", ".html", ".tpl"],
        "attrib": true
    }
3. Run command ` rerun --attrib`

## Build & Deployment
1. Set CGO enable for SQL Lite build. *(windows only)*
` SET CGO_ENABLED=0 `
2. run this command for compile static resource into *.go file
` packr `
3. run command for build *.go source
` go build -o AdInsEncryptor.exe `

## Feature List
- [x] Web socket connection for realtime controll
- [x] Using Packr for embedded static file (only need .exe file to run)
- [ ] Using Iris for web socket and framework


[^1]: All dependencies are noted in vendor folder, managed by govendor tools. Go default package are used beside those.

